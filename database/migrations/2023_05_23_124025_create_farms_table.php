<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('farms', function (Blueprint $table) {
            $table->id();
            $table->integer('room_number');
            $table->integer('bathroom_number');
            $table->double('pool_space');
            $table->double('pool_depth');
            $table->boolean('furnished');
            $table->boolean('has_stadium');
            $table->boolean('has_parking');
            $table->boolean('has_water_well');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('farms');
    }
};
