<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->id();
            $table->foreignId('region_id')->constrained('regions')->cascadeOnDelete();
            $table->integer('propertyable_id');
            $table->string('propertyable_type');
            $table->integer('owner_id');
            $table->string('owner_type');
            $table->boolean('status');
            $table->boolean('accept_refuse')->nullable(true);
            $table->boolean('has_cladding');
            $table->text('description')->nullable(true);
            $table->integer('price');
            $table->boolean('rent_sale');
            $table->string('address')->nullable(true);
            $table->string('monthly_yearly')->nullable(true);
            $table->double('space');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('properties');
    }
};
