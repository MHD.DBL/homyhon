<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\AdController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\ContractController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\PropertyController;
use App\Http\Controllers\SubscriptionController;
use App\Http\Controllers\ChartJSController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WalletController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//Route::get('/', function () {
//    return view('home.home');
//});

//Route::get('/index', function () {
//    return view('users.index');
//});

Route::get('/charts', [ChartJSController::class, 'index'])->name('charts.index');

Route::get('/sendCode', [MailController::class, 'index']);

Route::get('/login', [AuthController::class, 'login']);

Route::view('/', 'Auth.auth')->name('auth');


//Auth::routes();
Route::get('/home', [AccountController::class, 'showAll'])->name('home');


//Route::group(['middleware' => ['auth:web', 'isActive']], function () {

Route::group(['prefix' => 'accounts'], function () {
    Route::get('/', [AccountController::class, 'showAll'])->name('accounts.index');
    Route::get('/create', [AccountController::class, 'create'])->name('accounts.create');
    Route::post('/createAccount', [AuthController::class, 'createAccount'])->name('accounts.store');
    Route::get('/{account_id}/show', [AccountController::class, 'showAccount'])->name('accounts.show');
    Route::get('/{account_id}/edit', [AccountController::class, 'edit'])->name('accounts.edit');
    Route::post('/update', [AccountController::class, 'update'])->name('accounts.update');
    Route::delete('/delete/{account_id}', [AccountController::class, 'destroy'])->name('accounts.destroy');
    Route::get('/search', [AccountController::class, 'search'])->name('accounts.search');
    Route::post('/blocked', [AccountController::class, 'accountBlock'])->name('accounts.blocked');
});

Route::prefix('ads/')->group(function () {
    Route::get('index', [AdController::class, 'showAll'])->name('ads.index');
    Route::post('create', [AdController::class, 'store'])->name('ads.store');
    Route::view('create','ads.create')->name('ads.create');
    Route::post('update/{ad_id}', [AdController::class, 'update'])->name('ads.update');
    Route::get('show/{ad_id}', [AdController::class, 'showAd'])->name('ads.show');
    Route::get('editAd/{ad_id}', [AdController::class, 'editAd'])->name('ads.edit');
    Route::delete('delete/{ad_id}', [AdController::class, 'destroy'])->name('ads.delete');
    Route::post('uploadImg/{ad_id}', [AdController::class, 'uploadImg'])->name('ads.uploadImg');
});

Route::prefix('orders/')->group(function () {
    Route::get('showAllOrdersProperty', [PropertyController::class, 'showAllOrdersProperty'])->name('orders.index');
    Route::post('acceptRefuse', [PropertyController::class, 'acceptRefuse'])->name('orders.acceptRefuse');
    Route::get('show/{property_id}', [PropertyController::class, 'showProperty'])->name('orders.showProperty');

    Route::get('showAllOrdersContract', [ContractController::class, 'showAllOrdersContract'])->name('ordersContract.index');
    Route::post('acceptRefuseContract', [ContractController::class, 'acceptRefuse'])->name('ordersContract.acceptRefuse');
    Route::get('showContract/{contract_id}', [ContractController::class, 'showContract'])->name('ordersContract.showContract');
});


Route::prefix('subscriptions/')->name('subscriptions.')->group(function () {
    Route::get('index', [SubscriptionController::class, 'showAll'])->name('index');
    Route::view('create','subscriptions.create')->name('create');
    Route::post('create', [SubscriptionController::class, 'create'])->name('store');
    Route::delete('delete/{subscription_id}', [SubscriptionController::class, 'destroy'])->name('destroy');
    Route::get('show/{subscription_id}', [SubscriptionController::class, 'show'])->name('show');
    Route::post('update/{subscription_id}', [SubscriptionController::class, 'update'])->name('update');
    Route::get('edit/{subscription_id}', [SubscriptionController::class, 'edit'])->name('edit');
});


Route::prefix('wallets/')->name('wallets.')->group(function (){
    Route::get('index',[WalletController::class,'index'])->name('index');
    Route::post('chargeWallet',[WalletController::class,'chargeWallet'])->name('chargeWallet');
    Route::post('create',[WalletController::class,'create'])->name('create');
    Route::get('show/{wallet_id}',[WalletController::class,'show'])->name('show');
    Route::get('edit/{wallet_id}',[WalletController::class,'edit'])->name('edit');
    Route::delete('delete/{wallet_id}',[WalletController::class,'destroy'])->name('destroy');
});

//});

