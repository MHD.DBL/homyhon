<?php

use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\ContractController;
use App\Http\Controllers\FarmController;
use App\Http\Controllers\GovernorateController;
use App\Http\Controllers\HouseController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\OfficeController;
use App\Http\Controllers\PropertyController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\WishListController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\AdController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\EvaluationController;
use App\Http\Controllers\SubscriptionController;
use App\Http\Controllers\WalletController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('/register', [AuthController::class, 'register'])->name('register');
Route::post('/sendCode', [MailController::class, 'index']);
Route::post('/login', [AuthController::class, 'login'])->name('login');
Route::post('/logout', [AuthController::class, 'logout'])->name('logout');
Route::post('/createAccount', [AuthController::class, 'createAccount'])->name('createAccount');

Route::group(['middleware' => ['auth:api','isActive']], function () {

 Route::prefix('notification/')->group(function () {
        Route::get('show_all', [NotificationController::class, 'index']);
        Route::get('update/{NotificationId}',[NotificationController::class,'update']);
    });




    Route::group(['prefix' => 'property/shop'], function () {
        Route::post('/add', [ShopController::class, 'store'])
        ->middleware('checkSubscription');
        
        Route::delete('/delete/{propertyid}', [ShopController::class, 'destroy']);
        Route::post('/update/{propertyid}', [ShopController::class, 'update']);
        Route::post('/uploadShopImage', [ShopController::class, 'uploadShopImage']);
        Route::post('/deleteShopImages', [ShopController::class, 'deleteShopImages']);
    });

    Route::group(['prefix' => 'property/house'], function () {
        Route::post('/add', [HouseController::class, 'store'])
        ->middleware('checkSubscription');
        
        Route::delete('/delete/{propertyid}', [HouseController::class, 'destroy']);
        Route::post('/update/{propertyid}', [HouseController::class, 'update']);
        Route::post('/uploadHouseImage', [HouseController::class, 'uploadHouseImage']);
        Route::post('/deleteHouseImages', [HouseController::class, 'deleteHouseImages']);
    });

    Route::group(['prefix' => 'property/farm'], function () {
        Route::post('/add', [FarmController::class, 'store'])
        ->middleware('checkSubscription');
        
        Route::delete('/delete/{propertyid}', [FarmController::class, 'destroy']);
        Route::post('/update/{propertyid}', [FarmController::class, 'update']);
        Route::post('/uploadFarmImage', [FarmController::class, 'uploadFarmImage']);
        Route::post('/deleteFarmImages', [FarmController::class, 'deleteFarmImages']);
    });

    Route::group(['prefix' => 'property'], function () {
        Route::get('/show_all', [PropertyController::class, 'index']);
        Route::get('/orderDesc', [PropertyController::class, 'orderDesc']);
        Route::get('/show_all_mine', [PropertyController::class, 'showPropertyList']);
        Route::get('/show_details/{propertyid}', [PropertyController::class, 'show']);
        Route::post('/show_searchSorting', [PropertyController::class, 'searchSorting']);
        Route::get('/myProperties', [PropertyController::class, 'myProperties']);
        Route::get('/propertiesOffice/{office_id}', [PropertyController::class, 'propertiesOffice']);
    });


    Route::group(['prefix' => 'contract'], function () {
        Route::post('/add', [ContractController::class, 'store']);
        Route::get('/show_all', [ContractController::class, 'index']);
        Route::get('/show_one_contract/{contractid}', [ContractController::class, 'show']);

    });

    Route::group(['prefix' => 'comment'], function () {
        Route::post('/add/{propertyid}', [CommentController::class, 'store']);
        Route::post('/update/{commentid}', [CommentController::class, 'update']);
        Route::post('/delete/{commentid}', [CommentController::class, 'destroy']);
        Route::get('/show_all/{propertyid}', [CommentController::class, 'index']);
    });

    Route::group(['prefix' => 'wish_list'], function () {
        Route::get('/add/{propertyid}', [WishListController::class, 'store']);
        Route::get('/show_all', [WishListController::class, 'index']);
    });

    Route::post('/create', [GovernorateController::class, 'create']);
    Route::post('/createRegion', [GovernorateController::class, 'createRegion']);
    Route::get('/show_governorates', [GovernorateController::class, 'showGovernorate']);
    Route::get('/show_regions/{governorateid}', [GovernorateController::class, 'showRegions']);

    Route::prefix('subscriptions/')->group(function () {
        Route::get('index', [SubscriptionController::class, 'index'])->name('index');
        Route::get('show/{subscription_id}', [SubscriptionController::class, 'show'])->name('show');
        Route::put('update/{subscription_id}', [SubscriptionController::class, 'update'])->name('update');
        Route::post('create', [SubscriptionController::class, 'create'])->name('create');
        Route::delete('delete/{subscription_id}', [SubscriptionController::class, 'destroy'])->name('delete');
        Route::post('subscribe', [SubscriptionController::class, 'subscribe'])->name('subscribe');
        Route::get('showMySub',[SubscriptionController::class,'showMySub']);
        Route::get('showAllSubRecord', [SubscriptionController::class, 'showAllSubRecord'])->name('showAllSubRecord');
    });

    Route::prefix('ads/')->group(function () {
        Route::get('index', [AdController::class, 'index'])->name('index');
        Route::get('orderDesc', [AdController::class, 'orderDesc'])->name('orderDesc');
        Route::post('create', [AdController::class, 'store'])->name('create');
        Route::post('update/{ad_id}', [AdController::class, 'update'])->name('update');
        Route::get('show/{ad_id}', [AdController::class, 'show'])->name('show');
        Route::delete('delete/{ad_id}', [AdController::class, 'destroy'])->name('delete');
        Route::post('uploadImg/{ad_id}', [AdController::class, 'uploadImg'])->name('uploadImg');
    });

    Route::prefix('accounts/')->middleware([])->group(function () {
        Route::get('index', [AccountController::class, 'index'])->name('index');
        Route::delete('delete/{account_id}', [AccountController::class, 'destroy'])->name('delete');
        Route::get('show', [AccountController::class, 'show'])->name('show');
        Route::post('update', [AccountController::class, 'update'])->name('update');
        Route::put('accountBlock/{account_id}', [AccountController::class, 'accountBlock'])->name('accountBlock');
        Route::get('showOfficeDetails/{account_id}', [OfficeController::class, 'showOfficeDetails'])->name('showOfficeDetails');

    });

    Route::prefix('evaluations/')->group(function (){
        Route::post('store',[EvaluationController::class,'store'])->name('store');
        Route::get('show/{office_id}',[EvaluationController::class,'show'])->name('show');
    });

    Route::prefix('wallets/')->group(function (){
        Route::get('index',[WalletController::class,'index'])->name('index');
        Route::put('chargeWallet/{wallet_id}',[WalletController::class,'chargeWallet'])->name('chargeWallet');
        Route::post('create',[WalletController::class,'create'])->name('create');
        Route::get('show/',[WalletController::class,'show'])->name('show');
        Route::delete('delete/{wallet_id}',[WalletController::class,'destroy'])->name('delete');
    });

    Route::post('/searchForOffice', [OfficeController::class, 'searchForOffice'])->name('searchForOffice');
    Route::get('/SortPopularOffices',[OfficeController::class,'SortPopularOffices'])->name('SortPopularOffices');
    Route::get('/types',[PropertyController::class,'types'])->name('types');
});





