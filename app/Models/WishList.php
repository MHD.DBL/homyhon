<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WishList extends Model
{
    use HasFactory;

    protected $table = 'wish_lists';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'account_id',
    ];

    public function account()
    {
        $this->belongsTo(Account::class, 'account_id');
    }

    public function properties()
    {
        return $this->belongsToMany(Property::class, 'property_wish_lists', 'wishList_id', 'property_id');
    }
}
