<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory;

    protected $table = 'properties';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'region_id',
        'propertyable_id',
        'propertyable_type',
        'owner_id',
        'owner_type',
        'status',
        'accept_refuse',
        'has_cladding',
        'description',
        'price',
        'rent_sale',
        'address',
        'monthly_yearly',
        'space'
    ];

    public function location()
    {
        return $this->morphOne(Location::class, 'locationable', 'locationable_type', 'locationable_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class,'property_id');
    }

    public function imgs()
    {
        return $this->morphMany(Img::class, 'imgable', 'imgable_type', 'imgable_id');
    }

    public function region()
    {
        return $this->belongsTo(Region::class, 'region_id');
    }

    public function propertyable()
    {
        return $this->morphTo(__FUNCTION__, 'propertyable_type', 'propertyable_id');
    }

    public function ownerable()
    {
        return $this->morphTo(__FUNCTION__, 'owner_type', 'owner_id');
    }

    public function contract()
    {
        return $this->hasOne(Contract::class,'property_id');
    }

    public function wish_lists()
    {
        return $this->belongsToMany(WishList::class, 'property_wish_lists', 'property_id', 'wishList_id');
    }
}
