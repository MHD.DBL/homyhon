<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    use HasFactory;

    protected $table = 'regions';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'governorate_id',
        'name'
    ];

    public function office()
    {
        return $this->hasMany(Office::class, 'region_id');
    }

    public function governorate()
    {
        return $this->belongsTo(Governorate::class, 'governorate_id');
    }

    public function properties()
    {
        return $this->hasMany(Property::class, 'region_id');
    }
}
