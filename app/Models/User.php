<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use App\Http\Controllers\ImgController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Model
{
    use HasFactory;

    protected $table = 'users';

    protected $primaryKey = 'id';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'gender', 'birthdate', 'account_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */


    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id');
    }

    public function properties()
    {
        return $this->morphMany(Property::class, 'ownerable', 'owner_type', 'owner_id');
    }

    public function subscriptions()
    {
        return $this->belongsToMany(Subscription::class, 'sub_records', 'user_id', 'subscription_id')->withPivot('date');
    }

    public function wallet()
    {
        return $this->hasOne(Wallet::class, 'user_id');
    }
}
