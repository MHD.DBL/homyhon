<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class property_wishList extends Model
{
    use HasFactory;

    protected $table = 'property_wish_lists';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillabl = [
        'property_id',
        'wishList_id'
    ];

}
