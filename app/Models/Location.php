<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Location extends Model
{
    use HasFactory;

    protected $table = 'locations';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'locationable_id',
        'locationable_type',
        'longitude',
        'latitude'
    ];

    public function locationable()
    {
        return $this->morphTo(__FUNCTION__, 'locationable_type', 'locationable_id');
    }

}
