<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class account_notification extends Model
{
    use HasFactory;
    protected $table = 'account_notifications';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillabl = [
        'account_id',
        'notification_id'
    ];
}
