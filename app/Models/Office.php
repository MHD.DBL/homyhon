<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    use HasFactory;

    protected $table = 'offices';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'region_id',
        'address',
        'officeName',
        'telephone',
        'account_id',
        'rating',
        'description'
    ];



    public function region()
    {
        return $this->belongsTo(Region::class, 'region_id');
    }

    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id');
    }

    public function evaluations()
    {
        return $this->hasMany(Evaluation::class, 'office_id');
    }

    public function location()
    {
        return $this->morphOne(Location::class, 'locationable', 'locationable_type', 'locationable_id');
    }

    public function properties()
    {
        return $this->morphMany(Property::class, 'ownerable', 'owner_type', 'owner_id');
    }

    public function contracts()
    {
        return $this->hasMany(Contract::class, 'office_id');
    }
}
