<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    use HasFactory;

    protected $table = 'houses';

    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'room_number',
        'bathroom_number',
        'floor_number',
        'green_tabo',
        'furnished'
    ];


    public function property()
    {
        return $this->morphOne(Property::class, 'propertyable', 'propertyable_type', 'propertyable_id');
    }
}
