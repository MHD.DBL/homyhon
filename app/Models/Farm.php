<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Farm extends Model
{
    use HasFactory;

    protected $table = 'farms';

    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'room_number',
        'bathroom_number',
        'pool_space',
        'pool_depth',
        'furnished',
        'has_stadium',
        'has_parking',
        'has_water_well'
    ];

    public function property()
    {
        return $this->morphOne(Property::class, 'propertyable', 'propertyable_type', 'propertyable_id');
    }
}
