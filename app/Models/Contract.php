<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    use HasFactory;

    protected $table = 'contracts';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'office_id',
        'property_id',
        'name_first_party',
        'phone_number_FP',
        'ratio',
        'accept_refuse'
    ];

    public function office()
    {
        return $this->belongsTo(Office::class, 'office_id');
    }

    public function property()
    {
        return $this->belongsTo(Property::class, 'property_id');
    }
}
