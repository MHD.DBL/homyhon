<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    use HasFactory;

    protected $table = 'ads';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'description',
        'link',
        'companyName'
    ];

    public function img()
    {
        return $this->morphOne(Img::class, 'imgable', 'imgable_type', 'imgable_id');
    }
}
