<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    use HasFactory;

    protected $table = 'admins';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable=[
        'role',
        'account_id'

    ];

    public function account(){
        return $this->belongsTo(Account::class,'account_id');
    }

}
