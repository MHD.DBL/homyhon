<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    use HasFactory;

    protected $table = 'subscriptions';

    protected $primaryKey = 'id';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'price',
        'propertyNumber',
        'period'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'sub_records', 'subscription_id', 'user_id')->withPivot('date');
    }
}
