<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;

class Account extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = 'accounts';

    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'firstName',
        'lastName',
        'email',
        'phoneNumber',
        'password',
        'blocked',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'password' => 'hashed',
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'account_id');
    }

    public function office()
    {
        return $this->hasOne(Office::class, 'account_id');
    }

    public function admin()
    {
        return $this->hasOne(Admin::class, 'account_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'property_id', 'id');
    }

    public function wish_list()
    {
        return $this->hasOne(WishList::class, 'account_id');
    }

    public function img()
    {
        return $this->morphOne(Img::class, 'imgable', 'imgable_type', 'imgable_id');
    }

     public function notifications()
    {
        return $this->hasMany(Notification::class,'account_id');
    }


    public function getJWTIdentifier()
    {
        // TODO: Implement getJWTIdentifier() method.
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        // TODO: Implement getJWTCustomClaims() method.
        return [];
    }
}
