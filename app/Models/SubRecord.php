<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubRecord extends Model
{
    use HasFactory;

    protected $table = 'sub_records';

    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'subscription_id',
        'date',
        'remainingProperty'
    ];

}
