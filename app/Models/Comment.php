<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $table = 'comments';

    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'account_id',
        'property_id',
        'content'
    ];

    public function account()
    {
        return $this->belongsTo(Account::class, 'account_id');
    }

    public function property()
    {
        return $this->belongsTo(Property::class, 'property_id');
    }

}
