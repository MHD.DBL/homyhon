<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    use HasFactory;

    protected $table = 'shops';

    protected $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'type'
    ];

    public function property()
    {
        return $this->morphOne(Property::class, 'propertyable', 'propertyable_type', 'propertyable_id');
    }

}
