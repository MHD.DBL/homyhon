<?php

namespace App\Charts;

use App\Models\SubRecord;
use App\Models\Subscription;
use ArielMejiaDev\LarapexCharts\LarapexChart;

class PolarAreaChart
{
    protected $chart;

    public function __construct(LarapexChart $chart)
    {
        $this->chart = $chart;
    }

    public function build(): \ArielMejiaDev\LarapexCharts\PolarAreaChart
    {
        $subscriptions = Subscription::get();
        $numberSubRecord = [];

        $item = 0;

        foreach ($subscriptions as $subscription) {
            $numberSubRecord[$item]=[
                'Count' => SubRecord::where('subscription_id',$subscription->id)->get()->count(),
                'SubName' => $subscription->name
            ];
            $item++;
        }


        return $this->chart
            ->polarAreaChart()
            ->addData([$numberSubRecord[0]['Count'], $numberSubRecord[1]['Count'], $numberSubRecord[2]['Count'], $numberSubRecord[3]['Count']])
            ->setLabels([$numberSubRecord[0]['SubName'], $numberSubRecord[1]['SubName'], $numberSubRecord[2]['SubName'],$numberSubRecord[3]['SubName']]);
    }
}
