<?php

namespace App\Charts;

use App\Models\Admin;
use App\Models\Office;
use App\Models\User;
use ArielMejiaDev\LarapexCharts\LarapexChart;

class IncomeChart
{
    protected $chart;

    public function __construct(LarapexChart $chart)
    {
        $this->chart = $chart;
    }

    public function build(): \ArielMejiaDev\LarapexCharts\PieChart
    {

        $admin = Admin::get();
        $admin = count($admin);

        $user = User::get();
        $user = count($user);

        $office = Office::get();
        $office = count($office);

        return $this->chart->pieChart()
            ->addData([$user, $admin, $office])
            ->setLabels(['User', 'Admin', 'Office']);
    }
}
