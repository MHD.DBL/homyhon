<?php

namespace App\Charts;

use App\Models\SubRecord;
use App\Models\Subscription;
use ArielMejiaDev\LarapexCharts\LarapexChart;

class AreaChart
{
    protected $chart;

    public function __construct(LarapexChart $chart)
    {
        $this->chart = $chart;
    }

    public function build(): \ArielMejiaDev\LarapexCharts\AreaChart
    {

        $listMonthsSub=[];
        $listValuesSub=[];

        for($monthNumber=1;$monthNumber<=12;$monthNumber++){
            $listMonthsSub[$monthNumber] = SubRecord::whereMonth('created_at',$monthNumber)->get();
            $listValuesSub[$monthNumber]=0;
            foreach ($listMonthsSub[$monthNumber] as $item){
                $items = Subscription::find($item->subscription_id);
                $listValuesSub[$monthNumber] = $listValuesSub[$monthNumber] +$items->price;
            }
        }

        return $this->chart->areaChart()
            ->addData('Total subscriptions during the month', [$listValuesSub[1],$listValuesSub[2],$listValuesSub[3],$listValuesSub[4],$listValuesSub[5],$listValuesSub[6],$listValuesSub[7],$listValuesSub[8],$listValuesSub[9],$listValuesSub[10],$listValuesSub[11],$listValuesSub[12]])
            ->setXAxis(['January', 'February', 'March', 'April', 'May', 'June','July', 'August', 'September', 'October', 'November', 'December']);
    }
}
