<?php

namespace App\Charts;

use App\Models\Property;
use ArielMejiaDev\LarapexCharts\LarapexChart;

class DonutChart
{
    protected $chart;

    public function __construct(LarapexChart $chart)
    {
        $this->chart = $chart;
    }

    public function build(): \ArielMejiaDev\LarapexCharts\DonutChart
    {

        $accepted = Property::where('accept_refuse','1')->get()->count();
        $refused = Property::where('accept_refuse','0')->get()->count();
        $pending = Property::where('accept_refuse',null)->get()->count();

        return $this->chart->donutChart()
            ->addData([$accepted,$refused ,$pending])
            ->setLabels(['Accepted', 'Refused','Pending'])
            ->setColors(['#62bf3b', '#d53838','#feb019']);
    }
}
