<?php

namespace App\Charts;

use App\Models\Farm;
use App\Models\House;
use App\Models\Shop;
use ArielMejiaDev\LarapexCharts\LarapexChart;

class ExpensesChart
{
    protected $chart;

    public function __construct(LarapexChart $chart)
    {
        $this->chart = $chart;
    }

    public function build(): \ArielMejiaDev\LarapexCharts\BarChart
    {
        $listMonthsHouse=[];
        $listMonthsFarm=[];
        $listMonthsShop=[];

        for($monthNumber=1;$monthNumber<=12;$monthNumber++){
            $listMonthsHouse[$monthNumber] = House::whereMonth('created_at',$monthNumber)->get()->count();
        }

        for($monthNumber=1;$monthNumber<=12;$monthNumber++){
            $listMonthsFarm[$monthNumber] = Farm::whereMonth('created_at',$monthNumber)->get()->count();
        }

        for($monthNumber=1;$monthNumber<=12;$monthNumber++){
            $listMonthsShop[$monthNumber] = Shop::whereMonth('created_at',$monthNumber)->get()->count();
        }

        return $this->chart->barChart()
            ->addData('House', [$listMonthsHouse[1],$listMonthsHouse[2],$listMonthsHouse[3],$listMonthsHouse[4],$listMonthsHouse[5],$listMonthsHouse[6],$listMonthsHouse[7],$listMonthsHouse[8],$listMonthsHouse[9],$listMonthsHouse[10],$listMonthsHouse[11],$listMonthsHouse[12]])
            ->addData('Farm', [$listMonthsFarm[1],$listMonthsFarm[2],$listMonthsFarm[3],$listMonthsFarm[4],$listMonthsFarm[5],$listMonthsFarm[6],$listMonthsFarm[7],$listMonthsFarm[8],$listMonthsFarm[9],$listMonthsFarm[10],$listMonthsFarm[11],$listMonthsFarm[12]])
            ->addData('Shop', [$listMonthsShop[1],$listMonthsShop[2],$listMonthsShop[3],$listMonthsShop[4],$listMonthsShop[5],$listMonthsShop[6],$listMonthsShop[7],$listMonthsShop[8],$listMonthsShop[9],$listMonthsShop[10],$listMonthsShop[11],$listMonthsShop[12]])
            ->setXAxis(['January', 'February', 'March', 'April', 'May', 'June','July', 'August', 'September', 'October', 'November', 'December']);
    }
}

