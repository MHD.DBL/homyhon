<?php

namespace App\Http\Controllers;

use App\Models\Evaluation;
use App\Models\Office;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class EvaluationController extends Controller
{
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        $evaluation = Evaluation::where('account_id',Auth::id())->where('office_id',$request->office_id)->first();

        if ($evaluation){
            return response()->json([
                'Status' => 'Success',
                'Message' => 'You have evaluated this Office before',
                'Code' => Response::HTTP_OK,
            ]);
        }

        $office = Office::find($request->office_id);

        if ($office){

            $evaluate = Evaluation::create([
                'office_id' => $office->id,
                'account_id' => Auth::id(),
                'value' => $request->value
            ]);

            $evaluates = Evaluation::where('office_id',$office->id)->get();

            $count = count($evaluates);

            $value = 0;

            foreach ($evaluates as $evaluate){
                $value = $evaluate->value + $value;
            }
            if ($value != 0){
                $evaluate = $value/$count;
                $office->update([
                    'rating' => (int)$evaluate
                ]);

            }

            return response()->json([
                'Status' => 'Success',
                'Message' => 'Office has been evaluated successfully',
                'Code' => Response::HTTP_OK,
            ]);
        }

        return response()->json([
            'Status' => 'Failed',
            'Message' => 'Office has not been evaluated because office not found',
            'Code' => Response::HTTP_NOT_ACCEPTABLE,

        ]);

    }

    /**
     * Display the specified resource.
     */
    public function show($office_id)
    {
        $office =Office::find($office_id);

        if(isset($office) == 1){

            return response()->json([
                'Status' => 'Success',
                'Message' => 'View all evaluations for this office',
                'Code' => Response::HTTP_OK,
                'office' => $office,
            ]);

        }

        return response()->json([
            'Status' => 'Failed',
            'Message' => 'Office not found',
            'Code' => Response::HTTP_NOT_ACCEPTABLE,
        ]);
    }

}
