<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Admin;
use App\Models\Office;
use App\Models\SubRecord;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
class OfficeController extends Controller
{
    public function searchForOffice(Request $request){

        // $request->validate([
        //     'officeName'=>'min:1'
        // ]);

        if($request->officeName == null){
            $office = Office::with('account')->get();

            $office->map(function ($item) {
                $item->region_name = $item->region->name;
                $item->governorate_name = $item->region->governorate->name;
                $item->first_image = $item->account->img->path ?? null;
                $item->first_name = $item->account->firstName;
                $item->last_name = $item->account->lastName;
                $item->email = $item->account->email;
                $item->phone_number = $item->account->phoneNumber;
                unset($item->region);
                unset($item->region_id);
                unset($item->created_at);
                unset($item->updated_at);
                unset($item->account);
                return $item;
            });

            return response()->json([
                'Status' => 'Success',
                'Message' => 'All offices',
                'Code' => Response::HTTP_OK,
                'offices' => $office
            ]);
        }

        $office = Office::where('officeName','like','%'.$request->officeName.'%')->get();

                $office->map(function ($item) {
                $item->region_name = $item->region->name;
                $item->governorate_name = $item->region->governorate->name;
                $item->first_image = $item->account->img->path ?? null;
                $item->first_name = $item->account->firstName;
                $item->last_name = $item->account->lastName;
                $item->email = $item->account->email;
                $item->phone_number = $item->account->phoneNumber;
                unset($item->region);
                unset($item->region_id);
                unset($item->created_at);
                unset($item->updated_at);
                unset($item->account);
                return $item;
            });

        if (isset($office[0])==1){
            return response()->json([
                'Status' => 'Success',
                'Message' => 'All offices that match the office name',
                'Code' => Response::HTTP_OK,
                'offices' => $office
            ]);
        }

        return response()->json([
            'Status' => 'Failed',
            'Message' => 'There are not any office that match the office name',
            'Code' => Response::HTTP_NOT_ACCEPTABLE,
            'offices' => []

        ]);

    }

    public function SortPopularOffices(){

        $office = Office::orderBy('rating','desc')->get();

        if ($office){
            return response()->json([
                'Status' => 'Success',
                'Message' => 'View all popular offices',
                'Code' => Response::HTTP_OK,
                'offices' => $office
            ]);
        }

        return response()->json([
            'Status' => 'Failed',
            'Message' => 'Offices not found',
            'Code' => Response::HTTP_NOT_ACCEPTABLE,

        ]);
    }

        public function showOfficeDetails($account_id)
    {
        $account = Account::find($account_id);
        if (!isset($account)) {
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'Account does not exist',
                'List' => null,
                'Code' => Response::HTTP_NOT_ACCEPTABLE,
            ]);
        }

        $account->makeHidden(['created_at', 'updated_at', 'id', 'img']);

        if ($office = Office::where('account_id', $account_id)->with(['region', 'region.governorate'])->first()) {
            $image = null;
            $location = null;
            if ($account->img) {
                $image = $account->img->path;
            }
            if ($office->location) {
                $location = $office->location;
                $location->makeHidden(['created_at', 'updated_at', 'id', 'locationable_id', 'locationable_type']);
            }
            $office->makeHidden(['created_at', 'updated_at', 'id', 'account_id', 'location', 'region', 'region_id']);
            $region = $office->region;
            $accountt = $account->toArray();
            $accountt['blocked'] = (bool)$accountt['blocked'];
            $list = [
                'account' => $accountt,
                'Office' => $office,
                'image' => $image,
                'region name' => $region->name,
                'governorate name' => $region->governorate->name,
                'location' => $location,
            ];
            return response()->json([
                'Status' => 'Success',
                'Message' => 'Office information retrieved successfully',
                'List' => $list,
                'Code' => Response::HTTP_OK,
            ]);
        }
        return response()->json([
            'Status' => 'Failed',
            'Message' => 'Office not found',
            'List' => null,
            'Code' => Response::HTTP_NOT_ACCEPTABLE,
        ]);
    }


}
