<?php

namespace App\Http\Controllers;

use App\Models\Office;
use App\Models\Property;
use App\Models\User;
use App\Models\Account;
use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     */
     
     public function propertiesOffice($office_id)
    {

        $office = Office::find($office_id);

        if (isset($office) == 0) {
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'Office not found',
                'List' => null,
                'Code' => Response::HTTP_NOT_ACCEPTABLE,
            ]);
        }
        $pagination = Property::query()
            ->where('owner_id',$office_id)
            ->where('owner_type','App\Models\Office')
            ->select('id', 'price', 'propertyable_type', 'rent_sale', 'region_id')
            ->with(['region', 'region.governorate','imgs'])
            ->where('accept_refuse', '1')
            ->get();

        $properties = $pagination->map(function ($item) {
            $item->propertyable_type = class_basename($item->propertyable_type);
            $item->rent_sale = $item->rent_sale ? 'Rent' : 'Sell';
            $item->region_name = $item->region->name;
            $item->governorate_name = $item->region->governorate->name;
            $item->first_image = $item->imgs[0]->path ?? null;
            unset($item->region);
            unset($item->imgs);
            return $item;
        });
        $properties->makeHidden(['region_id']);


        if(isset($properties[0]) == 0){
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'This office have not been posted any property yet',
                'List' => $properties,
                'Code' => Response::HTTP_NOT_ACCEPTABLE,
            ]);
        }

        return response()->json([
            'Status' => 'Success',
            'Message' => 'All Properties That Have Been Posted By this office' ,
            'List' => $properties,
            'Code' => Response::HTTP_OK,
        ]);
    }


    public function myProperties()
    {
        $account = Account::query()
            ->with(['user','office'])
            ->find(Auth::id());

        if (isset($account->user->id) == 1){
            $account_id = $account->user->id;
            $account_type = 'App\Models\User';
        }
        if (isset($account->office->id) == 1){
            $account_id = $account->office->id;
            $account_type = 'App\Models\Office';
        }

        $pagination = Property::query()
            ->where('owner_id',$account_id)
            ->where('owner_type',$account_type)
            ->select('id', 'price', 'propertyable_type', 'rent_sale', 'region_id')
            ->with(['region', 'region.governorate','imgs'])
            // ->whereHas('imgs', function ($query) {
            //     $query->where('imgable_type', 'App\Models\Property')
            //         ->orderBy('id');
            // })
            ->get();

        $properties = $pagination->map(function ($item) {
            $item->propertyable_type = class_basename($item->propertyable_type);
            $item->rent_sale = $item->rent_sale ? 'Rent' : 'Sell';
            $item->region_name = $item->region->name;
            $item->governorate_name = $item->region->governorate->name;
            $item->first_image = $item->imgs[0]->path ?? null;
            unset($item->region);
            unset($item->imgs);
            return $item;
        });
        $properties->makeHidden(['region_id']);


        if(isset($properties[0]) == 0){
            return response()->json([
                'Status' => 'Failed',
                'Message' => $account->firstName. ' '.$account->lastName . ' have not been posted any property yet',
                'List' => $properties,
                'Code' => Response::HTTP_NOT_ACCEPTABLE,
            ]);
        }

        return response()->json([
            'Status' => 'Success',
            'Message' => 'All Properties That Have Been Posted By '.$account->firstName. ' '.$account->lastName ,
            'List' => $properties,
            'Code' => Response::HTTP_OK,
        ]);
    }

    public function types(){
        $PropertryType =[
            'House',
            'Farm',
            'Shop'
        ];

        return response()->json([
            'Status' => 'Success',
            'Message' => 'Type of properties',
            'List' => $PropertryType,
            'Code' => Response::HTTP_OK,
        ]);
    }

    public function orderDesc()
    {
        $pagination = Property::query()
            ->select('id', 'price', 'propertyable_type', 'rent_sale', 'region_id')
            ->with(['region', 'region.governorate','imgs'])
            // ->whereHas('imgs', function ($query) {
            //     $query->where('imgable_type', 'App\Models\Property')
            //         ->orderBy('id')
                // ;})
                ->where('accept_refuse', '1')->orderBy('id','desc')->take(5)->get();

        $properties = $pagination->map(function ($item) {
            $item->propertyable_type = class_basename($item->propertyable_type);
            $item->rent_sale = $item->rent_sale ? 'Rent' : 'Sell';
            $item->region_name = $item->region->name;
            $item->governorate_name = $item->region->governorate->name;
            $item->first_image = $item->imgs[0]->path ?? null;
            unset($item->region);
            unset($item->imgs);
            return $item;
        });
        $properties->makeHidden(['region_id']);

        return response()->json([
            'Status' => 'Success',
            'Message' => 'All Properties Have Been Successfully Retrieved ',
            'List' => $properties,
            'Code' => Response::HTTP_OK,
        ]);
    }

    // public function index()
    // {
         
    //     $pagination = Property::query()
    //         ->select('id', 'price', 'propertyable_type', 'rent_sale', 'region_id')
    //         ->with(['region', 'region.governorate'])
    //         // ->whereHas('imgs', function ($query) {
    //         //     $query->where('imgable_type', 'App\Models\Property')
    //         //         ->orderBy('id');
    //         // })
    //         // ->where('accept_refuse', '1')
    //         ->paginate(5);

    //     $properties = $pagination->getCollection()->map(function ($item) {
    //         $item->propertyable_type = class_basename($item->propertyable_type);
    //         $item->rent_sale = $item->rent_sale ? 'Rent' : 'Sell';
    //         $item->region_name = $item->region->name;
    //         $item->governorate_name = $item->region->governorate->name;
    //         $item->first_image = $item->imgs[0]->path ?? null;
    //         unset($item->region);
    //         unset($item->imgs);
    //         return $item;
    //     });
    //     $properties->makeHidden(['region_id']);

    //     $paginationData = [
    //         'total_pages' => $pagination->lastPage(),
    //         'current_page' => $pagination->currentPage(),
    //         'next_page_url' => $pagination->nextPageUrl(),
    //         'prev_page_url' => $pagination->previousPageUrl(),
    //     ];

    //     return response()->json([
    //         'Status' => 'Success',
    //         'Message' => 'All Properties Have Been Successfully Retrieved ',
    //         'List' => $properties,
    //         'Code' => Response::HTTP_OK,
    //         'Pagination' => $paginationData,
    //     ]);
    // }
    
    public function index()
    {
        $pagination = Property::query()
            ->select('id', 'price', 'propertyable_type', 'rent_sale', 'region_id')
            ->with(['region', 'region.governorate','imgs'])
            // ->whereHas('imgs', function ($query) {
            //     $query->where('imgable_type', 'App\Models\Property')
            //         ->orderBy('id');
            // })
            ->where('accept_refuse', '1')
            ->paginate(300);

        $properties = $pagination->getCollection()->map(function ($item) {
            $item->propertyable_type = class_basename($item->propertyable_type);
            $item->rent_sale = $item->rent_sale ? 'Rent' : 'Sell';
            $item->region_name = $item->region->name;
            $item->governorate_name = $item->region->governorate->name;
            $item->first_image = $item->imgs[0]->path ?? null;
            unset($item->region);
            unset($item->imgs);
            return $item;
        });
        $properties->makeHidden(['region_id']);

        $paginationData = [
            'total_pages' => $pagination->lastPage(),
            'current_page' => $pagination->currentPage(),
            'next_page_url' => $pagination->nextPageUrl(),
            'prev_page_url' => $pagination->previousPageUrl(),
        ];

        return response()->json([
            'Status' => 'Success',
            'Message' => 'All Properties Have Been Successfully Retrieved ',
            'List' => $properties,
            'Code' => Response::HTTP_OK,
            'Pagination' => $paginationData,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function showPropertyList()
    {
        $user = User::where('account_id', Auth::id())->first();
        $office = Office::where('account_id', Auth::id())->first();
        if ($user) {
            $pagination = Property::query()
                ->select('id', 'price', 'propertyable_type', 'rent_sale', 'region_id')
                ->with(['region', 'region.governorate'])
                ->where(function ($query) use ($user) {
                    $query->where('owner_type', 'App\Models\User')
                        ->where('owner_id', $user->id);
                })
                ->whereHas('imgs', function ($query) {
                    $query->where('imgable_type', 'App\Models\Property')
                        ->orderBy('id');
                })
                ->paginate(300);
        } else if ($office) {
            $pagination = Property::query()
                ->select('id', 'price', 'propertyable_type', 'rent_sale', 'region_id')
                ->with(['region', 'region.governorate'])
                ->where(function ($query) use ($office) {
                    $query->where('owner_type', 'App\Models\Office')
                        ->where('owner_id', $office->id);
                })
                ->whereHas('imgs', function ($query) {
                    $query->where('imgable_type', 'App\Models\Property')
                        ->orderBy('id');
                })
                ->paginate(300);
        } else {
            return response()->json([
                'Status' => 'Success',
                'Message' => 'All Properties Have Been Successfully Retrieved ',
                'List' => null,
                'Code' => Response::HTTP_OK,
            ]);
        }
        $properties = $pagination->getCollection()->map(function ($item) {
            $item->propertyable_type = class_basename($item->propertyable_type);
            $item->rent_sale = $item->rent_sale ? 'Rent' : 'Sell';
            $item->region_name = $item->region->name;
            $item->governorate_name = $item->region->governorate->name;
            $item->first_image = $item->imgs[0]->path ?? null;
            unset($item->region);
            unset($item->imgs);
            return $item;
        });
        $properties->makeHidden(['region_id']);

        $paginationData = [
            'total_pages' => $pagination->lastPage(),
            'current_page' => $pagination->currentPage(),
            'next_page_url' => $pagination->nextPageUrl(),
            'prev_page_url' => $pagination->previousPageUrl(),
        ];

        return response()->json([
            'Status' => 'Success',
            'Message' => 'All Properties Have Been Successfully Retrieved',
            'List' => $properties,
            'Code' => Response::HTTP_OK,
            'Pagination' => $paginationData,
        ]);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }


//   public function show($propertyid)
//     {
//         $property = Property::find($propertyid);

//         if ($property) {
//             $property->makeHidden(['created_at', 'updated_at', 'region_id', 'propertyable_id', 'owner_id', 'imgs', 'ownerable', 'propertyable', 'region', 'location']);
//             $images = $property->imgs->pluck('path')->toArray();
//             $location = $property->location;
//             $location->makeHidden(['created_at', 'updated_at', 'id', 'locationable_id', 'locationable_type']);
//             $owner = $property->ownerable;
//             $owner->makeHidden(['created_at', 'updated_at', 'account_id', 'region_id', 'account', 'birthdate', 'region']);
//             if ($owner->region && $owner->region->name) {
//                 $owner->region_Office = $owner->region->name;
//                 $owner->governorate_Office = $owner->region->governorate->name;
//             }
//             $account = $owner->account;
//             $account->makeHidden(['created_at', 'updated_at', 'blocked']);
//             $type = $property->propertyable;
//             $type->makeHidden(['created_at', 'updated_at']);
//             $list = [
//                 'property' => $property,
//                 'images' => $images,
//                 'location' => $location,
//                 'owner' => $owner,
//                 'account' => $account,
//                 'details' => $type,
//                 'region' => $property->region->name,
//                 'governorate_name' => $property->region->governorate->name,
//             ];
//             return response()->json([
//                 'Status' => 'Success',
//                 'Message' => 'Property Have Been Successfully Retrieved',
//                 'List' => $list,
//                 'Code' => Response::HTTP_OK,
//             ]);
//         }
//         return response()->json([
//             'Status' => 'Failed',
//             'Message' => 'Property not found',
//             'List' => null,
//             'Code' => Response::HTTP_NOT_ACCEPTABLE,
//         ]);
//     }

 public function show($propertyid)
    {
        $property = Property::find($propertyid);

        if ($property) {
            $property->makeHidden(['created_at', 'updated_at', 'region_id', 'propertyable_id', 'owner_id', 'imgs', 'ownerable', 'propertyable', 'region', 'location']);
            $images = $property->imgs->pluck('path')->toArray();
            $location = $property->location;
            $location->makeHidden(['created_at', 'updated_at', 'id', 'locationable_id', 'locationable_type']);
            $owner = $property->ownerable;
            $owner->makeHidden(['created_at', 'updated_at', 'account_id', 'region_id', 'account', 'birthdate', 'region']);
            if ($owner->region && $owner->region->name) {
                $owner->region_Office = $owner->region->name;
                $owner->governorate_Office = $owner->region->governorate->name;
            }
            $account = $owner->account;
            $account->makeHidden(['created_at', 'updated_at', 'blocked']);
            $type = $property->propertyable;
            $type->makeHidden(['created_at', 'updated_at']);
            $property->propertyable_type=class_basename($property->propertyable);
            $property->owner_type=class_basename($owner);
            $list = [
                'property' => $property,
                'images' => $images,
                'location' => $location,
                'owner' => $owner,
                'account' => $account,
                'details' => $type,
                'region' => $property->region->name,
                'governorate_name' => $property->region->governorate->name,
            ];
            return response()->json([
                'Status' => 'Success',
                'Message' => 'Property Have Been Successfully Retrieved',
                'List' => $list,
                'Code' => Response::HTTP_OK,
            ]);
        }
        return response()->json([
            'Status' => 'Failed',
            'Message' => 'Property not found',
            'List' => null,
            'Code' => Response::HTTP_NOT_ACCEPTABLE,
        ]);
    }

    public function searchSorting(Request $request)
    {
        $query = Property::query()
            ->select('id', 'price', 'propertyable_type', 'rent_sale', 'region_id')
            ->with(['region', 'region.governorate'])
            ->whereHas('imgs', function ($query) {
                $query->where('imgable_type', 'App\Models\Property')->orderBy('id');
            })
            ->where('accept_refuse', '1');

        if ($request->max && $request->min) {
            $query->where('price', '<=', $request->max)
                ->where('price', '>=', $request->min);
        } elseif ($request->max) {
            $query->where('price', '<=', $request->max);
        } elseif ($request->min) {
            $query->where('price', '>=', $request->min);
        }

        if ($request->governorate) {
            $query->whereHas('region.governorate', function ($query) use ($request) {
                $query->where('name', $request->governorate);
            });
        }

        if ($request->region) {
            $query->whereHas('region', function ($query) use ($request) {
                $query->where('name', $request->region);
            });
        }

        if ($request->rent) {
            $query->where('rent_sale', '1');
        }

        if ($request->sell) {
            $query->where('rent_sale', '0');
        }

        if ($request->shop) {
            $query->where('propertyable_type', 'App\Models\Shop');
        }

        if ($request->house) {
            $query->where('propertyable_type', 'App\Models\House');
        }

        if ($request->farm) {
            $query->where('propertyable_type', 'App\Models\Farm');
        }

        $pagination = $query->paginate(300);
        $properties = $this->mapAndHideProperties($pagination->getCollection());

        $paginationData = [
            'total_pages' => $pagination->lastPage(),
            'current_page' => $pagination->currentPage(),
            'next_page_url' => $pagination->nextPageUrl(),
            'prev_page_url' => $pagination->previousPageUrl(),
        ];

        return response()->json([
            'Status' => 'Success',
            'Message' => 'All Properties Meeting the Criteria Have Been Successfully Retrieved',
            'List' => $properties,
            'Code' => Response::HTTP_OK,
            'Pagination' => $paginationData,
        ]);
    }

    private function mapAndHideProperties($collection)
    {
        return $collection->map(function ($item) {
            $item->propertyable_type = class_basename($item->propertyable_type);
            $item->rent_sale = $item->rent_sale ? 'Rent' : 'Sell';
            $item->region_name = $item->region->name;
            $item->governorate_name = $item->region->governorate->name;
            $item->first_image = $item->imgs[0]->path ?? null;
            unset($item->region);
            unset($item->imgs);
            return $item;
        })->makeHidden(['region_id']);
    }


    public function showAllOrdersProperty()
    {
        $properties = Property::query()
            ->with(['region', 'region.governorate','propertyable','imgs'])
            ->where('accept_refuse', null)->orWhere('accept_refuse', '0')
            ->paginate(5);
            
        // $properties = $properties->getCollection()->map(function ($item) {
        //     $item->propertyable_type = class_basename($item->propertyable_type);
        //     $item->owner_type = class_basename($item->owner_type);
        //     return $item;
        // });

        return view('orders.viewOrdersProperty',['properties'=>$properties]);
    }

      public function acceptRefuse(Request $request)
    {
        $property = Property::find($request->property_id);
        if ($request->accept_refuse == 0) {
            if ($property->owner_type == 'App\Models\User') {
                $user = User::where('id', $property->owner_id)->first();
                $account = Account::where('id', $user->account_id)->first();
                if ($account) {
                    $notification = new Notification([
                        'seen' => 0,
                        'description' => "Your Property Is Rejected!",
                    ]);
                    $account->notifications()->save($notification);
                }
            } elseif ($property->owner_type == 'App\Models\Office') {
                $office = Office::where('id', $property->owner_id)->first();
                $account = Account::where('id', $office->account_id)->first();
                if ($account) {
                    $notification = new Notification([
                        'seen' => 0,
                        'description' => 'Your Property Is Rejected!',
                    ]);
                    $account->notifications()->save($notification);
                }
            }
        }
        elseif ($request->accept_refuse == 1){
            if ($property->owner_type == 'App\Models\User') {
                $user = User::where('id', $property->owner_id)->first();
                $account = Account::where('id', $user->account_id)->first();
                if ($account) {
                    $notification = new Notification([
                        'seen' => 0,
                        'description' => "Your Property Is Accepted!",
                    ]);
                    $account->notifications()->save($notification);
                }
            } elseif ($property->owner_type == 'App\Models\Office') {
                $office = Office::where('id', $property->owner_id)->first();
                $account = Account::where('id', $office->account_id)->first();
                if ($account) {
                    $notification = new Notification([
                        'seen' => 0,
                        'description' => 'Your Property Is Accepted!',
                    ]);
                    $account->notifications()->save($notification);
                }
            }
        }
        $property->update([
            'accept_refuse' => $request->accept_refuse
        ]);
    }

    public function showProperty($property_id){

        $property = Property::find($property_id);

        return view('orders.showOrderDetails',['property'=>$property]);
    }


}
