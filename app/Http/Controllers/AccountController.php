<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Admin;
use App\Models\Img;
use App\Models\Office;
use App\Models\Region;
use App\Models\SubRecord;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class AccountController extends Controller
{
    public function update(Request $request)
    {

        if (isset($request->accountType) == 0) {
            $account_id = Auth::id();
        } else {
            $account_id = $request->account_id;
        }

        $validator = Validator::make($request->all(), [
            'firstName' => 'min:3',
            'lastName' => 'min:3',
            'email' => 'email',
            'phoneNumber' => 'max_digits:10|min_digits:10|numeric',
            'password' => 'min:8',
            'image' => 'image',
            'gender' => 'String',
            'birthdate' => 'Date',
            'officeName' => 'String',
            'telephone' => 'numeric|max_digits:10|min_digits:10'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'Status' => 'Failed',
                'Message' => $validator->errors(),
                'List' => null,
                'Code' => Response::HTTP_UNPROCESSABLE_ENTITY,
            ]);
        }
        $account = Account::with('img')->find($account_id);
        if (isset($account) == 1) {
            $account->update($request->all());
            if ($request->image) {
                if ($account->img) {
                    $baseUrl = 'http://192.168.1.107:8000/';
                    $path = $account->img->path;
                    $pathWithoutBaseUrl = str_replace($baseUrl, '', $path);
                    $desiredSubstring = 'public_html/images/accounts/' . basename($pathWithoutBaseUrl);
                    $destination = public_path($desiredSubstring);
                    if (File::exists($destination)) {
                        File::delete($destination);
                    }
                    $account->img()->delete();
                }
                $file_extension = $request->image->getClientOriginalExtension();
                $file_name = time() . '.' . $file_extension;
                $path = 'public_html/images/accounts';
                $request->image->move($path, $file_name);
                $img = new Img([
                    'path' => 'http://192.168.1.107:8000/' . $path . '/' . $file_name,
                ]);
                $account->img()->save($img);
            }
            $user = User::where('account_id', $account_id)->first();
            if (isset($user) == 1) {
                $user->update($request->all());
            } else {
                $user = Office::where('account_id', $account_id)->first();
                if ($user) {
                    $user->update($request->all());
                    if ($request->longitude && $request->latitude) {
                        if ($user->location) {
                            $user->location()->update([
                                'longitude' => $request->longitude,
                                'latitude' => $request->latitude,
                            ]);
                        } else {
                            $user->location()->create([
                                'longitude' => $request->longitude,
                                'latitude' => $request->latitude,
                            ]);
                        }
                    }
                } else {
                    $user = Admin::where('account_id', $account_id)->first();
                    if (isset($user) == 1) {
                        $user->update($request->all());
                    }
                }
            }

            if (isset($request->accountType) == 1) {
                return redirect()->route('accounts.index');
            }

            return response()->json([
                'Status' => 'Success',
                'Message' => 'User updated successfully',
                'account' => $account,
                'user' => $user,
                'Code' => Response::HTTP_OK,
            ]);
        }

        if (isset($request->accountType) == 1) {
            return redirect()->route('accounts.index');
        }

        return response()->json([
            'Status' => 'Failed',
            'Message' => 'You do not have permission to modify this account',
            'List' => null,
            'Code' => Response::HTTP_NOT_ACCEPTABLE,

        ]);

    }

    public function index()
    {
        $accounts = Account::with('user', 'office', 'admin')->get();

        $offices = Office::with('account')->get();

        $users = User::with('account')->get();

        $admin = Admin::with('account')->get();

        if (isset($accounts[0]) == 0) {
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'There are not any accounts'
            ]);
        }

        return response()->json([
            'Status' => 'Success',
            'Message' => 'Show All accounts',
//            'accounts' => $accounts,
            'offices' => $offices,
            'users' => $users,
            'admin' => $admin,
        ]);


    }


    public function show()
    {
        $account = Account::find(Auth::id());
        $account->makeHidden(['created_at', 'updated_at', 'id', 'img']);
        if (!isset($account)) {
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'Account does not exist',
                'List' => null,
                'Code' => Response::HTTP_NOT_ACCEPTABLE,
            ]);
        }
        $user = User::where('account_id', Auth::id())->first();
        if ($user) {
            $image = null;
            if ($account->img) {
                $image = $account->img->path;
            }
            $subrecord = SubRecord::where('user_id', $user->id)->orderBy('id', 'desc')->first();
            if ($subrecord) {
                $date = Carbon::parse($subrecord->date);
                $today = Carbon::today();
                $dayleft = $today->diffInDays($date);
                if ($subrecord->date <= Carbon::today()) {
                    $dayleft = 0;
                }
            } else {
                $dayleft = 0;
            }

            $accountt = $account->toArray();
            $accountt['blocked'] = (bool)$accountt['blocked'];
            $user->makeHidden(['created_at', 'updated_at', 'id', 'account_id']);
            $list = [
                'account' => $accountt,
                'user' => $user,
                'image' => $image,
                'day left to end Subscription' => $dayleft,
            ];

            return response()->json([
                'Status' => 'Success',
                'Message' => 'User information retrieved successfully',
                'List' => $list,
                'Code' => Response::HTTP_OK,
            ]);
        } elseif ($office = Office::where('account_id', Auth::id())->with(['region', 'region.governorate'])->first()) {
            $image = null;
            $location = null;
            if ($account->img) {
                $image = $account->img->path;
            }
            if ($office->location) {
                $location = $office->location;
                $location->makeHidden(['created_at', 'updated_at', 'id', 'locationable_id', 'locationable_type']);
            }
            $office->makeHidden(['created_at', 'updated_at', 'id', 'account_id', 'location', 'region', 'region_id']);
            $region = $office->region;
            $accountt = $account->toArray();
            $accountt['blocked'] = (bool)$accountt['blocked'];
            $list = [
                'account' => $accountt,
                'Office' => $office,
                'image' => $image,
                'region name' => $region->name,
                'governorate name' => $region->governorate->name,
                'location' => $location,
            ];
            return response()->json([
                'Status' => 'Success',
                'Message' => 'User information retrieved successfully',
                'List' => $list,
                'Code' => Response::HTTP_OK,
            ]);
        } elseif ($admin = Admin::where('account_id', Auth::id())->first()) {
            return response()->json([
                'Status' => 'Success',
                'Message' => 'User information retrieved successfully',
                'account' => $account,
                'admin' => $admin,
                'Code' => Response::HTTP_OK,
            ]);
        }
        return response()->json([
            'Status' => 'Failed',
            'Message' => 'User not found',
            'List' => null,
            'Code' => Response::HTTP_NOT_ACCEPTABLE,
        ]);
    }

    public function destroy($account_id)
    {

        $account = Account::with('user', 'office', 'admin', 'comments', 'wish_list', 'img')->find($account_id);

        if (isset($account) == 1) {

            if ($account->img) {
                $baseUrl = 'http://192.168.1.107:8000/';
                $path = $account->img->path;
                $pathWithoutBaseUrl = str_replace($baseUrl, '', $path);
                $desiredSubstring = 'public_html/images/accounts' . basename($pathWithoutBaseUrl);
                $destination = public_path($desiredSubstring);
                if (File::exists($destination)) {
                    File::delete($destination);
                }
                $account->img()->delete();
            }
            $account->delete();

        }
        return redirect()->route('accounts.index');

    }

    public function accountBlock(Request $request)
    {

        $account = Account::with('user', 'office', 'admin')->find($request->account_id);

        if (isset($account) == 1) {

            $account->update([
                'blocked' => $request->blocked
            ]);

            if ($request->blocked == true) {
                $message = 'account has been blocked successfully';
            } else
                $message = 'account has been unblocked successfully';


            return response()->json([
                'Status' => 'Success',
                'Message' => $message,
                'account' => $account
            ]);
        }

        return response()->json([
            'Status' => 'Failed',
            'Message' => 'Account not found',
        ]);

    }

    public function showAll()
    {
        $accounts = Account::with(['user', 'office', 'admin', 'img'])->latest()->paginate(5);

        return view('accounts.index', ['accounts' => $accounts]);
    }

    public function create()
    {

        $regions = Region::all();

        return view('accounts.create', ['regions' => $regions]);
    }

    public function showAccount($account_id)
    {

        $account = Account::with('user', 'office', 'admin', 'img', 'office.region')->find($account_id);

        if (isset($account->user) == 1) {
            $subrecord = SubRecord::where('user_id', $account->user->id)->orderBy('id', 'desc')->first();
            if ($subrecord) {
                $date = Carbon::parse($subrecord->date);
                $today = Carbon::today();
                $dayleft = $today->diffInDays($date);
                if ($subrecord->date <= Carbon::today()) {
                    $dayleft = 0;
                }
            } else {
                $dayleft = 0;
            }


            return view('accounts.show', ['account' => $account, 'dayleft' => $dayleft]);

        }

        return view('accounts.show', ['account' => $account]);

    }

    public function edit($account_id)
    {

        $account = Account::with('user', 'office', 'admin', 'img', 'office.region')->find($account_id);

        $regions = Region::all();

        return view('accounts.edit', ['account' => $account, 'regions' => $regions]);
    }

}
