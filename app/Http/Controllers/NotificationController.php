<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Notification;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class NotificationController extends Controller
{
    public function index()
    {
        $account = Account::where('id',Auth::id())->first();
        if (!$account) {
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'Office or User not Found',
                'List' => null,
                'Code' => Response::HTTP_NOT_ACCEPTABLE,
            ]);
        }
        $notifications = $account->notifications;
        $notifications->makeHidden(['created_at', 'updated_at','account_id']);
        $list = $notifications->map(function ($item) {
            $item->seen = $item->seen ? 'true' : 'false';
            $item->recived_at = $item->created_at->diffForHumans();
            return $item;
        });
        return response()->json([
            'Status' => 'Success',
            'Message' => "All Notifications",
            'List' => $list,
            'Code' => Response::HTTP_OK,
        ]);
    }

    public function update($NotificationId)
    {
        $notification = Notification::query()->where('id', $NotificationId)->first();
        if (!$notification) {
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'Notification not Found',
                'List' => null,
                'Code' => Response::HTTP_NOT_ACCEPTABLE,
            ]);
        }
        $notification->seen = 1;
        $notification->save();
        return response()->json([
            'Status' => 'Success',
            'Message' => "Seen Successfully",
            'List' => null,
            'Code' => Response::HTTP_OK,
        ]);
    }
}
