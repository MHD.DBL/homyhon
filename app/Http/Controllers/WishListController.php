<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\WishList;
use Illuminate\Http\Request;
use App\Models\Property;
use App\Models\property_wishList;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class WishListController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    // public function index()
    // {
    //     $account = Account::find(Auth::id());
    //     if ($account) {
    //         $wishList = $account->wish_list;

    //         if (!$wishList) {
    //             return response()->json([
    //                 'Message' => 'Wish list is empty',
    //                 'List' => null,
    //                 'Code' => Response::HTTP_OK,
    //             ]);
    //         }
    //         $properties = $wishList->properties()->with('region', 'region.governorate', 'imgs')->paginate(6);
    //         if ($properties->isEmpty()) {
    //             return response()->json([
    //                 'Message' => 'Wish list is empty',
    //                 'List' => null,
    //                 'Code' => Response::HTTP_OK,
    //             ]);
    //         }
    //         $list = $properties->map(function ($property) {
    //             return [
    //                 'id' => $property->id,
    //                 'price' => $property->price,
    //                 'propertyable_type' => $property->propertyable_type,
    //                 'rent_sale' => $property->rent_sale,
    //                 'region_name' => $property->region->name,
    //                 'governorate_name' => $property->region->governorate->name,
    //                 'first_image' => $property->imgs->first()->path ?? null,
    //             ];
    //         });
    //         $paginationData = [
    //             'total_pages' => $properties->lastPage(),
    //             'current_page' => $properties->currentPage(),
    //             'next_page_url' => $properties->nextPageUrl(),
    //             'prev_page_url' => $properties->previousPageUrl(),
    //         ];
    //         return response()->json([
    //             'Status' => 'Success',
    //             'Message' => 'All Properties Have Been Successfully Retrieved ',
    //             'List' => $list,
    //             'Code' => Response::HTTP_OK,
    //             'Pagination' => $paginationData,
    //         ]);
    //     }
    //     return response()->json([
    //         'Status' => 'Failed',
    //         'Message' => 'Office or User not Found',
    //         'List' => null,
    //         'Code' => Response::HTTP_NOT_ACCEPTABLE,
    //     ]);

    // }

public function index()
    {
        $account = Account::find(Auth::id());
        if ($account) {
            $wishList = $account->wish_list;

            if (!$wishList) {
                return response()->json([
                    'Status' => 'Success',
                    'Message' => 'Wish list is empty',
                    'List' => null,
                    'Code' => Response::HTTP_OK,
                ]);
            }
            $properties = $wishList->properties()->with('region', 'region.governorate', 'imgs')->paginate(300);
            if ($properties->isEmpty()) {
                return response()->json([
                    'Status' => 'Success',
                    'Message' => 'Wish list is empty',
                    'List' => null,
                    'Code' => Response::HTTP_OK,
                ]);
            }
            $list = $properties->map(function ($property) {
                return [
                    'id' => $property->id,
                    'price' => $property->price,
                    'propertyable_type' => class_basename($property->propertyable_type),
                    'rent_sale' => $property->rent_sale ? 'Rent' : 'Sell',
                    'region_name' => $property->region->name,
                    'governorate_name' => $property->region->governorate->name,
                    'first_image' => $property->imgs->first()->path ?? null,
                ];
            });
            $paginationData = [
                'total_pages' => $properties->lastPage(),
                'current_page' => $properties->currentPage(),
                'next_page_url' => $properties->nextPageUrl(),
                'prev_page_url' => $properties->previousPageUrl(),
            ];
            return response()->json([
                'Status' => 'Success',
                'Message' => 'All Properties Have Been Successfully Retrieved ',
                'List' => $list,
                'Code' => Response::HTTP_OK,
                'Pagination' => $paginationData,
            ]);
        }
        return response()->json([
            'Status' => 'Failed',
            'Message' => 'Office or User not Found',
            'List' => null,
            'Code' => Response::HTTP_NOT_ACCEPTABLE,
        ]);

    }


    // public function store($property_id)
    // {
    //     $account = Account::find(Auth::id());
    //     if (!$account->wish_list) {
    //         $wishlist = new Wishlist();
    //         $account->wish_list()->save($wishlist);
    //     } else {
    //         $wishlist = $account->wish_list;
    //     }
    //     $wishlist->properties()->toggle($property_id);
    //     return response()->json([
    //         'Status' => 'Success',
    //         'Message' => 'Property Added or Deleted To Wishlist Successfully',
    //         'List' => null,
    //         'Code' => Response::HTTP_OK,
    //     ]);
    // }

    public function store($property_id)
    {
        $account = Account::find(Auth::id());
        if (!$account->wish_list) {
            $wishlist = new Wishlist();
            $account->wish_list()->save($wishlist);
        } else {
            $wishlist = $account->wish_list;
        }
        $wishlistProperty=property_wishList::where('property_id',$property_id)->where('wishList_id',$wishlist->id)->first();
        if($wishlistProperty){
            $massage="Property Deleted From Wishlist Successfully";
        }else{
            $massage="Property Added To Wishlist Successfully";
        }
        $wishlist->properties()->toggle($property_id);
        return response()->json([
            'Status' => 'Success',
            'Message' => $massage,
            'List' => null,
            'Code' => Response::HTTP_OK,
        ]);
    }


}
