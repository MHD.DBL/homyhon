<?php

namespace App\Http\Controllers;

use App\Models\Governorate;
use App\Models\Img;
use App\Models\Office;
use App\Models\Property;
use App\Models\property_wishList;
use App\Models\Shop;
use App\Models\SubRecord;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use ReflectionClass;
use Symfony\Component\HttpFoundation\Response;

class ShopController extends Controller
{

    public function uploadShopImage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'image' => 'required|image'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'Status' => 'Failed',
                'Message' => $validator->errors(),
                'List' => null,
                'Code' => Response::HTTP_UNPROCESSABLE_ENTITY,
            ]);
        }
        $file_extension = $request->image->getClientOriginalExtension();
        $file_name = random_int(100000, 999999) . time() . '.' . $file_extension;
        $path = 'public_html/images/property/shop';
        $request->image->move($path, $file_name);
        return response()->json([
            'Status' => 'Success',
            'Message' => 'Image Uploaded.',
            'Link' => 'http://192.168.1.107:8000/' . $path . '/' . $file_name,
            'Code' => Response::HTTP_OK,
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required',
            'region_id' => 'required',
            'has_cladding' => 'required',
            'price' => 'required|numeric|min:0',
            'rent_sale' => 'required',
            'space' => 'required|numeric|min:0',
            'longitude' => 'required',
            'latitude' => 'required',
            'images' => 'required|array',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'Status' => 'Failed',
                'Message' => $validator->errors(),
                'List' => null,
                'Code' => Response::HTTP_UNPROCESSABLE_ENTITY,
            ]);
        }

        $user = User::where('account_id', Auth::id())->first();
        $office = Office::where('account_id', Auth::id())->first();

        if (!$user && !$office) {
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'Office or User not Found',
                'List' => null,
                'Code' => Response::HTTP_NOT_ACCEPTABLE,
            ]);
        }

        $reflection = new ReflectionClass($user ?? $office);

        $shop = Shop::create([
            'type' => $request->type,
        ]);

        $property = $shop->property()->create([
            'region_id' => $request->region_id,
            'owner_id' => $user->id ?? $office->id,
            'owner_type' => $reflection->getName(),
            'status' => $request->status,
            'accept_refuse' => null,
            'has_cladding' => $request->has_cladding,
            'description' => $request->description,
            'price' => $request->price,
            'rent_sale' => $request->rent_sale,
            'address' => $request->address,
            'monthly_yearly' => $request->monthly_yearly,
            'space' => $request->space,
        ]);

        $property->location()->create([
            'longitude' => $request->longitude,
            'latitude' => $request->latitude,
        ]);

        foreach ($request->images as $image) {
            $img = new Img([
                'path' => $image,
            ]);
            $property->imgs()->save($img);
        }

        if ($user) {
            $latestSubscription = SubRecord::where('user_id', $user->id)->latest()->first();
            $latestSubscription->remainingProperty = $latestSubscription->remainingProperty - 1;
            $latestSubscription->save();
        }

        return response()->json([
            'Status' => 'Success',
            'Message' => 'The property has been added successfully, pending approval from the admin',
            'List' => $property,
            'Code' => Response::HTTP_OK,
        ]);
    }

    public function deleteShopImages(Request $request)
    {
        if ($request->images == null) {
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'Images is null',
                'List' => null,
                'Code' => Response::HTTP_NOT_ACCEPTABLE,
            ]);
        }
        foreach ($request->images as $image) {
            $baseUrl = 'http://192.168.1.107:8000/';
            $pathWithoutBaseUrl = str_replace($baseUrl, '', $image);
            $desiredSubstring = 'public_html/images/property/shop/' . basename($pathWithoutBaseUrl);
            $destination = public_path($desiredSubstring);
            if (File::exists($destination)) {
                File::delete($destination);
            }
        }
        return response()->json([
            'Status' => 'Success',
            'Message' => 'Images have been deleted successfully',
            'List' => null,
            'Code' => Response::HTTP_OK,
        ]);
    }


    public function update(Request $request, $propertyid)
    {
        $property = Property::where('id', $propertyid)->where('propertyable_type', 'App\Models\Shop')->first();
        if (!$property) {
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'Property Not Found',
                'List' => null,
                'Code' => Response::HTTP_NOT_ACCEPTABLE,
            ]);
        }
        $shopid = $property->propertyable_id;
        $validator = Validator::make($request->all(), [
            'price' => 'numeric|min:0',
            'space' => 'numeric|min:0',
            'images' => 'array',
            'paths' => 'array',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'Status' => 'Failed',
                'Message' => $validator->errors(),
                'List' => null,
                'Code' => Response::HTTP_UNPROCESSABLE_ENTITY,
            ]);
        }

        $shop = Shop::find($shopid);
        if (!$shop) {
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'Shop Not Found',
                'List' => null,
                'Code' => Response::HTTP_NOT_ACCEPTABLE,
            ]);
        }
        $user = User::where('account_id', Auth::id())->first();
        $office = Office::where('account_id', Auth::id())->first();

        if (($user && $property->owner_id == $user->id && $property->owner_type == 'App\Models\User') ||
            ($office && $property->owner_id == $office->id && $property->owner_type == 'App\Models\Office')
        ) {
            $fieldsToUpdate = [
                'region_id', 'status', 'has_cladding', 'description',
                'price', 'rent_sale', 'address', 'monthly_yearly', 'space'
            ];

            foreach ($fieldsToUpdate as $field) {
                if ($request->has($field)) {
                    $property->$field = $request->$field;
                }
            }
            if ($request->type) {
                $shop->type = $request->type;
                $shop->save();
            }
            if ($request->longitude) {
                $property->location->longitude = $request->longitude;
                $property->location->save();
            }
            if ($request->latitude) {
                $property->location->latitude = $request->latitude;
                $property->location->save();
            }
            if ($request->paths) {
                foreach ($request->paths as $path) {
                    $baseUrl = 'http://192.168.1.107:8000/';
                    $pathWithoutBaseUrl = str_replace($baseUrl, '', $path);
                    $desiredSubstring = 'public_html/images/property/shop/' . basename($pathWithoutBaseUrl);
                    $destination = public_path($desiredSubstring);
                    if (File::exists($destination)) {
                        File::delete($destination);
                    }
                    if ($property->imgs()->where('path', $path)->first())
                        $property->imgs()->where('path', $path)->first()->delete();
                }
            }
            if ($request->images) {
                foreach ($request->images as $image) {
                    $img = new Img([
                        'path' => $image,
                    ]);
                    $property->imgs()->save($img);
                }
            }
        }


        $property->accept_refuse = null;
        $property->save();
        return response()->json([
            'Status' => 'Success',
            'Message' => 'The property has been updated successfully, pending approval from the admin',
            'List' => $property,
            'Code' => Response::HTTP_OK,
        ]);
    }


    /**
     * Remove the specified resource from storage.
     */
    public
    function destroy($propertyid)
    {
        $property = Property::where('id', $propertyid)->where('propertyable_type', 'App\Models\Shop')->first();
        if (!$property) {
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'Property Not Found',
                'List' => null,
                'Code' => Response::HTTP_NOT_ACCEPTABLE,
            ]);
        }
        $shopid = $property->propertyable_id;
        $shop = Shop::find($shopid);
        if (!$shop) {
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'Shop Not Found',
                'List' => null,
                'Code' => Response::HTTP_NOT_ACCEPTABLE,
            ]);
        }
        if ($property->contract) {
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'This Shop Has a Contract and You Cant Delete it.',
                'List' => null,
                'Code' => Response::HTTP_NOT_ACCEPTABLE,
            ]);
        }

        $user = User::where('account_id', Auth::id())->first();
        $office = Office::where('account_id', Auth::id())->first();

        if (($user && $property->owner_id == $user->id && $property->owner_type == 'App\Models\User') ||
            ($office && $property->owner_id == $office->id && $property->owner_type == 'App\Models\Office')
        ) {
            $property->location()->delete();
            foreach ($property->imgs()->get() as $path) {
                $baseUrl = 'hhttp://192.168.1.107:8000/';
                $pathWithoutBaseUrl = str_replace($baseUrl, '', $path->path);
                $desiredSubstring = 'public_html/images/property/shop/' . basename($pathWithoutBaseUrl);
                $destination = public_path($desiredSubstring);
                if (File::exists($destination)) {
                    File::delete($destination);
                }
            }
            $wishlist = property_wishList::where('property_id', $propertyid)->get();
            foreach ($wishlist as $wish_list) {
                $wish_list->delete();
            }
            $property->imgs()->delete();
            $property->comments()->delete();
            $property->delete();
            $shop->delete();
            return response()->json([
                'Status' => 'Success',
                'Message' => 'Property Deleted Successfully',
                'List' => null,
                'Code' => Response::HTTP_OK,
            ]);
        } else {
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'You cannot delete this property',
                'List' => null,
                'Code' => Response::HTTP_NOT_ACCEPTABLE,
            ]);
        }
    }
}
