<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\GovernorateController;
use App\Models\Account;
use App\Models\Admin;
use App\Models\Governorate;
use App\Models\Img;
use App\Models\Office;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use PHPOpenSourceSaver\JWTAuth\JWT;

class AuthController extends Controller
{

    public function register(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'firstName' => 'required|min:3',
            'lastName' => 'required|min:3',
            'email' => 'required|email|unique:accounts',
            'phoneNumber' => 'required|min:10|max:10|unique:accounts',
            'password' => 'required|min:8'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'Status' => 'Failed',
                'Message' => $validator->errors(),
                'List' => null,
                'Code' => Response::HTTP_UNPROCESSABLE_ENTITY,
            ]);
        }

        $account = Account::create([
            'firstName' => $request->firstName,
            'lastName' => $request->lastName,
            'email' => $request->email,
            'phoneNumber' => $request->phoneNumber,
            'password' => $request->password
        ]);

        $user = User::create([
            'account_id' => $account->id
        ]);

        Wallet::create([
            'user_id' => $user->id
        ]);

        $token = Auth::login($account);

        return response()->json([
            'Status' => 'Success',
            'Message' => 'User registered successfully',
            'Code' => Response::HTTP_OK,
            'account' => $account,
            'token' => $token,

        ]);

    }


    public function login(Request $request)
    {

        $request->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        $credentials = $request->only('email', 'password');

        $token = Auth::attempt($credentials);

        if (!$token) {
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'Log in failed , there is wrong in email or password ',
                'Code' => Response::HTTP_NOT_ACCEPTABLE,

            ]);
        }


        $user = Account::with('user','office','admin')->where('id',Auth::user()->getAuthIdentifier())->first();

        if(isset($user->admin) == 1 ){
            return response()->json([
                'Status' => 'Success',
                'Message' => 'User logged in successfully',
                'Code' => Response::HTTP_OK,
                'accounts' => $user,
                'token' => $token,
                'FirstName' => $user->firstName,
                'log' => 'web'
            ]);
        }

        return response()->json([
            'Status' => 'Success',
            'Message' => 'User logged in successfully',
            'Code' => Response::HTTP_OK,
            'accounts' => $user,
            'token' => $token,
            'log' => 'api'
        ]);

    }

    public function createAccount(Request $request)
    {
        $request->validate([
            'firstName' => 'required|min:3',
            'lastName' => 'required|min:3',
            'email' => 'required|email',
            'phoneNumber' => 'required|min:10|max:10',
            'password' => 'required|min:8',
            'accountType' => 'required'
        ]);

        if ($request->accountType == "office") {

            $request->validate([
                'region_id' => 'required',
                'address' => 'required',
                'officeName' => 'required',
                'telephone' => 'required|min:10|max:10'
            ]);

        } else if ($request->accountType == "admin") {

            $request->validate([
                'role' => 'required'
            ]);

        }


        $account = Account::create([
            'firstName' => $request->firstName,
            'lastName' => $request->lastName,
            'phoneNumber' => $request->phoneNumber,
            'email' => $request->email,
            'password' => $request->password
        ]);


        if(isset($request->image) == 1){
        $file_extension = $request->image->getClientOriginalExtension();
        $file_name = time() . '.' . $file_extension;
        $path = 'public_html/images/accounts';
        $request->image->move($path, $file_name);
        $img = new Img([
            'path' => 'http://192.168.1.107:8000/' . $path . '/' . $file_name,
        ]);
        $account->img()->save($img);
        }


        if ($request->accountType == "office") {

            Office::create([
                'region_id' => $request->region_id,
                'account_id' => $account->id,
                'address' => $request->address,
                'officeName' => $request->officeName,
                'telephone' => $request->telephone,
                'description' => $request->description ?? null,
            ]);

        } elseif ($request->accountType == "admin") {

            Admin::create([
                'role' => $request->role,
                'account_id' => $account->id
            ]);

        } else {

            $user = User::create([
                'account_id' => $account->id,
                'gender' => $request->gender,
                'birthdate'  => $request->birthdate
            ]);

            Wallet::create([
                'user_id' => $user->id
            ]);
        }

        $token = Auth::login($account);

        return redirect()->route('accounts.index');

    }



    public function logout()
    {
        Auth::guard('api')->logout();

        return response()->json([
            'Status' => 'Success',
            'Message' => 'User logout successfully',
            'Code' => Response::HTTP_OK,
            ], 200);
    }

}
