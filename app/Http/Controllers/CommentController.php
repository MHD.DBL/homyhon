<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Comment;
use App\Models\Notification;
use App\Models\Office;
use App\Models\Property;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class CommentController extends Controller
{

    public function index($property_id)
    {
        $property = Property::find($property_id);
        if ($property) {
            $comments = Comment::with(['account', 'account.img'])->where('property_id', $property_id)->get();

            $comments->makeHidden(['created_at', 'updated_at', 'property_id']);
            $list = $comments->map(function ($item) {
                $item->account_name = $item->account->firstName . " " . $item->account->lastName;
                $item->account_id = $item->account_id;
                $item->account_img = $item->account->img->path ?? null;
                $item->post_at = $item->created_at->diffForHumans();
                unset($item->account);
                unset($item->img);
                return $item;
            });
            return response()->json([
                'Status' => 'Success',
                'Message' => 'All Comments Have Been Successfully Retrieved',
                'List' => $list,
                'Code' => Response::HTTP_OK,
            ]);
        }
    }


       public function store($property_id, Request $request)
    {
        $bigaccount = Account::where('id', Auth::id())->first();
        $property = Property::query()->find($property_id);
        $validator = Validator::make($request->all(), [
            'description' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'Status' => 'Failed',
                'Message' => $validator->errors(),
                'List' => null,
                'Code' => Response::HTTP_UNPROCESSABLE_ENTITY,
            ]);
        }
        $comment = $property->comments()->create([
            'content' => $request->description,
            'account_id' => Auth::id(),
        ]);
        if ($property->owner_type == 'App\Models\User') {
            $user = User::where('id', $property->owner_id)->first();
            $account = Account::where('id', $user->account_id)->first();
            if ($account) {
                $notification = new Notification([
                    'seen' => 0,
                    'description' => $bigaccount->firstName . " " . $bigaccount->lastName . " Add New Comment To Your Property!",
                ]);
                $account->notifications()->save($notification);
            }
        } elseif ($property->owner_type == 'App\Models\Office') {
            $office = Office::where('id', $property->owner_id)->first();
            $account = Account::where('id', $office->account_id)->first();
            if ($account) {
                $notification = new Notification([
                    'seen' => 0,
                    'description' => $bigaccount->firstName . ' ' . $bigaccount->lastName . ' Add New Comment To Your Property!',
                ]);
                $account->notifications()->save($notification);
            }
        }
        $comment->makeHidden(['created_at', 'updated_at']);
        return response()->json([
            'Status' => 'Success',
            'Message' => 'Comment Added Successfully',
            'List' => $comment,
            'Code' => Response::HTTP_OK,
        ]);
    }

    public function update(Request $request, $comment_id)
    {
        $comment = Comment::query()->find($comment_id);
        if ($comment->account_id == Auth::id()) {
            $comment->update($request->all());
            $comment->makeHidden(['created_at', 'updated_at']);
            return response()->json([
                'Status' => 'Success',
                'Message' => 'Comment Updated Successfully',
                'List' => $comment,
                'Code' => Response::HTTP_CREATED,
            ]);
        } else {
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'That is not your comment',
                'List' => null,
                'Code' => Response::HTTP_NOT_ACCEPTABLE,
            ]);
        }
    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy($comment_id)
    {
        $comment = Comment::find($comment_id);
        if ($comment->account_id == Auth::id()) {
            $comment->delete();
            return response()->json([
                'Status' => 'Success',
                'Message' => 'Comment Deleted Successfully',
                'List' => null,
                'Code' => Response::HTTP_OK,
            ]);
        } else {
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'That is not your comment',
                'List' => null,
                'Code' => Response::HTTP_NOT_ACCEPTABLE,
            ]);
        }
    }
}
