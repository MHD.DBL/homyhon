<?php

namespace App\Http\Controllers;

use App\Models\SubRecord;
use App\Models\Subscription;
use App\Models\User;
use App\Models\Wallet;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;
use function PHPUnit\Framework\isEmpty;

class SubscriptionController extends Controller
{

    // public function subscribe(Request $request)
    // {
    //     $user = User::where('account_id', Auth::id())->first();
    //     if (isset($user) == 0) {
    //         return response()->json([
    //             'Status' => 'Failed',
    //             'Message' => 'User not found',
    //             'List' => null,
    //             'Code' => Response::HTTP_NOT_ACCEPTABLE,
    //         ]);
    //     }
    //     $subscription = Subscription::find($request->subscription_id);
    //     if (isset($subscription) == 0) {
    //         return response()->json([
    //             'Status' => 'Failed',
    //             'Message' => 'Subscription not found',
    //             'List' => null,
    //             'Code' => Response::HTTP_NOT_ACCEPTABLE,
    //         ]);
    //     }
    //     $subRecord = SubRecord::where('user_id', $user->id)->latest()->first();
    //     if (isset($subRecord) == 1) {
    //         if ($subRecord->date >= Carbon::now()->format('Y-m-d')) {
    //             return response()->json([
    //                 'Status' => 'Failed',
    //                 'Message' => 'You are already subscribed',
    //                 'List' => null,
    //                 'Code' => Response::HTTP_NOT_ACCEPTABLE,
    //             ]);
    //         }
    //     }
    //     $wallet = Wallet::where('user_id', $user->id)->first();
    //     if ($wallet) {
    //         $value = $wallet->amount - $subscription->price;
    //         if ($value >= 0) {
    //             $wallet->update([
    //                 'amount' => $value
    //             ]);
    //             $subscription = SubRecord::create([
    //                 'subscription_id' => $request->subscription_id,
    //                 'date' => Carbon::now()->addDays((int)$subscription->period)->format('Y-m-d'),
    //                 'user_id' => $user->id
    //             ]);
    //             return response()->json([
    //                 'Status' => 'Success',
    //                 'Message' => 'Subscription completed successfully',
    //                 'List' => $subscription,
    //                 'Code' => Response::HTTP_OK,
    //             ]);
    //         }
    //         return response()->json([
    //             'Status' => 'Failed',
    //             'Message' => 'The amount in your wallet is not enough to pay the subscription cost',
    //             'subscription' => $subscription,
    //             'wallet' => $wallet,
    //             'Code' => Response::HTTP_NOT_ACCEPTABLE,
    //         ]);
    //     }
    //     return response()->json([
    //         'Status' => 'Failed',
    //         'Message' => 'The amount in your wallet is not enough to pay the subscription cost',
    //         'List' => null,
    //         'Code' => Response::HTTP_NOT_ACCEPTABLE,
    //     ]);
    // }
    
    
    
    public function showMySub()
    {
        $user = User::where('account_id', Auth::id())->first();
        $subRecord = SubRecord::where('user_id', $user->id)->orderBy('id', 'desc')->first();
        if (!$subRecord) {
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'You Are not Subscribe Yet',
                'List' => null,
                'Code' => Response::HTTP_NOT_ACCEPTABLE,
            ]);
        }
        $subRecord->makeHidden(['created_at', 'updated_at', 'id', 'user_id', 'subscription_id']);
        if ($subRecord->date >= Carbon::now()->format('Y-m-d')) {
            return response()->json([
                'Status' => 'Success',
                'Message' => 'Your Subscription',
                'List' => $subRecord,
                'Code' => Response::HTTP_OK,
            ]);
        } else {
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'Your subscription has expired.',
                'List' => null,
                'Code' => Response::HTTP_NOT_ACCEPTABLE,
            ]);
        }
    }
    
    public function subscribe(Request $request)
    {
        $user = User::where('account_id', Auth::id())->first();
        if (isset($user) == 0) {
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'User not found',
                'List' => null,
                'Code' => Response::HTTP_NOT_ACCEPTABLE,
            ]);
        }
        $subscription = Subscription::find($request->subscription_id);
        if (isset($subscription) == 0) {
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'Subscription not found',
                'List' => null,
                'Code' => Response::HTTP_NOT_ACCEPTABLE,
            ]);
        }
        $subRecord = SubRecord::where('user_id', $user->id)->orderBy('id', 'desc')->first();
        if (isset($subRecord) == 1) {
            if ($subRecord->date >= Carbon::now()->format('Y-m-d')) {
                return response()->json([
                    'Status' => 'Failed',
                    'Message' => 'You are already subscribed',
                    'List' => null,
                    'Code' => Response::HTTP_NOT_ACCEPTABLE,
                ]);
            }
        }
        $wallet = Wallet::where('user_id', $user->id)->first();
        if ($wallet) {
            $value = $wallet->amount - $subscription->price;
            if ($value >= 0) {
                $wallet->update([
                    'amount' => $value
                ]);
                $subscription = SubRecord::create([
                    'subscription_id' => $request->subscription_id,
                    'date' => Carbon::now()->addDays((int)$subscription->period)->format('Y-m-d'),
                    'remainingProperty' => $subscription->propertyNumber,
                    'user_id' => $user->id
                ]);
                return response()->json([
                    'Status' => 'Success',
                    'Message' => 'Subscription completed successfully',
                    'List' => $subscription,
                    'Code' => Response::HTTP_OK,
                ]);
            }
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'The amount in your wallet is not enough to pay the subscription cost',
                'List' => null,
                'Code' => Response::HTTP_NOT_ACCEPTABLE,
            ]);
        }
        return response()->json([
            'Status' => 'Failed',
            'Message' => 'The amount in your wallet is not enough to pay the subscription cost',
            'List' => null,
            'Code' => Response::HTTP_NOT_ACCEPTABLE,
        ]);
    }


    public function showAllSubRecord()
    {
        $user = User::where('account_id', Auth::id())->first();
        $subRecord = $user->subscriptions()->get();

        $subRecord->transform(function ($item) {
            return [
                'name' => $item->name,
                'period' => $item->period,
                'end in' => $item->pivot->date,
            ];
        });

        if ($subRecord->isEmpty()) {
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'You are not subscribed in the app before',
                'List' => null,
                'Code' => Response::HTTP_NOT_ACCEPTABLE,
            ]);
        }

        return response()->json([
            'Status' => 'Success',
            'Message' => 'All previous subscriptions',
            'List' => $subRecord,
            'Code' => Response::HTTP_OK,
        ]);
    }



    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $subscriptions = Subscription::all();
        if (isset($subscriptions[0]) == 1) {
            $subscriptions->makeHidden(['created_at', 'updated_at']);
            return response()->json([
                'Status' => 'Success',
                'Message' => 'All Subscriptions',
                'List' => $subscriptions,
                'Code' => Response::HTTP_OK,
            ]);
        }
        return response()->json([
            'Status' => 'Failed',
            'Message' => 'There are no Subscriptions',
            'List' => null,
            'Code' => Response::HTTP_NOT_ACCEPTABLE,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        Subscription::create([
            'price' => $request->price,
            'name' => $request->name,
            'period' => $request->period,
            'propertyNumber' => $request->propertyNumber
        ]);

        return redirect()->route('subscriptions.index');

    }

    /**
     * Display the specified resource.
     */
    public function show($subscription_id)
    {
        $subscription = Subscription::find($subscription_id);

        if (isset($subscription)) {
            $subscription->makeHidden(['created_at', 'updated_at']);
        }

        return view('subscriptions.show',['subscription'=>$subscription]);

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function update(Subscription $subscription, $subscription_id, Request $request)
    {
        $subscription = Subscription::find($subscription_id);

        if (isset($subscription)) {

            $subscription->update($request->all());

        }

        return redirect()->route('subscriptions.index');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($subscription_id)
    {
        $subscription = Subscription::find($subscription_id);

        if (isset($subscription)) {

            $subscription->delete();

        }

        return redirect()->route('subscriptions.index');

    }

    public function showAll(){

        $subscriptions = Subscription::latest()->paginate(5);

        return view('subscriptions.index',['subscriptions'=>$subscriptions]);
    }


    public function edit($subscription_id)
    {
        $subscription = Subscription::find($subscription_id);

        return view('subscriptions.edit',['subscription'=>$subscription]);

    }
}
