<?php

namespace App\Http\Controllers;

use App\Charts\AreaChart;
use App\Charts\DonutChart;
use App\Charts\IncomeChart;
use App\Charts\PolarAreaChart;
use App\Models\Admin;
use App\Models\House;
use App\Models\Office;
use App\Models\Property;
use App\Models\SubRecord;
use App\Models\Subscription;
use App\Models\User;
use App\Charts\ExpensesChart;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ChartJSController extends Controller
{
    public function index(ExpensesChart $expensesChart, IncomeChart $incomeChart,DonutChart $donutChart,AreaChart $areaChart,PolarAreaChart $polarAreaChart)
    {

        $subscriptions = Subscription::get();
        $SubRecord = [];

        $item = 0;
        $totalSub = 0;

        foreach ($subscriptions as $subscription) {
            $SubRecord[$item]=[
                'Count' => SubRecord::where('subscription_id',$subscription->id)->get()->count(),
                'SubName' => $subscription->name
            ];
            $totalSub = $totalSub + $SubRecord[$item]['Count'];
            $item++;
        }

        $admin = Admin::get()->count();
        $user = User::get()->count();
        $office = Office::get()->count();

        $accepted = Property::where('accept_refuse','1')->get()->count();
        $refused = Property::where('accept_refuse','0')->get()->count();
        $pending = Property::where('accept_refuse',null)->get()->count();

        return view('charts.charts', [
            'chart' => $incomeChart->build(),
            'chart1' => $donutChart->build(),
            'chart2' => $polarAreaChart->build(),
            'chart3' => $expensesChart->build(),
            'chart4' => $areaChart->build(),
            'accepted' => $accepted,
            'refused' => $refused,
            'pending' => $pending,
            'office' => $office,
            'admin' => $admin,
            'user' => $user,
            'SubRecord'=>$SubRecord,
            'TotalSub'=>$totalSub,
        ]);
    }
}
