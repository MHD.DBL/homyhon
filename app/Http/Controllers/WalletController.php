<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WalletController extends Controller
{

    public function index()
    {
        $wallets = Wallet::with('user.account')->latest()->paginate(6);

        return view('wallets.index',['wallets'=>$wallets]);
    }

    public function show()
    {
        $wallet = Wallet::with('user')->find(Auth::id());

        if($wallet){

            return response()->json([
                'code' => '200',
                'Status' => 'Success',
                'Message'  => 'View user with his/her wallet',
                'wallet' => $wallet
            ]);
        }
        return response()->json([
            'code' => '400',
            'Status' => 'Failed',
            'Message'  => 'Wallet not found',
        ]);
    }

    public function chargeWallet(Request $request)
    {
        $wallet = Wallet::with('user')->find($request->wallet_id);

        if($wallet){

            $wallet->update([
                'amount' => $wallet->amount + $request->amount
            ]);

            return response()->json([
                'code' => '200',
                'Status' => 'Success',
                'Message'  => 'Wallet has been charged successfully',
//                'wallet' => $wallet
            ]);
        }
        return response()->json([
            'code' => '400',
            'Status' => 'Failed',
            'Message'  => 'Wallet not found',
        ]);
    }

    public function destroy($wallet_id)
    {
        $wallet = Wallet::find($wallet_id);

        if($wallet){

            $wallet->delete();

            return response()->json([
                'code' => '200',
                'Status' => 'Success',
                'Message'  => 'Wallet has been deleted successfully',
                'wallet' => $wallet
            ]);
        }
        return response()->json([
            'code' => '400',
            'Status' => 'Failed',
            'Message'  => 'Wallet not found',
        ]);
    }

    public function create(Request $request){

        $user = User::find($request->user_id);

        if($user){

            $wallet = Wallet::where('user_id',$request->user_id)->first();

            if($wallet){
                return response()->json([
                    'code' => '400',
                    'Status' => 'Failed',
                    'Message'  => 'Wallet is already exist',
                ]);
            }

            $wallet = Wallet::with('user')->create([
                'user_id' => $user->id
            ]);

            return response()->json([
                'code' => '200',
                'Status' => 'Success',
                'Message'  => 'Wallet has been created successfully',
                'wallet' => $wallet
            ]);

        }
        return response()->json([
            'code' => '400',
            'Status' => 'Failed',
            'Message'  => 'User not found',
        ]);

    }

}
