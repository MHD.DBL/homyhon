<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use App\Models\Img;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class AdController extends Controller
{

    public function orderDesc()
    {
        $pagination = Ad::query()
            ->select('id', 'link', 'companyName','description')
            ->whereHas('img', function ($query) {
                $query->where('imgable_type', 'App\Models\Ad');
            })
            ->orderBy('id','desc')->take(5)->get();

        $ads = $pagination->map(function ($item) {
            $item->image = $item->img->path;
            unset($item->img);
            return $item;
        });

        return response()->json([
            'Status' => 'Success',
            'Message' => 'Show All ads',
            'List' => $ads,
            'Code' => Response::HTTP_OK,
        ]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pagination = Ad::query()
            ->select('id', 'link', 'companyName','description')
            ->whereHas('img', function ($query) {
                $query->where('imgable_type', 'App\Models\Ad');
            })
            ->paginate(300);

        $ads = $pagination->getCollection()->map(function ($item) {
            $item->image = $item->img->path;
            unset($item->img);
            return $item;
        });
        $paginationData = [
            'total_pages' => $pagination->lastPage(),
            'current_page' => $pagination->currentPage(),
            'next_page_url' => $pagination->nextPageUrl(),
            'prev_page_url' => $pagination->previousPageUrl(),
        ];
        return response()->json([
            'Status' => 'Success',
            'Message' => 'Show All ads',
            'List' => $ads,
            'Code' => Response::HTTP_OK,
            'Pagination' => $paginationData,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $ad = Ad::create([
            'description' => $request->description,
            'link' => $request->link,
            'companyName' => $request->companyName
        ]);
        $file_extension = $request->image->getClientOriginalExtension();
        $file_name = time() . '.' . $file_extension;
        $path = 'public_html/images/ad';
        $request->image->move($path, $file_name);
        $img = new Img([
            'path' => 'http://192.168.1.107:8000/' . $path . '/' . $file_name,
        ]);
        $ad->img()->save($img);
        return redirect()->route('ads.index');

    }

    /**
     * Display the specified resource.
     */
    public function show($ad_id)
    {
        $ad = Ad::find($ad_id);
        if (isset($ad) == 1) {
            $image = $ad->img->first()->path;
            $ad->makeHidden(['created_at', 'updated_at', 'img']);
            $list = [
                'ad' => $ad,
                'image' => $image,
            ];
            return response()->json([
                'Status' => 'Success',
                'Message' => 'Show Ad',
                'List' => $list,
                'Code' => Response::HTTP_OK,
            ]);
        }
        return response()->json([
            'Status' => 'Failed',
            'Message' => 'Ad not found',
            'List' => null,
            'Code' => Response::HTTP_NOT_ACCEPTABLE,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $ad_id)
    {
        $ad = Ad::find($ad_id);
        if (isset($ad)) {
            if ($request->image) {
                $baseUrl = 'http://192.168.1.107:8000/';
                $path = $ad->img->path;
                $pathWithoutBaseUrl = str_replace($baseUrl, '', $path);
                $desiredSubstring = 'public_html/images/ad/' . basename($pathWithoutBaseUrl);
                $destination = public_path($desiredSubstring);
                if (File::exists($destination)) {
                    File::delete($destination);
                }
                $ad->img()->delete();
                $file_extension = $request->image->getClientOriginalExtension();
                $file_name = time() . '.' . $file_extension;
                $path = 'public_html/images/ad';
                $request->image->move($path, $file_name);
                $img = new Img([
                    'path' => 'http://192.168.1.107:8000/' . $path . '/' . $file_name,
                ]);
                $ad->img()->save($img);
            }
            $ad->update($request->all());

        }

        return redirect()->route('ads.index');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($ad_id)
    {
        $ad = Ad::find($ad_id);
        if (isset($ad)) {
            $baseUrl = 'hhttp://192.168.1.107:8000/';
            $path = $ad->img->path;
            $pathWithoutBaseUrl = str_replace($baseUrl, '', $path);
            $desiredSubstring = 'public_html/images/ad/' . basename($pathWithoutBaseUrl);
            $destination = public_path($desiredSubstring);
            if (File::exists($destination)) {
                File::delete($destination);
            }
            $ad->img()->delete();
            $ad->delete();

        }

        return redirect()->route('ads.index');

    }

    public function showAll(){

        $ads = Ad::with('img')->latest()->paginate(5);

        return view('ads.index',['ads'=>$ads]);
    }

    public function showAd($ad_id){

        $ad = Ad::with('img')->find($ad_id);

        return view('ads.show',['ad'=>$ad]);
    }

    public function editAd($ad_id){

        $ad = Ad::with('img')->find($ad_id);

        return view('ads.edit',['ad'=>$ad]);
    }

}
