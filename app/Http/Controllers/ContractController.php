<?php

namespace App\Http\Controllers;

use App\Models\Contract;
use App\Models\Office;
use App\Models\Account;
use Illuminate\Http\Request;
use App\Models\Notification;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class ContractController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    // public function index()
    // {
    //     $office = Office::where('account_id', Auth::id())->first();

    //     if ($office) {
    //         $contracts = $office->contracts;
    //         $contracts->makeHidden(['created_at', 'updated_at', 'property_id', 'office_id']);

    //         $perPage = 6;
    //         $currentPage = request()->input('page', 1);
    //         $offset = ($currentPage - 1) * $perPage;

    //         $list = $contracts->slice($offset, $perPage)->values();

    //         $paginationData = new LengthAwarePaginator(
    //             $list,
    //             $contracts->count(),
    //             $perPage,
    //             $currentPage,
    //             ['path' => request()->url()]
    //         );

    //         $paginationData->appends(request()->query());

    //         $result = [
    //             'Status' => 'Success',
    //             'Message' => 'All Contracts Have Been Successfully Retrieved',
    //             'List' => $list,
    //             'Code' => Response::HTTP_OK,
    //             'Pagination' => [
    //                 'total_pages' => $paginationData->lastPage(),
    //                 'current_page' => $paginationData->currentPage(),
    //                 'next_page_url' => $paginationData->nextPageUrl(),
    //                 'prev_page_url' => $paginationData->previousPageUrl(),
    //             ],
    //         ];

    //         return response()->json($result);
    //     }

    //     return response()->json([
    //         'Status' => 'Failed',
    //         'Message' => 'Office not Found',
    //         'List' => null,
    //         'Code' => Response::HTTP_NOT_ACCEPTABLE,
    //     ]);
    // }


    public function index()
    {
        $office = Office::where('account_id', Auth::id())->first();

        if ($office) {
            $contracts = $office->contracts;
            $contracts->makeHidden(['created_at', 'updated_at', 'office_id']);

            $perPage = 6;
            $currentPage = request()->input('page', 1);
            $offset = ($currentPage - 1) * $perPage;

            $list = $contracts->slice($offset, $perPage)->values();

            $paginationData = new LengthAwarePaginator(
                $list,
                $contracts->count(),
                $perPage,
                $currentPage,
                ['path' => request()->url()]
            );

            $paginationData->appends(request()->query());

            $result = [
                'Status' => 'Success',
                'Message' => 'All Contracts Have Been Successfully Retrieved',
                'List' => $list,
                'Code' => Response::HTTP_OK,
                'Pagination' => [
                    'total_pages' => $paginationData->lastPage(),
                    'current_page' => $paginationData->currentPage(),
                    'next_page_url' => $paginationData->nextPageUrl(),
                    'prev_page_url' => $paginationData->previousPageUrl(),
                ],
            ];

            return response()->json($result);
        }

        return response()->json([
            'Status' => 'Failed',
            'Message' => 'Office not Found',
            'List' => null,
            'Code' => Response::HTTP_NOT_ACCEPTABLE,
        ]);
    }


    public function store(Request $request)
    {
        $office = Office::where('account_id', Auth::id())->first();
        if(!$office){
            return response()->json([
                'Status' => 'Failed',
                'Message' => 'Office not Found',
                'List' => null,
                'Code' => Response::HTTP_NOT_ACCEPTABLE,
            ]);
        }
        $validator = Validator::make($request->all(), [
            'name_first_party' => 'required',
            'phone_number_FP' => 'required|max_digits:10|min_digits:10|numeric',
            'ratio' => 'required|min:0|max:100',
            'property_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'Status' => 'Failed',
                'Message' => $validator->errors(),
                'List' => null,
                'Code' => Response::HTTP_UNPROCESSABLE_ENTITY,
            ]);
        }
        $contract = $office->contracts()->create([
            'property_id' => $request->property_id,
            'name_first_party' => $request->name_first_party,
            'phone_number_FP' => $request->phone_number_FP,
            'ratio' => $request->ratio,
            'accept_refuse' => null
        ]);
        return response()->json([
            'Status' => 'Success',
            'Message' => 'Contract Added Successfully',
            'List' => $contract,
            'Code' => Response::HTTP_OK,
        ]);
    }



    public function show($contractid)
    {
        $office = Office::where('account_id', Auth::id())->first();
        if ($office) {
            $contract = $office->contracts()->find($contractid);
            if ($contract) {
                $contract->makeHidden(['created_at', 'updated_at', 'property_id', 'office_id']);
                $images = $contract->property->imgs->pluck('path')->toArray();
                $location = $contract->property->location;
                $location->makeHidden(['created_at', 'updated_at', 'id', 'locationable_id', 'locationable_type']);
                $property = [
                    'propertyable_type' => class_basename($contract->property->propertyable_type),
                    'status' => $contract->property->status,
                    'has_cladding' => $contract->property->has_cladding,
                    'description' => $contract->property->description,
                    'price' => $contract->property->price,
                    'rent_sale' => $contract->property->rent_sale ? 'Rent' : 'Sell',
                    'address' => $contract->property->address,
                    'space' => $contract->property->space,
                    'region_name' => $contract->property->region->name,
                    'governorate_name' => $contract->property->region->governorate->name,
                ];
                if (!is_null($contract->property->monthly_yearly)) {
                    $property['monthly_yearly'] = $contract->property->monthly_yearly ? 'Monthly' : 'Yearly';
                }
                $list = [
                    'id' => $contract->id,
                    'name_first_party' => $contract->name_first_party,
                    'phone_number_FP' => $contract->phone_number_FP,
                    'ratio' => $contract->ratio,
                    'accept_refuse contract' => $contract->accept_refuse,
                    'property' => $property,
                    'images' => $images,
                    'location' => $location
                ];

                return response()->json([
                    'Status' => 'Success',
                    'Message' => 'Contract Have Been Successfully Retrieved',
                    'List' => $list,
                    'Code' => Response::HTTP_OK,
                ]);
            }
        }

        return response()->json([
            'Status' => 'Failed',
            'Message' => 'Contract not found',
            'List' => null,
            'Code' => Response::HTTP_NOT_ACCEPTABLE,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function showContract($contract_id)
    {
        $contract = Contract::query()->with(['office','property','property.imgs','office.account'])->find($contract_id);

        return view('contracts.show',['contract'=>$contract]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function acceptRefuse(Request $request)
    {
        $contract = Contract::query()->find($request->contract_id);
        if ($request->accept_refuse == 0) {
            $office = Office::where('id', $contract->office_id)->first();
            $account = Account::where('id', $office->account_id)->first();
            if ($account) {
                $notification = new Notification([
                    'seen' => 0,
                    'description' => 'Your Contract Is Rejected!',
                ]);
                $account->notifications()->save($notification);
            }

        } elseif ($request->accept_refuse == 1) {
            $office = Office::where('id', $contract->office_id)->first();
            $account = Account::where('id', $office->account_id)->first();
            if ($account) {
                $notification = new Notification([
                    'seen' => 0,
                    'description' => 'Your Contract Is Accepted!',
                ]);
                $account->notifications()->save($notification);
            }

        }
        $contract->update([
            'accept_refuse' => $request->accept_refuse
        ]);

    }

    /**
     * Remove the specified resource from storage.
     */
    public function showAllOrdersContract()
    {
        $contracts = Contract::query()
            ->with(['office','property','property.imgs','office.account'])
            ->where('accept_refuse', null)->orWhere('accept_refuse', '0')
            ->paginate(5);

        return view('contracts.index',['contracts'=>$contracts]);
    }


}
