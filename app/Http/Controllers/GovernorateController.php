<?php

namespace App\Http\Controllers;

use App\Models\Governorate;
use App\Models\Region;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class GovernorateController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $governorates = [
            'Aleppo',
            'Damascus',
            'Daraa',
            'Deir ez-Zor',
            'Hama',
            'Hasakah',
            'Homs',
            'Idlib',
            'Latakia',
            'Quneitra',
            'Raqqa',
            'Rif Dimashq',
            'Tartus',
            'Sweida',
        ];

        foreach ($governorates as $governorateName) {
            Governorate::create(['name' => $governorateName]);
        }
    }

    /**
     * Store a newly created resource in storage.
     */
    public function createRegion()
    {
        $governorate = Governorate::where('name', 'Damascus')->first();
        if ($governorate) {
            $regionNames = [
                'Al-Midan',
                'Al-Qanawat',
                'Al-Muhajireen',
                'Al-Qadam',
                'Al-Tadamon',
                'Al-Asali',
                'Al-Kiswah',
                'Jaramana',
                'Al-Mazzeh',
                'Barzeh',
                'Jobar',
                'Mezzeh',
                'Darayya',
                'Qudsaya',
                'Yarmouk Camp',
                'Dummar',
                'Kafr Sousa',
                'Douma',
                'Harasta',
                'Zamalka'
            ];

            foreach ($regionNames as $name) {
                Region::create([
                    'governorate_id' => $governorate->id,
                    'name' => $name
                ]);
            }
        }
        $governorate = Governorate::where('name', 'Aleppo')->first();

        if ($governorate) {
            $regionNames = [
                'Aleppo City',
                'Al-Bab',
                'Azaz',
                'Manbij',
                'Jarabulus',
                'Atarib',
                'Al-Safira',
                'Deir Hafer',
                'Al-Rai',
                'Al-Maskanah',
                'Afrin',
                'Al-Sfeira',
                'Al-Sukkariyah',
                'Al-Basuta',
                'Al-Mansoura',
                'Al-Atareb',
                'Al-Jebrin',
                'Al-Zarbah',
                'Kafr Naha',
                'Al-Zirbah'
            ];

            foreach ($regionNames as $name) {
                Region::create([
                    'governorate_id' => $governorate->id,
                    'name' => $name
                ]);
            }
        }
        $governorate = Governorate::where('name', 'Daraa')->first();

        if ($governorate) {
            $regionNames = [
                'Al-Shaykh Maskin',
                'Al-Muzayrib',
                'Jasim',
                'Tafas',
                'Nawa',
                'Inkhil',
                'Izraa',
                'Dael',
                'Al-Sanamayn',
                'Al-Harak',
                'Yadouda',
                'Al-Jiza',
                'Al-Yadoudah',
                'Al-Taybeh',
                'Khirbet Ghazaleh',
                'Al-Sahwa',
                'Al-Mseifrah',
                'Al-Shajarah',
                'Al-Ghariyah al-Gharbiyah',
                'Al-Ghariyah al-Sharqiyah'
            ];

            foreach ($regionNames as $name) {
                Region::create([
                    'governorate_id' => $governorate->id,
                    'name' => $name
                ]);
            }
        }
        $governorate = Governorate::where('name', 'Deir ez-Zor')->first();

        if ($governorate) {
            $regionNames = [
                'Deir ez-Zor City',
                'Al-Bukamal',
                'Al-Mayadin',
                'Al-Sur',
                'Al-Quriyah',
                'Al-Asharah',
                'Abu Kamal',
                'Al-Husseiniyah',
                'Al-Jalaa',
                'Al-Busayrah',
                'Al-Hawiqa',
                'Al-Sinaa',
                'Al-Rashidiyah',
                'Al-Jafra',
                'Al-Tayyana',
                'Al-Musaytirah',
                'Al-Shamitiyah',
                'Al-Baghuz',
                'Al-Susah',
                'Al-Sabha',
                'Al-Abbas',
                'Al-Busayrah',
                'Al-Hawayij',
                'Al-BuKamal',
                'Al-Sur',
                'Al-Shaitat',
                'Al-Marashidah',
                'Al-Tayana',
                'Al-Qouriyah',
                'Al-Jala',
                'Al-Kouriyeh',
                'Al-Mayadeen',
                'Hajin',
                'Sousse',
                'Al-Hajeen',
                'Al-Tabiya',
                'Al-Zir',
                'Al-Zer',
                'Al-Husseiniya',
                'Dhiban',
                'Al-Rashidiya',
                'Maeizileh',
                'Al-Herriya',
                'Al-Hrej',
                'Al-Hrij',
                'Al-Mayadeen',
                'Al-Hmeid',
                'Al-Kabasat',
                'Al-Tabiya',
                'Al-Mehimideh',
                'Al-Jalaa',
                'Al-Qouriya',
                'Abu Kamal',
                'Al-Bokamal'
            ];

            foreach ($regionNames as $name) {
                Region::create([
                    'governorate_id' => $governorate->id,
                    'name' => $name
                ]);
            }
        }
        $governorate = Governorate::where('name', 'Hama')->first();

        if ($governorate) {
            $regionNames = [
                'Hama City',
                'Salamiyah',
                'Mahardeh',
                'Masyaf',
                'Mork',
                'Souran',
                'Halfaya',
                'Taybet El Imam',
                'Kafr Zeita',
                'Kernaz',
                'Lataminah',
                'Morek',
                'Kafr Naboudeh',
                'Al-Suqaylabiyah',
                'Al-Rastan',
                'Al-Hamidiyah',
                'Al-Qusayr',
                'Al-Mukharram',
                'Al-Hula',
                'Al-Lataminah',
                'Al-Kafrun',
                'Al-Masasneh',
                'Al-Khunayzir',
                'Al-Ramliyah',
                'Al-Sabouniyah',
                'Al-Buwaydah',
                'Al-Qahtaniyah',
                'Al-Mughayr',
                'Al-Safsafiyah',
                'Al-Taybah',
                'Al-Qusayr',
                'Al-Rastan',
                'Al-Hamidiyah',
                'Al-Muhayyam',
                'Al-Bureij',
                'Al-Qastal',
                'Al-Salamiyah',
                'Al-Lataminah',
                'Al-Safsafiyah',
                'Al-Karamah',
                'Al-Sabbourah',
                'Al-Makramiyah',
                'Al-Mahamid',
                'Al-Foah',
                'Al-Buwaydah',
                'Al-Ghabat',
                'Al-Sabouniyah',
                'Al-Mukharram',
                'Al-Taybah',
                'Al-Muhayyam',
                'Al-Kubayr',
                'Al-Sabbourah',
                'Al-Kafrayn',
                'Al-Habah',
                'Al-Tunaybah',
                'Al-Hafasiyah',
                'Al-Qaramitah',
                'Al-Qirbat',
                'Al-Qarn',
                'Al-Hanaturah',
                'Al-Maklabah',
                'Al-Jawazat',
                'Al-Tanar',
                'Al-Qutaytah',
                'Al-Buwaidah',
                'Al-Qahtaniyah',
                'Al-Kamam',
                'Al-Maghayyir',
                'Al-Qarqa',
                'Al-Ghabah',
                'Al-Musharafah',
                'Al-Madakhat',
                'Al-Asilah',
                'Al-Misriyah',
                'Al-Shari',
                'Al-Hawash',
                'Al-Sadah',
                'Al-Zahra',
                'Al-Aqabah',
                'Al-Bayadir',
                'Al-Dab',
                'Al-Rashid',
                'Al-Mansourah',
                'Al-Jina',
                'Al-Kafrayn',
                'Al-Hazm',
                'Al-Samalil',
                'Al-Qasr',
                'Al-Kamam',
                'Al-Khazanah',
                'Al-Naghiliyah',
                'Al-Muthallath',
                'Al-Ashrafiyah',
                'Al-JCertainly'
            ];
            foreach ($regionNames as $name) {
                Region::create([
                    'governorate_id' => $governorate->id,
                    'name' => $name
                ]);
            }
        }
        $governorate = Governorate::where('name', 'Hasakah')->first();
        if ($governorate) {
            $regionNames = [
                'Hasakah City',
                'Qamishli',
                'Al-Malikiyah',
                'Ras al-Ain',
                'Amuda',
                'Derik',
                'Al-Qahtaniyah',
                'Tell Tamer',
                'Al-Yaarubiyah',
                'Al-Sabaa Biyar',
                'Al-Shaddadi',
                'Al-Qamishli Subdistrict',
                'Al-Darbasiyah',
            ];

            foreach ($regionNames as $name) {
                Region::create([
                    'governorate_id' => $governorate->id,
                    'name' => $name
                ]);
            }
        }
        $governorate = Governorate::where('name', 'Homs')->first();

        if ($governorate) {
            $regionNames = [
                'Homs City',
                'Al-Rastan',
                'Al-Qusayr',
                'Palmyra',
                'Al-Mukharram',
                'Al-Hula',
                'Al-Qaryatayn',
                'Al-Taybah',
                'Al-Muhayyam',
                'Al-Buwaidah',
                'Al-Qahtaniyah',
                'Al-Ghantu',
                'Al-Qusayr Subdistrict',
                'Al-Mukharram Subdistrict',
                'Al-Hula Subdistrict',
                'Al-Taybah Subdistrict',
                'Al-Muhayyam Subdistrict',
            ];

            foreach ($regionNames as $name) {
                Region::create([
                    'governorate_id' => $governorate->id,
                    'name' => $name
                ]);
            }
        }
        $governorate = Governorate::where('name', 'Idlib')->first();

        if ($governorate) {
            $regionNames = [
                'Idlib City',
                'Jisr al-Shughur',
                'Ariha',
                'Maarat al-Numan',
                'Saraqib',
                'Salqin',
                'Kafr Nabl',
                'Maarat Misrin',
                'Al-Dana',
                'Binnish',
                'Taftanaz',
                'Sarmin',
                'Maarat al-Atiq',
                'Kafrouma',
                'Hazano',
                'Kafr Takharim',
                'Idlib Subdistrict',
                'Jisr al-Shughur Subdistrict',
                'Maarat al-Numan Subdistrict',
                'Ariha Subdistrict',
                'Maarat Misrin Subdistrict',
                'Al-Dana Subdistrict',
                'Binnish Subdistrict',
                'Taftanaz Subdistrict',
                'Sarmin Subdistrict',
                'Maarat al-Atiq Subdistrict',
                'Kafr Takharim Subdistrict',
            ];

            foreach ($regionNames as $name) {
                Region::create([
                    'governorate_id' => $governorate->id,
                    'name' => $name
                ]);
            }
        }
        $governorate = Governorate::where('name', 'Latakia')->first();

        if ($governorate) {
            $regionNames = [
                'Latakia City',
                'Jableh',
                'Qardaha',
                'Tartus',
                'Safita',
                'Al-Haffah',
                'Al-Shaykh Badr',
                'Al-Qadmus',
                'Al-Suwayda',
                'Al-Sarraf',
                'Al-Rawda',
                'Al-Duraykish',
                'Haffah Subdistrict',
                'Al-Qadmus Subdistrict',
                'Al-Suwayda Subdistrict',
                'Al-Sarraf Subdistrict',
                'Al-Rawda Subdistrict',
                'Al-Duraykish Subdistrict',
            ];

            foreach ($regionNames as $name) {
                Region::create([
                    'governorate_id' => $governorate->id,
                    'name' => $name
                ]);
            }
        }
        $governorate = Governorate::where('name', 'Quneitra')->first();

        if ($governorate) {
            $regionNames = [
                'Quneitra City',
                'Khan Arnabah',
                'Al-Qahtaniyah',
                'Al-Hurriyah',
                'Jubata al-Khashab',
                'Hadar',
                'Quneitra Subdistrict',
                'Khan Arnabah Subdistrict',
                'Al-Qahtaniyah Subdistrict',
                'Al-Hurriyah Subdistrict',
                'Jubata al-Khashab Subdistrict',
                'Hadar Subdistrict',
            ];

            foreach ($regionNames as $name) {
                Region::create([
                    'governorate_id' => $governorate->id,
                    'name' => $name
                ]);
            }
        }
        $governorate = Governorate::where('name', 'Raqqa')->first();

        if ($governorate) {
            $regionNames = [
                'Raqqa City',
                'Tel Abyad',
                'Tabqa',
                'Al-Thawrah',
                'Al-Mansoura',
                'Ain Issa',
                'Suluk',
                'Al-Salhabiyyah',
                'Tabaqa Subdistrict',
                'Al-Thawrah Subdistrict',
                'Al-Mansoura Subdistrict',
                'Ain Issa Subdistrict',
                'Suluk Subdistrict',
            ];

            foreach ($regionNames as $name) {
                Region::create([
                    'governorate_id' => $governorate->id,
                    'name' => $name
                ]);
            }
        }
        $governorate = Governorate::where('name', 'Rif Dimashq')->first();

        if ($governorate) {
            $regionNames = [
                'Douma',
                'Darayya',
                'Jaramana',
                'Qatana',
                'Tishreen',
                'Harasta',
                'Al-Tal',
                'Yabroud',
                'Rankous',
                'Zabadani',
                'Maaloula',
                'Sahnaya',
                'Al-Kiswa',
                'Al-Malihah',
                'Deir Al-Asafir',
                'Qudsaya',
                'Kisweh',
                'Muadamiyat Al-Sham',
                'Sayyidah Zaynab',
                'Dumayr',
                'Yalda',
                'Al-Nashabiyah',
                'Al-Qutayfah',
                'Al-Qadam',
                'Al-Muqaylabiyah',
                'Jdaidet Al-Sharqiyah',
                'Adra',
                'Al-Zabadani',
                'Al-Qutayfah Subdistrict',
                'Al-Qalamoun Subdistrict',
                'Dumayr Subdistrict',
                'Yalda Subdistrict',
                'Al-Qadam Subdistrict',
                'Al-Muqaylabiyah Subdistrict',
                'Jdaidet Al-Sharqiyah Subdistrict',
                'Adra Subdistrict',
            ];

            foreach ($regionNames as $name) {
                Region::create([
                    'governorate_id' => $governorate->id,
                    'name' => $name
                ]);
            }
        }
        $governorate = Governorate::where('name', 'Tartus')->first();

        if ($governorate) {
            $regionNames = [
                'Tartus City',
                'Safita',
                'Baniyas',
                'Dweir Bzabur',
                'Al-Duraykish',
                'Al-Haffah',
                'Al-Mashta Al-Helu',
                'Al-Ghanto',
                'Al-Qadmus',
                'Al-Shaykh Badr',
                'Safita Subdistrict',
                'Baniyas Subdistrict',
                'Dweir Bzabur Subdistrict',
                'Al-Duraykish Subdistrict',
                'Al-Haffah Subdistrict',
                'Al-Mashta Al-Helu Subdistrict',
                'Al-Ghanto Subdistrict',
                'Al-Qadmus Subdistrict',
                'Al-Shaykh Badr Subdistrict',
            ];

            foreach ($regionNames as $name) {
                Region::create([
                    'governorate_id' => $governorate->id,
                    'name' => $name
                ]);
            }
        }
        $governorate = Governorate::where('name', 'Sweida')->first();

        if ($governorate) {
            $regionNames = [
                'Sweida City',
                'Shahba',
                'Salkhad',
                'Izra',
                'Al-Qurayya',
                'Qanawat',
                'Al-Suwayda',
                'Shahba Subdistrict',
                'Salkhad Subdistrict',
                'Izra Subdistrict',
                'Al-Qurayya Subdistrict',
                'Qanawat Subdistrict',
            ];

            foreach ($regionNames as $name) {
                Region::create([
                    'governorate_id' => $governorate->id,
                    'name' => $name
                ]);
            }
        }

    }


    /**
     * Display the specified resource.
     */
    public function showGovernorate()
    {
        if (Governorate::count() === 0) {
            $governorateController = new GovernorateController();
            $governorateController->create();
            $governorateController->createRegion();
        }
        $governorateNames = Governorate::pluck('name', 'id')->toArray();
        return response()->json([
            'Status' => 'Success',
            'Message' => 'All Governorates Have Been Successfully Retrieved ',
            'List' => $governorateNames,
            'Code' => Response::HTTP_OK,
        ]);
    }

    public function showRegions($governorateId)
    {
        $governorate = Governorate::find($governorateId);

        if ($governorate) {
            if ($governorate->regions) {
                $regionNames = $governorate->regions->pluck('name', 'id')->toArray();

                return response()->json([
                    'Status' => 'Success',
                    'Message' => 'All Regions Have Been Successfully Retrieved ',
                    'List' => $regionNames,
                    'Code' => Response::HTTP_OK,
                ]);
            }
        }

        return response()->json([
            'Status' => 'Failed',
            'Message' => 'Governorate Not Found',
            'List' => null,
            'Code' => Response::HTTP_OK,
        ]);

    }

}
