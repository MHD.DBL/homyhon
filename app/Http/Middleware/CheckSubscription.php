<?php

namespace App\Http\Middleware;

use App\Models\Account;
use App\Models\SubRecord;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class CheckSubscription
{
    /**
     * Handle an incoming request.
     *
     * @param \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next)
    {
        $account = Account::where('id', Auth::id())->first();
        if ($account->user) {
            $user = $account->user;
            $latestSubscription = SubRecord::where('user_id', $user->id)->orderBy('id', 'desc')->first();
            if ($latestSubscription) {
                if ($latestSubscription->date >= Carbon::now()->format('Y-m-d')) {
                    if ($latestSubscription->remainingProperty > 0) {
                        return $next($request);
                    } else {
                        return response()->json([
                            'Message' => 'You have reached the limit of allowed property posts.',
                        ]);
                    }
                } else {
                    return response()->json([
                        'Message' => 'Your subscription has expired. Please renew your subscription to continue posting properties.',
                    ]);
                }
            } else {
                return response()->json([
                    'Message' => 'You need to subscribe before posting properties.',
                ]);
            }
        }
        if ($account->office) {
            return $next($request);
        }
    }
}
