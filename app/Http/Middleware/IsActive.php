<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class IsActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next)
    {

        if (\Auth::check() && \Auth::user()->blocked == 0 ) {
            return $next($request);
        }
        return response()->json([
            'Status' => "blocked",
            'Message' => 'Your account is inactive.',
        ]);
    }
}
