@extends('home.home')

@section('content')

    <div class="row m-5">
        <h3>Account statistics</h3>
        <h6>By account type</h6>
        <div class="col-6">
            {!! $chart->container() !!}
        </div>

        <div class="col-6">

            <table class="table border table-rounded table-striped table-row-bordered text-center mt-5">
                <tbody>
                <tr class="text-haram">
                    <th>Account Type</th>
                    <th>Number of Account</th>
                </tr>
                <tr>
                    <td>User</td>
                    <td>{{$user}}</td>
                </tr>
                <tr>
                    <td>Office</td>
                    <td>{{$office}}</td>
                </tr>
                <tr>
                    <td>Admin</td>
                    <td>{{$admin}}</td>
                </tr>
                <tr class="text-bold">
                    <td><b>Total</b></td>
                    <td><b>{{$admin+$office+$user}}</b></td>
                </tr>
                </tbody>
            </table>
        </div>


    </div>

    <div class="row m-5">
        <h3>Property statistics</h3>
        <h6>Depending on the status of the property</h6>

        <div class="col-6">
            {!! $chart1->container() !!}
        </div>
        <div class="col-6">
            <table class="table border table-rounded table-striped table-row-bordered text-center mt-5">
                <tbody>
                <tr class="text-haram">
                    <th>Status of Property</th>
                    <th>Number of Property</th>
                </tr>
                <tr>
                    <td>Accepted</td>
                    <td>{{$accepted}}</td>
                </tr>
                <tr>
                    <td>Pending</td>
                    <td>{{$pending}}</td>
                </tr>
                <tr>
                    <td>Refused</td>
                    <td>{{$refused}}</td>
                </tr>
                <tr class="text-bold">
                    <td><b>Total</b></td>
                    <td><b>{{$refused+$accepted+$pending}}</b></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row m-5">
        <h3>The most attractive subscriptions for users</h3>
        <h6>By subscription type</h6>
        <div class="col-6">
            {!! $chart2->container() !!}
        </div>
        <div class="col-6">
            <table class="table border table-rounded table-striped table-row-bordered text-center mt-5">
                <tbody>
                <tr class="text-haram">
                    <th>Name of subscription</th>
                    <th>Number of Subscription</th>
                </tr>

                @foreach($SubRecord as $item)
                    <tr>
                        <td>{{$item['SubName']}}</td>
                        <td>{{$item['Count']}}</td>
                    </tr>
                @endforeach
                <tr class="text-bold">
                    <td><b>Total</b></td>
                    <td><b>{{$TotalSub}}</b></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row m-5">
        <h3>Number of new properties per month (current year)</h3>
        <h6>According to the type of property</h6>
        {!! $chart3->container() !!}
    </div>

    <div class="row m-5">
        <h3>Application revenue during the current year</h3>
        <h6>Based on user subscriptions</h6>
        {!! $chart4->container() !!}
    </div>

@endsection
@section('scripts')
    <script src="{{ $chart1->cdn() }}"></script>
    {{ $chart1->script() }}

    <script src="{{ $chart2->cdn() }}"></script>
    {{ $chart2->script() }}

    <script src="{{ $chart3->cdn() }}"></script>
    {{ $chart3->script() }}

    <script src="{{ $chart4->cdn() }}"></script>
    {{ $chart4->script() }}

    <script src="{{ $chart->cdn() }}"></script>
    {{ $chart->script() }}
    <script>
        $('.charts').addClass("btnFocused");

    </script>
@endsection
