<html>
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    {{--    <script defer="defer" src="{{asset('/index.bundle.js')}}"></script>--}}
    <style>.vQ_1eSDn9cYSioF3wguV {
            position: absolute;
            background-color: white;
            display: flex;
            flex-direction: column;
            border-radius: 10px;
            overflow: hidden;
            box-shadow: inset 0 0 10px rgba(0, 0, 0, 0.15)
        }
    </style>
    <style>.container {
            width: 631px;
            height: 295px
        }

        .container .header {
            height: 110px;
            background-color: var(--modal-header-background);
            display: flex;
            justify-content: center
        }

        .container .header .close {
            position: absolute;
            right: 36px;
            top: 27px;
            cursor: pointer
        }

        .container .header .check {
            align-self: center
        }

        .container .content .message {
            font-size: 24px;
            text-align: center;
            padding: 0px 25px
        }

        .container .content .message .bold {
            font-weight: 600
        }
    </style>
    <style>
        @font-face {
            font-family: 'Segoe UI';
        }

        @font-face {
            font-family: 'Segoe UI Bold';
        }
    </style>
    <style>html, body {
            font-family: 'Segoe UI';
            zoom: 0.9;
            margin: 0;
            background-color: var(--background-color) !important;
        }

        #root {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0
        }

        button {
            font-family: 'Segoe UI';
            cursor: pointer;
            font-size: var(--default-font-size)
        }

        a:hover {
            cursor: pointer
        }

        .App {
            transition: margin-left 0.5s;
            height: 100%
        }

        .btnProfile {
            width: 70px;
            height: 70px;
            border-radius: 70px;
            border: 0px;
            background-color: var(--foreground-color);
            position: absolute;
            top: 30px;
            right: 30px;
            z-index: 1;
            font-weight: bold;
            font-size: 24px;
            display: flex;
            align-items: center;
            justify-content: center;
            color: #93a0b5
        }

        .popupContent {
            position: absolute;
            top: 80px;
            right: 30px;
            padding-top: 35px;
            display: none;
            z-index: 1
        }

        .popupMenu {
            min-width: 225px;
            width: auto;
            border-radius: 9px;
            background-color: var(--foreground-color);
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.15);
            overflow: hidden
        }

        .popupMenu button {
            background-color: transparent;
            border: 0px;
            width: 100%;
            text-align: left;
            padding: 20px;
            font-size: 22px;
            color: #4e639e
        }

        .popupMenu button img {
            margin-right: 20px
        }

        .popupMenu button:hover {
            background-color: #F2F7FF
        }

        .btnProfile:hover + .popupContent, .popupContent:hover {
            display: block
        }

        .helpContent {
            position: fixed;
            right: 30px;
            bottom: 60px;
            display: flex;
            align-items: center;
            justify-content: center
        }

        .btnHelp {
            border: 0px;
            width: 70px;
            height: 70px;
            border-radius: 70px;
            background: linear-gradient(0.17deg, #7644ff 1.34%, #9680ff 99.9%);
            box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.15);
            display: flex;
            align-items: center;
            justify-content: center;
            transition: 0.5s;
            z-index: 1
        }

        .btnHelpSwipe {
            background: linear-gradient(0.17deg, #7a4cff 1.34%, #9176ff 99.9%);
            width: 0px;
            height: 50px;
            margin-right: -25px;
            border-top-left-radius: 25px;
            border-bottom-left-radius: 25px;
            overflow: hidden;
            transition: 0.5s;
            font-weight: bold;
            font-size: 16px;
            color: white;
            position: fixed;
            right: 103px;
            bottom: 70px
        }

        .btnHelpSwipe div {
            margin-top: 13px;
            margin-left: 15px;
            width: 110px
        }

        .btnHelp:hover + .btnHelpSwipe, .btnHelpSwipe:hover {
            width: 150px;
            margin-left: -150px
        }

        .ReactModalPortal > * {
            opacity: 0
        }

        .ReactModalPortal .ReactModal__Overlay {
            background-color: rgba(11, 22, 42, 0.5) !important;
            align-items: center;
            display: flex;
            justify-content: center;
            transition: opacity 200ms ease-in-out;
            z-index: 8000
        }

        .ReactModalPortal .ReactModal__Overlay--after-open {
            opacity: 1
        }

        .ReactModalPortal .ReactModal__Overlay--before-close {
            opacity: 0
        }

        .rateUsToast {
            background-color: #f0f6ff;
            position: fixed;
            bottom: 30px;
            right: 130px;
            width: 422.89px;
            height: 166px;
            box-shadow: 0px 0px 10.6522px rgba(0, 0, 0, 0.15);
            border-radius: 9.58696px;
            font-size: 22px;
            font-weight: 600;
            color: var(--primary-font-color)
        }

        .rateUsToast div {
            margin-top: 40px;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            gap: 10px
        }

        .rateUsToast div div {
            margin: 0px;
            flex-direction: row
        }

        .rateUsToast div div button {
            padding: 0px;
            height: 40px;
            min-width: 136px
        }

        .rateUsToast div .toastBtns {
            margin-top: 10px;
            gap: 30px
        }

        .rateUsToast .toastCancel {
            position: absolute;
            top: 15px;
            right: 15px;
            padding: 0px;
            background-color: transparent;
            border: none
        }
    </style>
    <style>:root {
            --dark-blue-background: #183360;
            --active-blue-font-color: #183360;
            --modal-header-darkblue-background: #05153f;
            --grey-font-color: #93a0b5;
            --background-color: #f1f4f8;
            --foreground-color: #ffffff;
            --tertiary-color: #05153f;
            --primary-font-color: #183360;
            --green-font-color: #04d289;
            --red-font-color: #ff3b30;
            --purple-font-color: #6726ff;
            --orange-color: #ff8f11;
            --default-font-size: 18px;
            --modal-header-background: #f2f2f2;
            --hover-orange-color: #d97a0e
        }

        h1 {
            font-family: 'Segoe UI Bold';
            font-size: 50px;
            font-weight: 700;
            line-height: 66.5px
        }

        h2 {
            font-size: 30px;
            padding: 0px;
            margin: 5px 0px;
            margin-top: 0px
        }

        h3 {
            font-size: 20px;
            font-weight: normal;
            white-space: pre-line
        }

        p {
            padding: 0px;
            margin: 5px 0px
        }

        a {
            text-decoration: none
        }

        .flex {
            display: flex
        }

        .grid {
            display: grid
        }

        .fwrap {
            flex-wrap: wrap
        }

        .ait {
            align-items: top
        }

        .aic {
            align-items: center
        }

        .jcl {
            justify-content: left
        }

        .jcc {
            justify-content: center
        }

        .jcr {
            justify-content: right
        }

        .tac {
            text-align: center
        }

        .w100 {
            width: 100%
        }

        .sb {
            font-weight: 600
        }

        .borderButtonColor {
            color: var(--orange-color);
            border: 3px solid var(--orange-color)
        }

        .paddinglr {
            padding: 100px 50px
        }

        .redColor {
            color: var(--red-font-color);
            fill: var(--red-font-color)
        }

        .greenColor {
            color: var(--green-font-color);
            fill: var(--green-font-color)
        }

        .purpleColor {
            color: var(--purple-font-color)
        }

        .orangeColor {
            color: var(--orange-color)
        }

        .content {
            color: var(--primary-font-color);
            margin: auto;
            max-width: 85%;
            padding-top: 30px;
            padding-bottom: 20px
        }

        .sectionContent {
            margin-top: 40px;
            margin-bottom: 40px;
            font-size: 22px;
            color: var(--primary-font-color)
        }

        .btnAction {
            min-width: 170px;
            padding: 10px 25px;
            color: var(--orange-color);
            border: 2.5px solid var(--orange-color);
            border-radius: 39px;
            font-weight: 600;
            background-color: transparent
        }

        .btnAction:hover {
            background-color: var(--orange-color);
            color: white
        }

        .btnDwm {
            background: linear-gradient(#fff, #fff) padding-box, linear-gradient(to right, #8526ff, #2a26ff) border-box;
            border: 2.5px solid transparent;
            color: #7644ff
        }

        .btnDwm:hover {
            background: linear-gradient(to right, #8526ff, #2a26ff) padding-box, linear-gradient(to right, #8526ff, #2a26ff) border-box;
            border: 2.5px solid transparent
        }

        .btnBuy {
            display: flex;
            align-items: center;
            justify-content: center;
            gap: 10px;
            min-width: 170px;
            padding: 15px 40px;
            color: #fff;
            border-radius: 39px;
            font-weight: 600;
            background-color: var(--tertiary-color);
            border: none;
            cursor: pointer
        }

        .btnBuy:hover {
            background-color: #0f3cb0
        }

        .btnBuy:active {
            background-color: #0f3391
        }

        .btnBuyOrange {
            display: flex;
            align-items: center;
            justify-content: center;
            gap: 10px;
            min-width: 170px;
            padding: 15px 40px;
            color: #fff;
            border-radius: 39px;
            font-weight: 600;
            background-color: var(--orange-color);
            border: none;
            cursor: pointer
        }

        .btnBuyOrange:hover {
            background-color: #ffa846
        }

        .btnBuyOrange:active {
            background-color: #d97a0e
        }

        .sectionTitle {
            font-weight: bold;
            font-size: 20px;
            margin-bottom: 25px
        }

        .sectionTitle img {
            margin-left: 5px;
            margin-right: 13px
        }

        .fullHeadContent {
            height: 400px;
            background: var(--tertiary-color);
            box-shadow: -3px 0px 3px rgba(0, 0, 0, 0.1);
            border-radius: 0px 0px 22px 22px;
            color: var(--foreground-color)
        }

        .fullHeadContent img {
            width: 442px
        }

        .fullHeadContent p {
            margin: 10px
        }

        .spinner {
            width: 120px;
            height: 120px
        }

        @media screen and (min-width: 1500px) {
            .content {
                max-width: 70%
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg)
            }
            100% {
                transform: rotate(360deg)
            }
        }
    </style>
    <style>.checkboxContainer {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 12px;
            user-select: none
        }

        .checkboxContainer input {
            position: absolute;
            opacity: 0;
            height: 0;
            width: 0
        }

        .checkboxContainer .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 18px;
            width: 18px;
            border: 1px solid #cad0dd;
            border-radius: 100%
        }

        .checkboxContainer .checkmark.greenColor {
            border: 2.5px solid #cad0dd;
            border-radius: 8px
        }

        .checkboxContainer:hover input ~ .checkmark {
            background-color: #cad0dd
        }

        .checkboxContainer input:checked ~ .checkmark {
            background-color: var(--primary-font-color)
        }

        .checkboxContainer input:checked ~ .purpleColor {
            background-color: var(--purple-font-color)
        }

        .checkboxContainer input:checked ~ .greenColor {
            background-color: var(--green-font-color);
            border: 2px solid var(--green-font-color);
            border-radius: 8px
        }

        .checkmark:after {
            content: '';
            position: absolute;
            display: none
        }

        .checkboxContainer input:checked ~ .checkmark:after {
            display: block
        }

        .checkboxContainer .checkmark:after {
            left: 6px;
            top: 3px;
            width: 3px;
            height: 7px;
            border: solid white;
            border-width: 0 3px 3px 0;
            transform: rotate(45deg)
        }

        .checkboxContainer .uncheckAll:after {
            left: 7.5px;
            top: 4px;
            width: 0px;
            height: 9px;
            border-width: 0 3px 0 0;
            transform: rotate(90deg)
        }

        .sectionSelectRadio {
            display: none
        }

        .sectionSelectRadio + label {
            padding: 7px 2px;
            font-size: 20px;
            font-weight: 700;
            margin-right: 50px;
            color: var(--primary-font-color);
            border: 0px;
            background-color: transparent
        }

        .sectionSelectRadio:checked + label {
            border-bottom: 4px solid var(--purple-font-color)
        }
    </style>
    <style>.rateUsPage {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100%;
            padding-top: 0px;
            padding-bottom: 0px
        }

        .rateUsPage .rateUsPageContent {
            width: 1076px;
            min-height: 750px;
            padding: 0px;
            overflow: hidden;
            transition: height 0.5s
        }

        .rateUsPage .rateUsPageContent .header {
            height: 210px
        }

        .rateUsPage .rateUsPageContent .content {
            padding-top: 30px
        }

        .rateUsPage .rateUsPageContent .content .pThanks {
            margin: 50px 100px;
            margin-top: 30px
        }

        .rateUsPage .rateUsPageContent .content .checksContent {
            margin-top: 50px
        }

        .rateUsPage .rateUsPageContent .content .writeFeedbackContent {
            margin-top: -30px
        }

        .rateUsPage .rateUsPageContent .content .writeFeedbackContent iframe {
            margin-top: 10px;
            margin-bottom: 0px
        }

        .rateUsPage .rateUsPageContent .firstScreenContent {
            margin-top: 70px
        }

        .rateUsModal {
            width: 856px;
            background: white;
            border-radius: 10px;
            overflow: hidden;
            transition: height 0.5s
        }

        .rateUsModal .header, .rateUsPage .header {
            height: 153px;
            background-color: var(--modal-header-background);
            display: flex;
            align-items: center;
            justify-content: center;
            position: relative;
            flex-direction: column
        }

        .rateUsModal .header .close, .rateUsPage .header .close {
            position: absolute;
            right: 36px;
            top: 27px;
            cursor: pointer;
            width: 15px;
            height: 15px
        }

        .rateUsModal .header .icon, .rateUsPage .header .icon {
            width: 40px
        }

        .rateUsModal .header .title, .rateUsPage .header .title {
            color: var(--primary-font-color);
            font-family: 'Segoe UI Bold';
            margin-top: 10px;
            font-size: 28px
        }

        .rateUsModal .content p, .rateUsPage .content p {
            text-align: center;
            font-size: 20px
        }

        .rateUsModal .content .starsContent, .rateUsPage .content .starsContent {
            display: flex;
            align-items: center;
            justify-content: center;
            margin-top: 35px;
            margin-bottom: 50px
        }

        .rateUsModal .content .star, .rateUsPage .content .star {
            margin: 0px 5px
        }

        .rateUsModal .content .btnContent, .rateUsPage .content .btnContent {
            display: flex;
            align-items: center;
            justify-content: center;
            margin-top: 25px
        }

        .rateUsModal .content .btnContent button, .rateUsPage .content .btnContent button {
            width: 267px;
            height: 47px;
            font-size: 16px
        }

        .rateUsModal .content .webstore, .rateUsPage .content .webstore {
            margin-top: 40px;
            margin-bottom: 20px
        }

        .rateUsModal .content .webstore button, .rateUsPage .content .webstore button {
            display: flex;
            align-items: center;
            justify-content: center
        }

        .rateUsModal .content .webstore button img, .rateUsPage .content .webstore button img {
            margin-left: 10px
        }

        .rateUsModal .content .checksContent, .rateUsPage .content .checksContent {
            margin-top: 30px;
            font-size: 16px
        }

        .rateUsModal .content .checksContent div, .rateUsPage .content .checksContent div {
            display: flex;
            align-items: center;
            justify-content: center;
            margin-bottom: 15px
        }

        .rateUsModal .content .checksContent div .greenColor, .rateUsPage .content .checksContent div .greenColor {
            margin-top: -3px
        }

        .rateUsModal .content .writeFeedbackContent, .rateUsPage .content .writeFeedbackContent {
            text-align: center;
            margin-top: -15px;
            margin-bottom: 20px
        }

        .rateUsModal .content .writeFeedbackContent p, .rateUsPage .content .writeFeedbackContent p {
            font-size: 15px
        }

        .rateUsModal .content .writeFeedbackContent p:last-of-type, .rateUsPage .content .writeFeedbackContent p:last-of-type {
            text-align: left;
            margin-top: 15px
        }

        .rateUsModal .content .writeFeedbackContent a, .rateUsPage .content .writeFeedbackContent a {
            text-decoration: underline
        }

        .rateUsModal .content .writeFeedbackContent .submitedContent, .rateUsPage .content .writeFeedbackContent .submitedContent {
            margin-top: 25px;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            gap: 5px;
            font-size: 16px;
            font-family: 'Segoe UI Bold';
            color: var(--green-font-color)
        }

        .rateUsModal .content .writeFeedbackContent iframe, .rateUsPage .content .writeFeedbackContent iframe {
            margin-top: 20px;
            margin-bottom: 15px;
            width: 100%;
            height: 165px;
            border: none
        }
    </style>
    <style>.welcomeModalA {
            position: relative;
            width: 740px;
            background: white;
            border-radius: 10px;
            overflow: hidden;
            transition: height 0.5s;
            padding: 75px 0px;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            gap: 30px
        }

        .welcomeModalDwm {
            position: relative;
            width: 550px;
            background: white;
            border-radius: 10px;
            overflow: hidden;
            transition: height 0.5s;
            padding: 21px 0px;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center
        }

        .welcomeModalB {
            position: relative;
            width: 746px;
            background: white;
            border-radius: 10px;
            overflow: hidden;
            transition: height 0.5s;
            padding: 55px 0px;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            gap: 10px
        }

        .welcomeModalB .headerB {
            display: flex;
            align-items: flex-start;
            gap: 25px;
            margin-left: -100px;
            margin-top: 15px
        }

        .welcomeModalB .headerB img {
            padding-top: 15px
        }

        .welcomeModalB .textB {
            max-width: 480px;
            margin-left: 85px;
            text-align: left
        }

        .welcomeModalB .buttonContentB {
            width: 100%;
            margin-top: 25px;
            margin-left: 100px;
            margin-bottom: 0px;
            text-align: right
        }

        .welcomeModalA .close, .welcomeModalB .close {
            top: 30px;
            right: 30px;
            position: absolute;
            cursor: pointer
        }

        .welcomeModalA h1, .welcomeModalB h1 {
            font-family: 'Segoe UI Bold';
            color: var(--primary-font-color);
            font-size: 30px;
            margin: 0px
        }

        .welcomeModalA p, .welcomeModalB p {
            color: var(--primary-font-color);
            font-size: 18px;
            text-align: center;
            max-width: 430px;
            margin-top: 0px
        }

        .welcomeModalA button, .welcomeModalB button {
            width: 242px;
            height: 55px;
            border-radius: 6px;
            border: none;
            font-family: 'Segoe UI Bold';
            font-size: 20px;
            color: #ffffff;
            cursor: pointer
        }

        .welcomeModalA .buttonA, .welcomeModalB .buttonA {
            background: #3285e0
        }

        .welcomeModalA .buttonB, .welcomeModalB .buttonB {
            background: #5552ff
        }

        .welcomeModalA .emailInput, .welcomeModalB .emailInput {
            font-size: 18px;
            padding: 19px;
            border: 1px solid #dadcd0;
            border-radius: 6px;
            width: 320px
        }

        .welcomeModalA .emailInput:focus, .welcomeModalB .emailInput:focus {
            outline: none !important;
            border: 3px solid #3285e0
        }

        .welcomeModalA .invalidInput, .welcomeModalA .invalidInput:focus, .welcomeModalB .invalidInput, .welcomeModalB .invalidInput:focus {
            border: 3px solid #d93025
        }

        .welcomeModalA .invalidEmail, .welcomeModalB .invalidEmail {
            width: 350px;
            color: #d93025;
            text-align: left;
            font-size: 15px;
            margin-top: 5px;
            margin-left: 3px
        }

        .welcomeModalDwm.updated {
            padding: 35px 0px
        }

        .welcomeModalDwm.updated .email-wrapper {
            margin: 35px 0
        }

        .welcomeModalDwm.updated .consentContainer {
            margin-bottom: 28px
        }

        .welcomeModalDwm .close {
            top: 30px;
            right: 30px;
            position: absolute;
            cursor: pointer
        }

        .welcomeModalDwm h1 {
            font-family: 'Segoe UI Bold';
            color: var(--primary-font-color);
            font-size: 22px;
            margin: 0px;
            line-height: 30px;
            text-align: center;
            width: 430px;
            margin-bottom: 15px
        }

        .welcomeModalDwm h1 sup {
            font-size: 10px
        }

        .welcomeModalDwm p {
            color: var(--primary-font-color);
            font-size: 14px;
            text-align: center;
            max-width: 465px;
            margin: 0px
        }

        .welcomeModalDwm p.add-email {
            font-weight: 600;
            margin-top: 40px;
            width: 320px
        }

        .welcomeModalDwm button {
            width: 242px;
            height: 55px;
            border-radius: 6px;
            border: none;
            font-family: 'Segoe UI Bold';
            font-size: 20px;
            color: #ffffff;
            cursor: pointer
        }

        .welcomeModalDwm .buttonDWM {
            background: #FF8F11;
            border-radius: 30px
        }

        .welcomeModalDwm .buttonDWM:disabled {
            opacity: 0.25;
            cursor: default
        }

        .welcomeModalDwm .email-wrapper {
            margin: 18px 0 21px 0
        }

        .welcomeModalDwm .emailInput {
            font-size: 14px;
            padding: 14px;
            border: 1px solid #dadcd0;
            border-radius: 6px;
            width: 320px;
            text-align: center
        }

        .welcomeModalDwm .emailInput:focus {
            outline: none !important;
            border: 3px solid #3285e0
        }

        .welcomeModalDwm .invalidInput, .welcomeModalDwm .invalidInput:focus {
            border: 3px solid #d93025
        }

        .welcomeModalDwm .invalidEmail {
            width: 350px;
            color: #d93025;
            font-size: 12px;
            margin-top: 5px
        }

        .welcomeModalDwm .dwmWelcomeLogo {
            height: 90px;
            width: 90px;
            margin-bottom: 23px
        }

        .welcomeModalDwm .consent_cb {
            font-weight: 400;
            font-size: 10px;
            text-align: center
        }

        .welcomeModalDwm .checkboxContainer {
            width: 320px;
            display: flex;
            justify-content: center;
            align-items: center
        }

        .welcomeModalDwm .checkboxContainer .checkmark {
            top: auto
        }
    </style>
    <style>.sectionSelectRadio {
            display: none
        }

        .sectionSelectRadio + label {
            padding: 7px 2px;
            font-size: 20px;
            font-weight: 700;
            margin-right: 50px;
            color: var(--primary-font-color);
            border: 0px;
            background-color: transparent
        }

        .sectionSelectRadio:checked + label {
            border-bottom: 4px solid var(--purple-font-color)
        }

        .header h1 {
            display: flex;
            align-items: center;
            gap: 18px;
            font-size: 35px;
            line-height: 50px;
            margin: 15px 0
        }

        .header h1 img {
            height: 40px;
            width: 40px
        }

        .header h3 {
            font-size: 15px;
            line-height: 22px;
            margin: 0px
        }
    </style>
    <style>.checkContent {
            margin: 0px;
            width: 40px;
            margin-top: 25px;
            float: left;
            margin-left: 50px
        }

        .lc {
            display: flex;
            border: 1.5px solid #c7d5eb;
            border-radius: 10px;
            margin: 0px 20px;
            padding: 0px 25px;
            margin-bottom: 10px
        }

        .lc div {
            padding: 20px 10px;
            word-wrap: break-word
        }

        .lcHightlight {
            background: linear-gradient(#fff, #fff) padding-box, linear-gradient(#7644ff, #1f15e5) border-box;
            border: 1.5px solid transparent
        }

        .lh {
            border: 0;
            font-weight: 700;
            margin-right: 70px;
            margin-bottom: 15px
        }

        .lh .checkContent {
            margin-left: -5px;
            margin-top: 5px
        }

        .lc:hover {
            border: 1.5px solid var(--primary-font-color)
        }

        .lh:hover {
            border: 0px
        }

        .controlContent {
            height: 60px;
            padding-top: 50px;
            text-align: right;
            box-shadow: 0px -1px 3px rgba(0, 0, 0, 0.1);
            margin-left: -40px;
            margin-right: -70px;
            margin-bottom: -40px;
            padding-bottom: 40px;
            padding: 27px 140px
        }

        .lineBreak {
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap
        }

        .listContent {
            height: 480px;
            padding-right: 50px;
            overflow-y: auto;
            overflow-x: auto
        }

        .listContent::-webkit-scrollbar {
            width: 5px
        }

        .listContent::-webkit-scrollbar-track {
            background: transparent
        }

        .listContent::-webkit-scrollbar-thumb {
            background: #c7d5eb;
            border-radius: 14px
        }

        .listContent::-webkit-scrollbar-thumb:hover {
            background: #a9b4c7
        }
    </style>
    <style>.sectionSelectRadio {
            display: none
        }

        .sectionSelectRadio + label {
            font-family: 'Segoe UI Bold';
            padding: 7px 2px;
            font-size: 20px;
            margin-right: 50px;
            color: var(--primary-font-color);
            border: 0px;
            background-color: transparent;
            opacity: 0.5;
            cursor: pointer
        }

        .sectionSelectRadio:checked + label {
            border-bottom: 4px solid var(--purple-font-color);
            opacity: 1
        }

        .search {
            float: right
        }

        .search input {
            height: 40px;
            width: 230px;
            border: 0px;
            border-bottom: 2px solid var(--purple-font-color);
            outline: none;
            color: var(--primary-font-color);
            background-color: transparent;
            margin-left: -20px;
            padding-left: 30px;
            font-size: var(--default-font-size)
        }

        .search img {
            width: 20px
        }
    </style>
    <style>.boxContent {
            background-color: var(--foreground-color);
            box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.1);
            border-radius: 9px;
            padding: 30px 20px;
            margin-bottom: 50px;
            overflow: hidden;
            font-size: var(--default-font-size)
        }
    </style>
    <style></style>
    <style>.scanResultsWrapper {
            width: 930px;
            background: white;
            border-radius: 10px;
            overflow: hidden;
            transition: height 0.5s;
            position: relative;
            display: flex;
            color: var(--primary-font-color)
        }

        .scanResultsWrapper .close {
            position: absolute;
            right: 36px;
            top: 27px;
            cursor: pointer;
            width: 15px;
            height: 15px;
            z-index: 2
        }

        .scanResultsWrapper .sideContent {
            background-color: var(--modal-header-darkblue-background);
            display: flex;
            align-items: center;
            justify-content: space-between;
            position: relative;
            flex-direction: column
        }

        .scanResultsWrapper .sideContent .sideTop {
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            margin: 45px 40px 0
        }

        .scanResultsWrapper .sideContent .sideBottom {
            color: #c7d5eb;
            margin: 0 20px 50px;
            text-align: center;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column
        }

        .scanResultsWrapper .sideContent .sideBottom > p {
            font-size: 14px
        }

        .scanResultsWrapper .sideContent .title {
            color: var(--foreground-color);
            font-family: 'Segoe UI Bold';
            margin-top: 10px;
            font-size: 28px
        }

        .scanResultsWrapper .mainContent {
            display: flex;
            flex-direction: column;
            justify-content: space-between;
            flex: 1
        }

        .scanResultsWrapper .mainContent .mainContentTop {
            padding: 35px 10px 0 45px;
            flex: 1
        }

        .scanResultsWrapper .mainContent .mainContentTop > h3 {
            margin-bottom: 25px;
            font-family: 'Segoe UI Bold'
        }

        .scanResultsWrapper .mainContent .mainContentTop > .scanResultItem {
            margin-bottom: 25px;
            display: flex;
            align-items: center
        }

        .scanResultsWrapper .mainContent .mainContentTop > .scanResultItem div.greenColor {
            background: #bbffe7
        }

        .scanResultsWrapper .mainContent .mainContentTop > .scanResultItem div.redColor {
            background: #ffe7e7;
            color: #ff3b30
        }

        .scanResultsWrapper .mainContent .mainContentTop > .scanResultItem > .totalSection {
            min-width: 60px;
            padding: 0 15px;
            height: 45px;
            display: flex;
            align-items: center;
            justify-content: center;
            border-radius: 22px;
            color: var(--active-blue-font-color);
            font-family: 'Segoe UI Bold';
            font-size: 24px;
            margin-right: 10px
        }

        .scanResultsWrapper .mainContent .mainContentTop > .scanResultItem > .textSection {
            display: flex;
            align-items: center;
            justify-content: center;
            font-size: 18px
        }

        .scanResultsWrapper .mainContent .mainContentTop > .scanResultItem > .textSection img {
            margin-right: 10px;
            width: 30px;
            height: 30px
        }

        .scanResultsWrapper .mainContent .mainContentTop > .scanResultItem > .textSection .status {
            font-family: 'Segoe UI Bold';
            margin: 0 5px
        }

        .scanResultsWrapper .mainContent .mainContentTop > .scanResultItem > .textSection .hightlightedText {
            font-family: 'Segoe UI Bold';
            color: #ff3b30
        }

        .scanResultsWrapper .mainContent .mainContentBottom {
            display: flex;
            align-items: center;
            justify-content: center;
            background-color: #edf3fa;
            padding: 25px 35px
        }

        .scanResultsWrapper .mainContent .mainContentBottom > .textSection {
            margin: 0 10px 0 0;
            font-size: 16px;
            flex: 2
        }

        .scanResultsWrapper .mainContent .mainContentBottom > .textSection .riskyText {
            color: #ff3b30;
            margin: 0 5px
        }

        .scanResultsWrapper .mainContent .mainContentBottom > .textSection b {
            font-family: 'Segoe UI Bold'
        }

        .scanResultsWrapper .mainContent .mainContentBottom .levelUpButton {
            background: linear-gradient(270deg, #ff8f11 0%, #ffbc11 100%);
            max-width: 250px;
            border-radius: 35px;
            border: none;
            height: 45px;
            color: var(--foreground-color);
            flex: 2
        }

        .scanResultsWrapper .mainContent .mainContentBottom .levelUpButton:active {
            background: linear-gradient(0deg, #ff8f11 0%);
            cursor: pointer;
            border: 1px solid red
        }

        .scanResultsWrapper .mainContent .mainContentBottom .levelUpButton:hover {
            background: linear-gradient(0deg, #ff8f11 0%, #ffbc11 100%);
            cursor: pointer
        }
    </style>
    <style>.purchasedModal {
            width: 750px;
            background: white;
            border-radius: 10px;
            transition: height 0.5s;
            display: flex;
            position: relative;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            gap: 15px;
            padding: 63px;
            color: var(--primary-font-color)
        }

        .purchasedModal .close {
            position: absolute;
            right: 36px;
            top: 27px;
            cursor: pointer;
            width: 15px;
            height: 15px
        }

        .purchasedModal h2 {
            font-family: 'Segoe UI Bold';
            font-size: 30px
        }

        .purchasedModal h3 {
            font-family: 'Segoe UI Bold'
        }

        .purchasedModal p {
            max-width: 500px;
            text-align: center;
            font-size: var(--default-font-size)
        }

        .purchasedModal a {
            font-family: 'Segoe UI Bold';
            text-decoration: underline;
            font-size: 20px;
            color: #0000ff
        }

        .purchasedModal .bottom {
            margin-top: 20px;
            font-size: 16px
        }

        .purchasedModal .bottom a {
            font-size: 16px;
            color: var(--primary-font-color)
        }
    </style>
    <style>.tile {
            position: relative;
            width: 100%;
            height: 150px;
            min-width: 500px;
            background-color: var(--foreground-color);
            box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.1);
            border-radius: 9px;
            margin: 12px;
            text-align: left;
            transition: height 0.5s, outline 0s;
            overflow: hidden;
            cursor: pointer
        }

        .tile-wrapper {
            position: relative
        }

        .tile-wrapper .newBadge {
            width: 100px;
            height: 25px;
            border-radius: 12.5px;
            background: linear-gradient(269.97deg, #8526FF .02%, #2A26FF 98.5%);
            position: absolute;
            top: 0;
            right: 0;
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: center;
            gap: 4px
        }

        .tile-wrapper .newBadge p {
            font-size: 10px;
            color: white
        }

        .tile-wrapper .newBadge img {
            height: 10px;
            width: 10px
        }

        .tile-wrapper.rav-tile .tile {
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: space-between
        }

        .tile-wrapper.rav-tile .tileIcon {
            height: auto;
            width: 74px;
            margin: 0;
            margin-left: 24px;
            position: relative
        }

        .tile-wrapper.rav-tile .tileIcon img {
            height: 59px;
            width: 74px
        }

        .tile-wrapper.rav-tile .tileIcon .tileStatus {
            position: absolute;
            bottom: 0;
            right: 0
        }

        .tile-wrapper.rav-tile .tileIcon .tileStatus img {
            height: 40px;
            width: 40px
        }

        .tile-wrapper.rav-tile .tileDesc {
            flex: 1;
            margin-top: 0px;
            flex-direction: column
        }

        .tile-wrapper.rav-tile .tileDesc .title {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis
        }

        .tile-wrapper.rav-tile .tileUnlock {
            background-color: white;
            width: 150px;
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
            cursor: pointer
        }

        .tile-wrapper.rav-tile .tileUnlock img {
            height: 59px;
            width: 51px
        }

        .tile-wrapper.rav-tile .tileUnlock p {
            font-size: 15px;
            line-height: 20px;
            font-family: 'Segoe UI Bold';
            margin-top: 10px
        }

        .tile-wrapper.rav-tile .tileUnlock.orange {
            background: linear-gradient(90deg, #FF8F11 0%, #FFBC11 100%)
        }

        .tile-wrapper.rav-tile .tileUnlock.blue {
            background: radial-gradient(96.84% 100% at 49.19% .4%, #4FBFFE 0%, #7644FF 100%)
        }

        .tile-wrapper.rav-tile .tileUnlock.blue p {
            color: white
        }

        .tileIcon {
            height: 59px;
            margin-top: 20px;
            margin-left: 24px;
            margin-right: 24px;
            position: relative
        }

        .tileIcon .tileIconImg {
            height: 60px;
            width: 60px
        }

        .missingComponent {
            background-color: #FBECDB;
            border-top-left-radius: 8px;
            border-bottom-left-radius: 8px;
            position: absolute;
            top: 5px;
            right: -24px
        }

        .missingComponent p {
            width: 122px;
            text-align: center;
            font-size: 14px;
            margin: 6px 10px;
            color: #ff8700
        }

        .tileStatus {
            width: 27px;
            height: 27px;
            margin-left: -15px
        }

        .tileDesc {
            position: relative;
            font-family: 'Segoe UI Bold';
            font-size: 16px;
            line-height: 21px;
            color: var(--primary-font-color);
            margin: 0px 24px;
            margin-top: 17px;
            display: flex;
            flex-direction: row;
            justify-content: space-between
        }

        .tileDesc .title {
            font-size: 22px;
            line-height: 30px
        }

        .tileDesc .subtitle {
            font-family: 'Segoe UI';
            font-size: 12px;
            line-height: 16px
        }

        .tileDesc img {
            color: #9680ff;
            font-weight: normal;
            text-decoration: none;
            float: right;
            font-size: var(--default-font-size);
            transition: 0.5s;
            cursor: pointer
        }

        .tileDesc button {
            float: right;
            margin-top: -6px;
            min-width: auto;
            padding: 0px;
            width: 150px;
            height: 40px;
            font-size: 15px;
            line-height: 19px;
            font-weight: 400;
            border-width: 1.5px
        }

        .tileDesc button .btnLoading {
            display: flex;
            align-items: center;
            justify-content: center;
            gap: 10px
        }

        .tileDesc button .btnLoading img {
            height: 20px;
            width: 20px;
            animation: spin 2s linear infinite
        }

        .tileDesc .dots {
            position: absolute;
            left: 160px;
            bottom: 5px;
            width: 60px
        }

        .titleThreats {
            margin-left: 80px;
            margin-top: -45px;
            color: #808080;
            font-size: 16px
        }

        .tileExpandContent {
            margin: 20px 40px;
            margin-top: 30px;
            font-size: 15px
        }

        .tileExpandContent button {
            float: right;
            margin-top: 20px
        }

        .tileExpandContent p {
            width: 100%;
            height: 70px;
            font-size: 18px
        }

        .tilesSection {
            grid-template-columns:1fr 1fr;
            grid-column-gap: 24px;
            padding-right: 20px
        }

        .tileLocked {
            outline: 2.5px solid var(--orange-color);
            outline-offset: -2.5px
        }

        .tileLocked .tileDesc span {
            opacity: 0.5
        }

        .tileLocked .missingComponent {
            right: -21.5px
        }

        @media screen and (min-width: 1500px) {
            .tileDesc {
                font-size: 16px
            }

            .tileDesc button {
                min-width: 170px;
                padding: 10px 25px
            }

            .tilesSection {
                padding-right: 100px
            }
        }
    </style>
    <style>.risk-capitalize {
            text-transform: capitalize
        }

        .risk-low {
            color: #52b8ff
        }

        .risk-medium {
            color: #ff8f11
        }

        .risk-high {
            color: #ff3b30
        }

        .threatList {
            position: relative
        }

        .spinnerCenter {
            position: absolute;
            width: 100px;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%)
        }

        .btnLoading {
            display: flex;
            align-items: center;
            justify-content: center;
            gap: 10px
        }

        .btnLoading img {
            animation: spin 2s linear infinite
        }

        *:hover > .btnLoading img {
            filter: brightness(0) saturate(100%) invert(100%) sepia(6%) saturate(43%) hue-rotate(256deg) brightness(117%) contrast(100%)
        }
    </style>
    <style></style>
    <style>.osSwitch {
            position: relative;
            display: inline-block;
            width: 34px;
            height: 15.3px
        }

        .osSwitch input {
            opacity: 0;
            width: 0;
            height: 0
        }

        .osSlider {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            border-radius: 34px;
            background-color: #93a0b5;
            transition: 0.4s
        }

        .osSlider:before {
            position: absolute;
            content: '';
            height: 13px;
            width: 13px;
            left: 2px;
            bottom: 1px;
            border-radius: 50%;
            background-color: white;
            transition: 0.4s
        }

        input:checked + .sliderGreen {
            background-color: #04d289
        }

        input:checked + .sliderRed {
            background-color: #ff3b30
        }

        input:not(:checked) + .defaultGreen {
            background-color: #04d289
        }

        input:checked + .osSlider:before {
            transform: translateX(17px)
        }
    </style>
    <style>.confirmNotificationChange {
            width: 856px;
            height: 425px;
            background: white;
            border-radius: 10px;
            overflow: hidden
        }

        .confirmNotificationChange .header {
            height: 130px;
            background-color: var(--modal-header-background);
            display: flex;
            justify-content: center;
            position: relative
        }

        .confirmNotificationChange .header .close {
            position: absolute;
            right: 36px;
            top: 27px;
            cursor: pointer;
            width: 15px;
            height: 15px
        }

        .confirmNotificationChange .header .risk {
            align-self: center;
            width: 80px;
            height: 80px
        }

        .confirmNotificationChange .content p {
            text-align: center;
            font-size: 20px
        }

        .confirmNotificationChange .content .pConfirm {
            margin-top: 50px;
            font-weight: 600
        }

        .confirmNotificationChange .content div {
            display: flex;
            align-items: center;
            justify-content: center;
            margin-top: 30px
        }

        .confirmNotificationChange .content div button {
            margin: 0px 10px;
            padding-top: 0px;
            padding-bottom: 0px;
            height: 40px;
            font-size: 15px
        }
    </style>
    <style>body.react-confirm-alert-body-element {
            overflow: hidden
        }

        .react-confirm-alert-blur {
            filter: url(#gaussian-blur);
            filter: blur(2px);
            -webkit-filter: blur(2px)
        }

        .react-confirm-alert-overlay {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            z-index: 99;
            background: rgba(255, 255, 255, 0.9);
            display: -webkit-flex;
            display: -moz-flex;
            display: -ms-flex;
            display: -o-flex;
            display: flex;
            justify-content: center;
            -ms-align-items: center;
            align-items: center;
            opacity: 0;
            -webkit-animation: react-confirm-alert-fadeIn 0.5s 0.2s forwards;
            -moz-animation: react-confirm-alert-fadeIn 0.5s 0.2s forwards;
            -o-animation: react-confirm-alert-fadeIn 0.5s 0.2s forwards;
            animation: react-confirm-alert-fadeIn 0.5s 0.2s forwards
        }

        .react-confirm-alert-body {
            font-family: Arial, Helvetica, sans-serif;
            width: 400px;
            padding: 30px;
            text-align: left;
            background: #fff;
            border-radius: 10px;
            box-shadow: 0 20px 75px rgba(0, 0, 0, 0.13);
            color: #666
        }

        .react-confirm-alert-svg {
            position: absolute;
            top: 0;
            left: 0
        }

        .react-confirm-alert-body > h1 {
            margin-top: 0
        }

        .react-confirm-alert-body > h3 {
            margin: 0;
            font-size: 16px
        }

        .react-confirm-alert-button-group {
            display: -webkit-flex;
            display: -moz-flex;
            display: -ms-flex;
            display: -o-flex;
            display: flex;
            justify-content: flex-start;
            margin-top: 20px
        }

        .react-confirm-alert-button-group > button {
            outline: none;
            background: #333;
            border: none;
            display: inline-block;
            padding: 6px 18px;
            color: #eee;
            margin-right: 10px;
            border-radius: 5px;
            font-size: 12px;
            cursor: pointer
        }

        @-webkit-keyframes react-confirm-alert-fadeIn {
            from {
                opacity: 0
            }
            to {
                opacity: 1
            }
        }

        @-moz-keyframes react-confirm-alert-fadeIn {
            from {
                opacity: 0
            }
            to {
                opacity: 1
            }
        }

        @-o-keyframes react-confirm-alert-fadeIn {
            from {
                opacity: 0
            }
            to {
                opacity: 1
            }
        }

        @keyframes react-confirm-alert-fadeIn {
            from {
                opacity: 0
            }
            to {
                opacity: 1
            }
        }
    </style>
    <style>.extensionBox {
            background-color: var(--background-color)!important;
            border-radius: 9px;
            margin: 10px;
            padding: 20px;
            padding-bottom: 10px;
            font-size: 16px
        }

        .extensionBox img {
            width: 30px;
            height: 30px
        }

        .extensionBox .toggle {
            float: right
        }

        .extensionBox div {
            display: flex;
            align-items: top;
            justify-content: left;
            overflow: hidden;
            width: 100%
        }

        .extensionBox div span {
            width: 100%;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            padding-left: 10px
        }

        .extensionSection {
            padding: 40px;
            display: grid;
            grid-template-columns:repeat(auto-fit, minmax(calc(100% / 3), 272px));
            align-items: left
        }

        .react-confirm-alert-overlay {
            background: #00000096
        }

        .react-confirm-alert-button-group > button {
            background: var(--tertiary-color);
            font-size: var(--default-font-size)
        }

        .react-confirm-alert-body {
            white-space: pre-line;
            color: var(--primary-font-color);
            font-size: var(--default-font-size)
        }

        .react-confirm-alert-blur {
            filter: none
        }
    </style>
    <style></style>
    <style>.paddingBox {
            padding: 50px 150px
        }

        .obs {
            padding-left: 35px;
            color: #828282
        }

        select {
            margin-left: 10px;
            padding: 5px;
            font-size: 16px;
            border: 1px solid var(--primary-font-color);
            color: var(--primary-font-color)
        }

        div[disabled] {
            opacity: 0.6;
            pointer-events: none
        }

        div[disabled] .selectorScanRadio {
            pointer-events: all
        }

        .sectionIcon {
            margin-left: -70px;
            margin-top: 12px;
            float: left
        }
    </style>
    <style>.typeUntrusted {
            display: flex;
            align-items: center;
            justify-content: center;
            background: #ffebea;
            color: red;
            width: 100px;
            height: 30px;
            font-weight: 400;
            font-size: 16px
        }

        .notif_link {
            text-decoration: underline
        }

        .section {
            position: relative;
            margin-top: 40px
        }

        .section .btnBuyOrange {
            margin-bottom: 30px
        }

        .disallowedMark {
            position: absolute;
            right: -150px;
            width: 222px;
            height: 47px;
            margin-top: -70px
        }

        .disallowedMark div {
            position: relative
        }

        .disallowedMark div span {
            position: absolute;
            left: 55px;
            top: 26%;
            font-weight: 600;
            font-size: 14px;
            color: #fff
        }

        .notifysBoxContent {
            overflow: visible;
            position: relative
        }

        .notifysBoxContent .lc {
            margin-left: 60px
        }

        .notifysBoxContent .lh {
            margin-left: 60px
        }
    </style>
    <style>.confirmNotificationChange {
            width: 856px;
            height: 425px;
            background: white;
            border-radius: 10px;
            overflow: hidden
        }

        .confirmNotificationChange .header {
            height: 130px;
            background-color: var(--modal-header-background);
            display: flex;
            justify-content: center;
            position: relative
        }

        .confirmNotificationChange .header .close {
            position: absolute;
            right: 36px;
            top: 27px;
            cursor: pointer;
            width: 15px;
            height: 15px
        }

        .confirmNotificationChange .header .risk {
            align-self: center;
            width: 80px;
            height: 80px
        }

        .confirmNotificationChange .content p {
            text-align: center;
            font-size: 20px
        }

        .confirmNotificationChange .content .pConfirm {
            margin-top: 50px;
            font-weight: 600
        }

        .confirmNotificationChange .content div {
            display: flex;
            align-items: center;
            justify-content: center;
            margin-top: 30px
        }

        .confirmNotificationChange .content div button {
            margin: 0px 10px;
            padding-top: 0px;
            padding-bottom: 0px;
            height: 40px;
            font-size: 15px
        }
    </style>
    <style>.aboutContent {
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100%;
            padding-top: 0px;
            padding-bottom: 0px
        }

        .versionContent {
            font-size: 20px;
            padding: 30px 80px;
            margin: 40px;
            border: 2.5px solid var(--purple-font-color);
            border-left: 0px;
            border-right: 0px
        }

        .socialLinks {
            padding: 10px
        }

        .socialLinks a {
            margin: 0px 7px
        }

        .legalLinks p {
            margin: 10px 0px
        }

        .legalLinks a {
            font-size: 14px;
            color: var(--purple-font-color)
        }
    </style>
    <style>.scanPage {
            color: var(--active-blue-font-color)
        }

        .scanPage .scanTitleWrapper {
            text-align: center
        }

        .scanPage .scanTitleWrapper .scanTitle {
            margin: 5px 0 15px 0;
            font-size: 28px;
            font-weight: 700;
            font-family: 'Segoe UI Bold'
        }

        .scanPage .scanTitleWrapper .scanSubTitle {
            font-size: 22px;
            color: var(--grey-font-color)
        }

        .scanPage .progressBar {
            margin: 25px 0px;
            background-color: #eff5fd;
            border-radius: 13px
        }

        .scanPage .progressBar div {
            background-color: #04d289;
            height: 12px;
            border-radius: 10px;
            transition: all 1s
        }

        .scanPage .icon {
            width: 25px;
            height: 20px
        }

        .scanPage .scanContent {
            background-color: var(--foreground-color);
            box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.1);
            border-radius: 9px;
            font-size: var(--default-font-size);
            position: absolute;
            min-height: auto;
            padding: 50px 100px 100px;
            width: 60%;
            margin: 0 auto;
            inset: 200px 0 auto 0;
            max-width: 914px
        }

        .scanPage .itemTitle {
            width: 260px;
            margin: 20px 0px;
            margin-left: 15px;
            font-size: 22px;
            font-weight: 600;
            color: var(--grey-font-color)
        }

        .scanPage .contentTitle {
            background-color: var(--dark-blue-background);
            display: flex;
            justify-content: center;
            align-items: center;
            color: var(--foreground-color);
            font-size: 27px;
            width: 300px;
            padding: 10px 30px;
            border-radius: 30px;
            position: absolute;
            inset: -40px 0 0 0;
            margin: 0 auto;
            height: 50px
        }

        .scanPage .contentTitle > div {
            margin-left: 15px
        }

        .scanPage .scanItemWrapper {
            display: flex;
            align-items: center;
            justify-content: space-between;
            border-bottom: 1px solid lightgrey
        }

        .scanPage .scanItemWrapper > div {
            display: flex;
            align-items: center;
            justify-content: center
        }

        .scanPage .scanItemWrapper .inactive {
            color: var(--grey-font-color)
        }

        .scanPage .scanItemWrapper .activate {
            background-color: transparent;
            color: var(--active-blue-font-color)
        }

        .scanPage .contentFooter {
            position: absolute;
            bottom: -100px;
            width: 80%;
            text-align: center;
            display: flex;
            flex-direction: column;
            align-items: center;
            font-size: 20px;
            font-weight: 600
        }
    </style>
    <style>.blockPageWrapper {
            color: var(--primary-font-color)
        }

        .blockPageWrapper .headerContent {
            height: 330px;
            background: #aa413b;
            box-shadow: -3px 0px 3px rgba(0, 0, 0, 0.1);
            border-radius: 0px 0px 22px 22px;
            color: var(--foreground-color);
            padding: 30px 45px
        }

        .blockPageWrapper .headerContent .logoSection {
            margin-left: 125px;
            margin-top: 5px;
            margin-right: 10px
        }

        .blockPageWrapper .headerContent .titleSection {
            text-align: center
        }

        .blockPageWrapper .headerContent .titleSection h1 {
            font-size: 42px;
            margin-bottom: 0px
        }

        .blockPageWrapper .headerContent .titleSection h2 {
            margin-top: 10px;
            font-family: 'Segoe UI';
            font-style: normal;
            font-weight: 600;
            line-height: 173.51%;
            font-size: 24px
        }

        .blockPageWrapper .headerContent .titleSection img {
            width: 90px
        }

        .blockPageWrapper .blockedUrlContent {
            display: flex;
            align-items: center;
            justify-content: center;
            position: relative
        }

        .blockPageWrapper .blockedUrlContent > div {
            height: 75px;
            background-color: #ffe2e2;
            width: 70%;
            max-width: 1000px;
            display: flex;
            align-items: center;
            justify-content: center;
            position: absolute;
            bottom: -40px;
            font-size: 25px;
            border-radius: 8px;
            border: 1px solid #ff0000;
            color: #fc1f1f;
            box-shadow: 0px 0px 10.6714px rgba(0, 0, 0, 0.1);
            padding: 0px 20px
        }

        .blockPageWrapper .blockedUrlContent > div span {
            display: inline-block;
            white-space: nowrap;
            overflow: hidden !important;
            text-overflow: ellipsis
        }

        .blockPageWrapper .actionsWrapper {
            display: flex;
            justify-content: center;
            align-items: center;
            padding: 100px 0 60px;
            margin: auto;
            gap: 45px
        }

        .blockPageWrapper .actionsWrapper button {
            display: flex;
            flex-direction: row;
            justify-content: center;
            align-items: center;
            gap: 10px;
            background: none;
            border: none;
            font-weight: 600;
            font-size: 17px;
            line-height: 17px;
            color: #747d8b
        }

        .blockPageWrapper .actionsWrapper .btnHighlight {
            filter: brightness(0) saturate(100%) invert(14%) sepia(85%) saturate(7105%) hue-rotate(247deg) brightness(98%) contrast(113%)
        }

        .blockPageWrapper .actionsWrapper button:hover {
            filter: brightness(0) saturate(100%) invert(39%) sepia(31%) saturate(565%) hue-rotate(178deg) brightness(91%) contrast(88%)
        }

        .blockPageWrapper .actionsWrapper .btnHighlight:hover {
            filter: brightness(0) saturate(100%) invert(10%) sepia(94%) saturate(7237%) hue-rotate(249deg) brightness(71%) contrast(133%)
        }

        .blockPageWrapper .actionsWrapper img {
            margin-top: 5px;
            width: 20px;
            height: 20px
        }

        .blockPageWrapper .actionsWrapper .InfoIcon {
            margin-top: 0px
        }

        .blockPageWrapper .withoutUpsell {
            padding: 150px 0 60px
        }

        .blockPageWrapper .withoutUpsell button {
            font-size: 20px
        }

        .blockPageWrapper .upgradeProtectionWrapper {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            font-size: 25px;
            gap: 30px
        }

        .blockPageWrapper .upgradeProtectionWrapper div {
            max-width: 800px;
            text-align: center
        }

        .blockPageWrapper .upgradeProtectionWrapper .or {
            display: flex;
            align-items: center;
            justify-content: center;
            gap: 10px
        }

        .blockPageWrapper .upgradeProtectionWrapper .or hr {
            width: 25px;
            height: 0px;
            border: 1px solid #747d8b
        }

        .blockPageWrapper .upgradeProtectionWrapper .levelUpButton {
            display: flex;
            align-items: center;
            justify-content: center;
            gap: 10px;
            width: 320px;
            height: 65px;
            background: #183360;
            border-radius: 107.2px;
            font-size: 20px;
            line-height: 141.51%;
            font-family: 'Segoe UI Bold';
            color: #fff
        }

        .blockPageWrapper .upgradeProtectionWrapper .levelUpButton:hover {
            background: #274d8d
        }

        .blockPageWrapper .blockBtnWrapper {
            display: flex;
            align-items: center;
            cursor: default
        }
    </style>
    <style>.btn-contained {
            min-width: 170px;
            padding: 10px 25px;
            color: var(--foreground-color);
            background-color: var(--orange-color);
            border: none;
            border-radius: 39px;
            font-weight: 600
        }

        .btn-contained:hover {
            background-color: var(--hover-orange-color);
            color: white
        }
    </style>
    <style>.welcomePage .box {
            width: 306px;
            height: 255px;
            background: var(--foreground-color);
            box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.1);
            border-radius: 9px;
            color: var(--primary-font-color);
            font-size: 22px;
            font-weight: bold;
            padding-top: 5px;
            margin: 10px
        }

        .welcomePage .box img {
            margin-bottom: 20px;
            width: 55px;
            height: 55px
        }

        .welcomePage button {
            width: 250px;
            height: 60px;
            margin: 40px 0px;
            margin-bottom: 1px;
            border: 0px;
            background: #ff8f11;
            border-radius: 55.6769px;
            font-weight: bold;
            font-size: 20px;
            color: #ffffff;
            cursor: pointer
        }

        .welcomePage p {
            margin: 5px
        }

        .welcomePage a {
            margin: 0px 7px;
            cursor: pointer;
            color: blue;
            text-decoration: underline
        }
    </style>
    <style>.Sidenav {
            height: 100%;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background: var(--tertiary-color);
            box-shadow: -3px 0px 3px 0px rgba(0, 0, 0, 0.1);
            border-radius: 0px 20px 20px 0px;
            padding-top: 5px;
            zoom: 0.93
        }

        .logo {
            position: absolute;
            height: 37px;
            left: 35px;
            top: 39px
        }

        .premiumBadge {
            position: absolute;
            height: 37px;
            left: 76px;
            top: 65px;
            width: 80px
        }

        .options {
            padding-top: 130px;
            margin-right: 15px
        }

        .btnOption {
            background-color: transparent;
            color: white;
            width: 100%;
            height: 70px;
            margin-left: 15px;
            border: 0px;
            text-align: left;
            font-size: 20px;
            transition: none
        }

        .btnIcons path {
            fill: white;
            stroke: white
        }

        .btnFocused svg path {
            fill: var(--primary-font-color);
            stroke: var(--primary-font-color)
        }

        .btnFocused {
            background: var(--background-color)!important;
            box-shadow: -6px 6px 6px rgba(0, 0, 0, 0.15);
            border-radius: 38px 0px 0px 38px;
            font-weight: 600;
            color: var(--primary-font-color);
            transition: none
        }

        .btnFocused:after {
            content: '';
            position: absolute;
            right: 0;
            margin-top: -100px;
            height: 30px;
            width: 30px;
            border-bottom-right-radius: 50%;
            box-shadow: 0 20px 0 0 var(--background-color) !important;
        }

        .btnFocused:before {
            content: '';
            position: absolute;
            right: 0;
            margin-top: 100px;
            height: 30px;
            width: 30px;
            border-top-right-radius: 50%;
            box-shadow: 0 -20px 0 0 var(--background-color) !important;
        }

        .icon-wrapper {
            position: relative;
            margin-left: 15px;
            margin-right: 20px
        }

        .newIcon {
            height: 20px;
            width: 20px;
            border-radius: 10px;
            background: linear-gradient(269.97deg, #8526FF .02%, #2A26FF 98.5%);
            position: absolute;
            top: -5px;
            right: -5px;
            display: flex;
            align-items: center;
            justify-content: center
        }

        .newIcon img {
            height: 10px;
            width: 10px
        }

        .btnToggle, .btnToggleOff {
            border-radius: 50px;
            padding: 0px;
            border: 0px;
            margin: -8px;
            background-color: transparent;
            position: absolute;
            bottom: 100px;
            float: right
        }

        .btnScanNow {
            background: linear-gradient(var(--tertiary-color), var(--tertiary-color)) padding-box, linear-gradient(#52b8ff, #7253ff) border-box;
            border-radius: 47px;
            border: 3px solid transparent;
            color: white;
            font-size: 22px;
            height: 65px;
            width: 100%;
            margin: 60px 17px
        }

        .btnScanNow span {
            padding-left: 10px
        }
    </style>
    <style>.premiumButton {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 85px;
            border: none;
            background: none;
            border-top: 1.6px solid #3855a1;
            color: #fff;
            font-size: 25px;
            border-radius: 0 0 20px 0;
            display: flex;
            align-items: center;
            justify-content: center;
            gap: 7px
        }

        .premiumButton img {
            width: 32px
        }

        .goldStyleButton {
            font-family: 'Segoe UI Bold';
            color: var(--primary-font-color);
            background: linear-gradient(90deg, #ff8f11 0%, #ffbc11 100%)
        }

        .premiumButton:hover {
            background-color: #11265f
        }

        .goldStyleButton:hover {
            background: #ffcc47
        }

        .premiumButton:active:not(.goldStyleButton) {
            background-color: #22449c
        }
    </style>
    <style>.registerPrompt {
            position: relative;
            width: 585px;
            background: white;
            border-radius: 10px;
            overflow: hidden;
            transition: height 0.5s;
            padding: 36px;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            text-align: center
        }

        .registerPrompt .close {
            top: 30px;
            right: 30px;
            position: absolute;
            cursor: pointer
        }

        .registerPrompt .header {
            width: 442px;
            margin: 0px
        }

        .registerPrompt h1 {
            font-size: 22px;
            color: #183360;
            line-height: 29px;
            text-transform: capitalize
        }

        .registerPrompt p {
            font-size: 14px;
            color: #183360;
            line-height: 19px
        }

        .registerPrompt p.hint {
            font-size: 12px;
            line-height: 15px
        }

        .registerPrompt button {
            background: linear-gradient(180deg, #52b8ff 0%, #7253ff 100%);
            width: 258px;
            height: 58px;
            border-radius: 55px;
            border: none;
            font-weight: 700;
            color: #fff;
            display: flex;
            align-items: center;
            justify-content: center;
            margin: 25px 0
        }
    </style>
    <style>.emailStatus {
            position: relative;
            width: 506px;
            background: white;
            border-radius: 10px;
            overflow: hidden;
            transition: height 0.5s;
            padding: 36px;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            text-align: center
        }

        .emailStatus .close {
            top: 30px;
            right: 30px;
            position: absolute;
            cursor: pointer
        }

        .emailStatus .mainIcon {
            height: 49px;
            width: 49px
        }

        .emailStatus h1 {
            font-size: 22px;
            color: #183360;
            line-height: 29px;
            text-transform: capitalize
        }

        .emailStatus p {
            font-size: 14px;
            color: #183360;
            line-height: 19px
        }

        .emailStatus p a {
            text-decoration: underline
        }
    </style>
    <style>.pickerContent {
            background: #fff;
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            overflow: hidden
        }

        .pickerIframe {
            border: none;
            width: 100%;
            height: 100%
        }
    </style>
    <style>.checkDataleaks {
            display: flex;
            align-items: flex-start;
            gap: 30px;
            padding-top: 35px;
            min-height: none
        }

        .checkDataleaks .icon {
            margin-left: 35px
        }

        .checkDataleaks .dwCheckContent {
            width: 100%;
            max-width: 800px;
            margin-right: 35px
        }

        .checkDataleaks .dwCheckContent b {
            font-family: 'Segoe UI Bold'
        }

        .checkDataleaks .dwCheckContent .emailContent {
            position: relative;
            display: flex;
            gap: 25px;
            margin-top: 30px;
            justify-content: space-between
        }

        .checkDataleaks .dwCheckContent .emailContent div {
            width: calc(100% - 230px)
        }

        .checkDataleaks .dwCheckContent .emailContent div img {
            width: 30px;
            height: 30px;
            position: absolute;
            right: 205px;
            top: 15px
        }

        .checkDataleaks .dwCheckContent .emailContent input {
            font-size: 18px;
            padding: 19px;
            border: 1px solid #dadcd0;
            border-radius: 6px;
            width: 100%
        }

        .checkDataleaks .dwCheckContent .emailContent button {
            min-width: 166px;
            height: 58px;
            background: linear-gradient(180deg, #52b8ff 0%, #7253ff 100%);
            border-radius: 55px;
            border: none;
            font-weight: 700;
            color: #fff;
            display: flex;
            align-items: center;
            justify-content: center;
            gap: 10px
        }

        .checkDataleaks .dwCheckContent .emailContent button .spinner {
            width: 23px;
            height: 23px;
            animation: spin 2s linear infinite
        }

        .checkDataleaks .dwCheckContent .emailContent button:disabled {
            cursor: default;
            opacity: 0.7
        }

        .checkDataleaks .dwCheckContent .redAlert {
            font-size: 16px;
            color: #f00
        }

        .checkDataleaks .dwCheckContent .error-banner {
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: start;
            padding: 8px;
            gap: 4px
        }

        .checkDataleaks .dwCheckContent .error-banner p {
            font-size: 12px
        }

        .checkDataleaks .dwCheckContent .error-banner p.errorText {
            color: #FF3B30
        }

        .checkDataleaks .dwCheckContent .error-banner p.support a {
            color: var(--primary-font-color);
            text-decoration: underline
        }

        .checkDataleaks .dwCheckContent .chkBox-wrapper {
            margin-top: 30px
        }
    </style>
    <style>.upgradeDataleaks {
            position: relative;
            display: flex;
            align-items: flex-start;
            gap: 30px;
            padding-bottom: 25px 0px;
            min-height: none;
            margin-top: 50px;
            background-color: var(--tertiary-color);
            color: #fff
        }

        .upgradeDataleaks .bg {
            height: 100%;
            position: absolute;
            left: 0;
            top: 0;
            bottom: 0;
            z-index: 0
        }

        .upgradeDataleaks .upgradeContent {
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: space-between;
            margin-left: 130px;
            margin-right: 20px
        }

        .upgradeDataleaks .upgradeContent p {
            display: flex;
            align-items: center;
            gap: 10px;
            margin-left: 10px;
            margin-bottom: 8px
        }

        .upgradeDataleaks .upgradeContent b {
            font-family: 'Segoe UI Bold'
        }

        .upgradeDataleaks .upgradeContent .title {
            font-size: 25px;
            margin-bottom: 20px;
            margin-left: 0px
        }

        .upgradeDataleaks .upgradeContent .title img {
            width: 40px
        }
    </style>
    <style>.Breaches {
            margin-bottom: 2px
        }

        .Breaches .breachList .title {
            display: flex;
            align-items: center;
            gap: 7px;
            font-size: 20px;
            line-height: 26px;
            font-weight: 400;
            margin-left: 20px;
            margin-bottom: 24px
        }

        .Breaches .breachList .title img {
            width: 22px;
            height: 22px
        }

        .Breaches .breachList .listContent {
            padding-right: 0;
            height: auto
        }

        .Breaches .breachList .lc {
            padding: 0px
        }

        .Breaches .breachList .lc.lh {
            margin-right: 20px
        }

        .Breaches .breachList .lc.lh div {
            padding: 0px
        }

        .Breaches .breachList .lc div {
            margin: auto 0;
            font-size: 14px;
            line-height: 19px
        }

        .Breaches .breachList .lc div.category {
            display: flex;
            justify-content: flex-start;
            align-items: center;
            gap: 6px;
            padding: 0px
        }

        .Breaches .breachList .lc div.category img {
            height: 18px;
            width: 18px
        }

        .Breaches .breachList .lc div.listItem:nth-child(1) {
            padding-left: 14px !important
        }

        .Breaches .breachList .lc div.listItem:nth-child(2) {
            display: flex;
            flex-direction: column;
            gap: 12px
        }

        .Breaches .breachList .lc div.listItem:nth-child(2) a.desc {
            text-decoration: underline;
            color: #7644FF
        }

        .Breaches .breachList .lc div.listItem.exposed_info {
            max-height: 240px;
            overflow-y: auto;
            display: flex;
            flex-direction: column;
            gap: 12px
        }

        .Breaches .breachList .lc div.listItem.exposed_info p {
            margin: 0;
            word-wrap: break-word;
            overflow: auto;
            word-break: break-all
        }

        .Breaches .breachList .lc div.listItem.severity {
            display: flex;
            justify-content: center
        }

        .Breaches .breachList .lc div.listItem.severity p {
            padding: 4px 12px;
            color: #183360;
            border-radius: 100px;
            text-transform: capitalize;
            text-align: center
        }

        .Breaches .breachList .lc div.listItem.severity.critical p {
            color: #A10000;
            background-color: #FF8585
        }

        .Breaches .breachList .lc div.listItem.severity.high p {
            color: #C60000;
            background-color: #FFD9D9
        }

        .Breaches .breachList .lc div.listItem.severity.medium p {
            color: #A34E00;
            background-color: #FFECC7
        }

        .Breaches .breachList .lc div.listItem.severity.low p {
            color: #195B17;
            background-color: #BDFFD4
        }

        .Breaches .breachList .lc div.listItem.severity.informational p, .Breaches .breachList .lc div.listItem.severity.info p {
            color: #183360;
            background-color: white;
            border: 1px solid #183360
        }

        .Breaches .breachList .lc div.listItem button {
            width: 124px;
            height: 40px;
            background: linear-gradient(269.97deg, #8526FF .02%, #2A26FF 98.5%);
            border-radius: 55px;
            border: none;
            font-weight: 600;
            font-size: 14px;
            line-height: 19px;
            color: #fff;
            display: flex;
            align-items: center;
            justify-content: center;
            gap: 10px
        }

        .breachModal {
            position: relative;
            width: 585px;
            padding: 36px;
            background: white;
            color: #183360;
            border-radius: 10px;
            overflow: hidden;
            transition: height 0.5s;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center
        }

        .breachModal .close {
            top: 30px;
            right: 30px;
            position: absolute;
            cursor: pointer
        }

        .breachModal .desc {
            max-height: 207px;
            overflow: auto;
            width: 100%
        }

        .breachModal .header {
            padding-bottom: 12px;
            border-bottom: 1px solid #C7D5EB;
            width: 100%;
            margin-bottom: 0px
        }

        .breachModal h1 {
            font-size: 22px;
            line-height: 30px;
            font-weight: 700
        }

        .breachModal p {
            margin-top: 26px;
            font-size: 14px;
            line-height: 19px
        }

        .breachModal p b {
            font-family: 'Segoe UI Bold'
        }

        .breachModal button {
            width: 221px;
            height: 57px;
            background: linear-gradient(180deg, #52B8FF 0%, #7253FF 100%);
            border-radius: 55px;
            border: none;
            font-weight: 600;
            font-size: 18px;
            line-height: 24px;
            color: #fff;
            margin-top: 38px
        }
    </style>
    <style>.watchlist {
            padding: 24px 28px
        }

        .watchlist .title {
            display: flex;
            align-items: center;
            gap: 7px;
            font-size: 20px;
            line-height: 26px;
            font-weight: 400
        }

        .watchlist .title img {
            width: 22px;
            height: 22px
        }

        .watchlist .emailsList {
            display: flex;
            margin-top: 16px
        }

        .watchlist .emailsList .emails {
            flex: 1;
            display: flex;
            flex-wrap: wrap;
            flex-direction: row
        }

        .watchlist .emailsList .emails .email {
            padding: 10px 14px;
            background-color: #EDF4FF;
            font-size: 16px;
            line-height: 22px;
            font-weight: 400;
            margin-right: 12px;
            margin-bottom: 12px;
            border-radius: 8px;
            display: flex;
            flex-direction: row;
            align-items: center;
            gap: 8px
        }

        .watchlist .emailsList .emails .email .remove {
            height: 9px;
            width: 9px;
            cursor: pointer
        }

        .watchlist .emailsList .emails .email .warning {
            height: 16px;
            width: 16px
        }

        .watchlist .emailsList .emails .email a {
            text-decoration: underline;
            color: #2A26FF
        }

        .watchlist .emailsList .emails .email i {
            color: cadetblue
        }

        .watchlist .emailsList .addEmail {
            width: 150px;
            justify-content: flex-end;
            display: flex
        }

        .watchlist .emailsList .addEmail button {
            border-radius: 100rem;
            padding: 14px 19px;
            font-size: 16px;
            color: #2A26FF;
            background-clip: text;
            box-shadow: 0 0 6px 0 rgba(157, 96, 212, 0.5);
            border: solid 3px transparent;
            background-image: linear-gradient(rgba(255, 255, 255, 0), rgba(255, 255, 255, 0)), linear-gradient(269.97deg, #8526FF .02%, #2A26FF 98.5%);
            background-origin: border-box;
            background-clip: content-box, border-box;
            box-shadow: 2px 1000px 1px #fff inset;
            width: 145px;
            max-height: 55px
        }

        .watchlist .emailsList .addEmail button img {
            height: 12px;
            width: 12px;
            margin-right: 12px
        }
    </style>
    <style>.addEmailModal {
            position: relative;
            width: 585px;
            background: white;
            color: #183360;
            border-radius: 10px;
            overflow: hidden;
            transition: height 0.5s
        }

        .addEmailModal .addEmailform {
            position: relative;
            padding: 36px;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center
        }

        .addEmailModal .close {
            top: 30px;
            right: 30px;
            position: absolute;
            cursor: pointer
        }

        .addEmailModal .addEmailLogo {
            height: 86px;
            width: 86px;
            margin-bottom: 24px;
            position: relative
        }

        .addEmailModal .desc {
            max-height: 207px;
            overflow: auto
        }

        .addEmailModal .header {
            padding-bottom: 12px;
            border-bottom: 1px solid #C7D5EB;
            width: 100%;
            margin-bottom: 0px
        }

        .addEmailModal h1 {
            font-size: 30px;
            line-height: 40px;
            font-weight: 700;
            max-width: 334px;
            text-align: center;
            text-transform: capitalize;
            margin: 0px
        }

        .addEmailModal p {
            font-size: 14px;
            line-height: 19px
        }

        .addEmailModal p a {
            text-decoration: underline;
            color: #00e
        }

        .addEmailModal button {
            width: 221px;
            height: 57px;
            background: linear-gradient(180deg, #52B8FF 0%, #7253FF 100%);
            border-radius: 55px;
            border: none;
            font-weight: 600;
            font-size: 18px;
            line-height: 24px;
            color: #fff;
            margin-top: 38px;
            display: flex;
            justify-content: center;
            align-items: center;
            gap: 4px
        }

        .addEmailModal button .spinner {
            width: 23px;
            height: 23px;
            animation: spin 2s linear infinite
        }

        .addEmailModal button:disabled {
            opacity: 0.25;
            cursor: default
        }

        .addEmailModal .email-wrapper {
            margin: 28px 0
        }

        .addEmailModal .emailInput {
            font-size: 18px;
            padding: 19px;
            border: 1px solid #dadcd0;
            border-radius: 6px;
            width: 376px
        }

        .addEmailModal .emailInput:focus {
            outline: none !important;
            border: 3px solid #3285e0
        }

        .addEmailModal .invalidInput, .addEmailModal .invalidInput:focus {
            border: 3px solid #d93025
        }

        .addEmailModal .invalidEmail {
            width: 350px;
            color: #d93025;
            text-align: left;
            font-size: 15px;
            margin-top: 5px;
            margin-left: 3px
        }

        .addEmailModal .consentContainer {
            max-width: 376px
        }

        .addEmailModal .support-banner {
            background: #ECF3FF;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            padding: 18px
        }

        .removeEmailModal {
            position: relative;
            width: 550px;
            background: white;
            border-radius: 10px;
            overflow: hidden;
            transition: height 0.5s;
            padding: 32px;
            display: flex;
            flex-direction: column;
            align-items: flex-start
        }

        .removeEmailModal p {
            font-weight: 400;
            font-size: 15px
        }

        .removeEmailModal .buttonContainer {
            margin-top: 2em;
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            align-items: center;
            width: 60%
        }

        .removeEmailModal .buttonContainer button {
            border-radius: 100px;
            padding: 14px 16px;
            width: 150px
        }

        .removeEmailModal .buttonContainer button .spinner {
            width: 23px;
            height: 23px;
            color: #00e;
            animation: spin 2s linear infinite
        }

        .removeEmailModal .buttonContainer button:disabled {
            opacity: 0.25;
            cursor: default
        }
    </style>
    <style>.leaked {
            padding: 24px 28px;
            min-height: 277px;
            display: flex;
            flex-direction: column
        }

        .leaked .title {
            font-size: 20px;
            line-height: 26px;
            font-weight: 400;
            height: 26px;
            text-overflow: ellipsis;
            white-space: nowrap;
            overflow: hidden
        }

        .leaked .verify-email {
            flex: 1;
            display: flex;
            justify-content: center;
            align-items: center;
            flex-direction: column;
            gap: 20px
        }

        .leaked .verify-email .email-icon {
            height: 60px;
            width: 60px;
            border-radius: 30px;
            background-color: #C7D5EB66;
            display: flex;
            justify-content: center;
            align-items: center
        }

        .leaked .verify-email .email-icon img {
            height: 25px;
            width: 32px
        }

        .leaked .verify-email p {
            font-size: 16px;
            line-height: 22px;
            color: #747D8B
        }

        .leaked .exposed-data {
            flex: 1;
            display: flex;
            flex-direction: row;
            flex-wrap: wrap
        }

        .leaked .exposed-data .category-info {
            width: 25%;
            min-width: 200px;
            min-height: 70px;
            display: flex;
            flex-direction: row;
            gap: 12px
        }

        .leaked .exposed-data .category-info .img-wrapper {
            display: flex;
            justify-content: flex-start;
            align-items: center
        }

        .leaked .exposed-data .category-info .img-wrapper .img-bg {
            height: 60px;
            width: 60px;
            border-radius: 30px;
            background: rgba(199, 213, 235, 0.28);
            display: flex;
            justify-content: center;
            align-items: center
        }

        .leaked .exposed-data .category-info .img-wrapper .img-bg img {
            width: 30px;
            height: 30px
        }

        .leaked .exposed-data .category-info .info-wrapper {
            font-size: 14px;
            line-height: 19px;
            color: #183360;
            display: flex;
            justify-content: center;
            flex-direction: column;
            align-items: flex-start;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis
        }

        .leaked .exposed-data .category-info .info-wrapper p {
            font-family: 'Segoe UI Bold';
            padding: 2px 13px;
            background-color: #EBF0F3;
            border-radius: 20px
        }

        .leaked .clickable {
            cursor: pointer
        }
    </style>
    <style>.noDataleaks {
            display: flex;
            align-items: flex-start;
            gap: 30px;
            padding-top: 35px;
            padding-bottom: 50px;
            min-height: none;
            margin-bottom: 30px;
            margin-top: 30px
        }

        .noDataleaks .icon {
            margin-left: 35px
        }

        .noDataleaks .noDwContent {
            width: 100%;
            max-width: 800px;
            margin-right: 35px
        }

        .noDataleaks .noDwContent b {
            font-family: 'Segoe UI Bold'
        }

        .noDataleaks .noDwContent .title {
            font-size: 25px
        }

        .noDataleaks .noDwContent p {
            margin-bottom: 30px
        }
    </style>
    <style>.Dataleaks {
            padding-top: 35px;
            padding-bottom: 40px;
            min-height: none;
            margin-bottom: 30px;
            margin-top: 30px
        }

        .Dataleaks .headerBreachesContent {
            display: flex;
            align-items: flex-start;
            gap: 30px
        }

        .Dataleaks .headerBreachesContent .icon {
            margin: 5px;
            margin-left: 35px
        }

        .Dataleaks .headerBreachesContent .dwContent {
            width: 100%;
            max-width: 800px;
            margin-right: 35px
        }

        .Dataleaks .headerBreachesContent .dwContent b {
            font-family: 'Segoe UI Bold'
        }

        .Dataleaks .headerBreachesContent .dwContent .title {
            font-size: 25px
        }

        .Dataleaks .headerBreachesContent .dwContent .breachesDetails {
            display: flex;
            margin-top: 40px;
            gap: 25px
        }

        .Dataleaks .headerBreachesContent .dwContent .breachesDetails .box {
            display: flex;
            align-items: center;
            gap: 15px;
            width: 240px;
            padding: 15px;
            background: #fbfbfb;
            border: 1px solid #c7d5eb;
            border-radius: 8px
        }

        .Dataleaks .showList {
            text-align: center
        }

        .Dataleaks .showList hr {
            border: none;
            border-bottom: 1px solid #e4e4e4;
            margin: 35px 114px
        }

        .Dataleaks .showList button {
            background: none;
            border: none;
            font-weight: 600;
            font-size: 18px;
            background: linear-gradient(269.97deg, #8526ff .02%, #2a26ff 98.5%);
            -webkit-background-clip: text;
            -webkit-text-fill-color: transparent;
            background-clip: text
        }

        .Dataleaks .showList button img {
            margin-left: 5px
        }

        .Dataleaks .showList button .arrowReverse {
            transform: rotate(180deg)
        }
    </style>
    <style>.header {
            margin-bottom: 40px
        }

        .boxContent {
            margin-bottom: 24px
        }
    </style>
    <style>.loginModal {
            position: relative;
            width: 585px;
            background: white;
            color: #183360;
            border-radius: 10px;
            overflow: hidden;
            transition: height 0.5s
        }

        .loginModal .loginForm {
            position: relative;
            padding: 36px;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center
        }

        .loginModal .upgrade-banner {
            background: #ECF3FF;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            padding: 18px
        }

        .loginModal .error-banner {
            background: #FFF2F2;
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: center;
            padding: 8px;
            gap: 4px
        }

        .loginModal .error-banner p {
            font-size: 12px
        }

        .loginModal .error-banner p.errorText {
            color: #FF3B30
        }

        .loginModal .error-banner p.support a {
            color: var(--primary-font-color);
            text-decoration: underline
        }

        .loginModal .close {
            top: 30px;
            right: 30px;
            position: absolute;
            cursor: pointer
        }

        .loginModal h1 {
            font-family: 'Segoe UI Bold';
            color: var(--primary-font-color);
            font-size: 26px;
            margin: 0px;
            line-height: 35px;
            text-align: center;
            width: 430px;
            margin-bottom: 8px
        }

        .loginModal p {
            color: var(--primary-font-color);
            font-size: 14px;
            line-height: 18px;
            text-align: center;
            max-width: 465px;
            margin: 0px
        }

        .loginModal p a {
            text-decoration: underline
        }

        .loginModal p.forgot_pwd {
            margin-top: 15px
        }

        .loginModal .loginlogo {
            height: 67px;
            width: 56px;
            margin-bottom: 24px;
            position: relative
        }

        .loginModal .input {
            font-size: 16px;
            font-weight: 400;
            padding: 12px;
            border: 1px solid #93A0B5;
            border-radius: 6px;
            width: 375px;
            color: var(--primary-font-color)
        }

        .loginModal .input:focus {
            outline: none !important;
            border: 1px solid #3285e0
        }

        .loginModal .invalidInput, .loginModal .invalidInput:focus {
            border: 1px solid #d93025
        }

        .loginModal .invalidEmail {
            width: 350px;
            color: #d93025;
            font-size: 12px;
            margin-top: 12px;
            text-align: left
        }

        .loginModal .inputTitle {
            width: 350px;
            font-size: 12px;
            margin: 12px 0 8px 0;
            text-align: left
        }

        .loginModal .form {
            margin: 12px 0 24px 0
        }

        .loginModal .pwd-input {
            position: relative
        }

        .loginModal .pwd-input .showPass {
            height: 15px;
            width: 22px;
            position: absolute;
            top: 16px;
            right: 12px;
            cursor: pointer
        }

        .loginModal .buttonLogin {
            width: 258px;
            height: 57px;
            font-family: 'Segoe UI Bold';
            background: linear-gradient(180deg, #52B8FF 0%, #7253FF 100%);
            font-size: 18px;
            border-radius: 56px;
            border: none;
            color: #ffffff;
            cursor: pointer;
            display: flex;
            justify-content: center;
            align-items: center;
            gap: 4px
        }

        .loginModal .buttonLogin:disabled {
            opacity: 0.25;
            cursor: default
        }

        .loginModal .buttonLogin .spinner {
            width: 23px;
            height: 23px;
            animation: spin 2s linear infinite
        }
        a {
            text-decoration: none !important;
        }
        td , th{
            text-align: center !important;
        }

        .btn-primary{
            min-width: 100px!important;
            height: 40px;
            background: linear-gradient(180deg, #52b8ff 0%, #7253ff 100%)!important;
            border-radius: 55px!important;
            border: none!important;
            font-weight: 700!important;
            color: #fff!important;
            display: flex!important;
            align-items: center!important;
            justify-content: center!important;
            gap: 10px!important;
        }


        input{
            width: auto !important;
        }


        .switch {
            /* switch */
            --switch-width: 46px;
            --switch-height: 24px;
            --switch-bg: rgb(131, 131, 131);
            --switch-checked-bg: rgb(0, 218, 80);
            --switch-offset: calc((var(--switch-height) - var(--circle-diameter)) / 2);
            --switch-transition: all .2s cubic-bezier(0.27, 0.2, 0.25, 1.51);
            /* circle */
            --circle-diameter: 18px;
            --circle-bg: #fff;
            --circle-shadow: 1px 1px 2px rgba(146, 146, 146, 0.45);
            --circle-checked-shadow: -1px 1px 2px rgba(163, 163, 163, 0.45);
            --circle-transition: var(--switch-transition);
            /* icon */
            --icon-transition: all .2s cubic-bezier(0.27, 0.2, 0.25, 1.51);
            --icon-cross-color: var(--switch-bg);
            --icon-cross-size: 6px;
            --icon-checkmark-color: var(--switch-checked-bg);
            --icon-checkmark-size: 10px;
            /* effect line */
            --effect-width: calc(var(--circle-diameter) / 2);
            --effect-height: calc(var(--effect-width) / 2 - 1px);
            --effect-bg: var(--circle-bg);
            --effect-border-radius: 1px;
            --effect-transition: all .2s ease-in-out;
        }

        .switch input {
            display: none;
        }

        .switch {
            display: inline-block;
        }

        .switch svg {
            -webkit-transition: var(--icon-transition);
            -o-transition: var(--icon-transition);
            transition: var(--icon-transition);
            position: absolute;
            height: auto;
        }

        .switch .checkmark {
            width: var(--icon-checkmark-size);
            color: var(--icon-checkmark-color);
            -webkit-transform: scale(0);
            -ms-transform: scale(0);
            transform: scale(0);
        }

        .switch .cross {
            width: var(--icon-cross-size);
            color: var(--icon-cross-color);
        }

        .slider {
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            width: var(--switch-width);
            height: var(--switch-height);
            background: var(--switch-bg);
            border-radius: 999px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            position: relative;
            -webkit-transition: var(--switch-transition);
            -o-transition: var(--switch-transition);
            transition: var(--switch-transition);
            cursor: pointer;
        }

        .circle {
            width: var(--circle-diameter);
            height: var(--circle-diameter);
            background: var(--circle-bg);
            border-radius: inherit;
            -webkit-box-shadow: var(--circle-shadow);
            box-shadow: var(--circle-shadow);
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-transition: var(--circle-transition);
            -o-transition: var(--circle-transition);
            transition: var(--circle-transition);
            z-index: 1;
            position: absolute;
            left: var(--switch-offset);
        }

        .slider::before {
            content: "";
            position: absolute;
            width: var(--effect-width);
            height: var(--effect-height);
            left: calc(var(--switch-offset) + (var(--effect-width) / 2));
            background: var(--effect-bg);
            border-radius: var(--effect-border-radius);
            -webkit-transition: var(--effect-transition);
            -o-transition: var(--effect-transition);
            transition: var(--effect-transition);
        }

        /* actions */

        .switch input:checked+.slider {
            background: var(--switch-checked-bg);
        }

        .switch input:checked+.slider .checkmark {
            -webkit-transform: scale(1);
            -ms-transform: scale(1);
            transform: scale(1);
        }

        .switch input:checked+.slider .cross {
            -webkit-transform: scale(0);
            -ms-transform: scale(0);
            transform: scale(0);
        }

        .switch input:checked+.slider::before {
            left: calc(100% - var(--effect-width) - (var(--effect-width) / 2) - var(--switch-offset));
        }

        .switch input:checked+.slider .circle {
            left: calc(100% - var(--circle-diameter) - var(--switch-offset));
            -webkit-box-shadow: var(--circle-checked-shadow);
            box-shadow: var(--circle-checked-shadow);
        }

        /*.btn-default{*/
        /*    min-width: 100px!important;*/
        /*    height: 40px;*/
        /*    background: linear-gradient(180deg, #def5f5 0%, #aae8d0 100%)!important;*/
        /*    border-radius: 55px!important;*/
        /*    border: none!important;*/
        /*    font-weight: 700!important;*/
        /*    color: #fff!important;*/
        /*    display: flex!important;*/
        /*    align-items: center!important;*/
        /*    justify-content: center!important;*/
        /*    gap: 10px!important;*/
        /*}*/
    </style>
    <style>
        * {box-sizing: border-box}
        body {font-family: Verdana, sans-serif; margin:0}
        .mySlides {display: none}
        img {vertical-align: middle;}

        /* Slideshow container */
        .slideshow-container {
            max-width: 415px;
            position: relative;
            margin: auto;
        }

        /* Next & previous buttons */
        .prev, .next {
            cursor: pointer;
            position: absolute;
            top: 50%;
            width: auto;
            padding: 16px;
            margin-top: -22px;
            color: white;
            font-weight: bold;
            font-size: 18px;
            transition: 0.6s ease;
            border-radius: 0 3px 3px 0;
            user-select: none;
        }

        /* Position the "next button" to the right */
        .next {
            right: 0;
            border-radius: 3px 0 0 3px;
        }

        /* On hover, add a black background color with a little bit see-through */
        .prev:hover, .next:hover {
            background-color: rgba(0,0,0,0.8);
        }

        /* Caption text */
        .text {
            color: #f2f2f2;
            font-size: 15px;
            padding: 8px 12px;
            position: absolute;
            bottom: 8px;
            width: 100%;
            text-align: center;
        }

        /* Number text (1/3 etc) */
        .numbertext {
            color: #f2f2f2;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
        }

        /* The dots/bullets/indicators */
        .dot {
            cursor: pointer;
            height: 15px;
            width: 15px;
            margin: 0 2px;
            background-color: #bbb;
            border-radius: 50%;
            display: inline-block;
            transition: background-color 0.6s ease;
        }

        .active, .dot:hover {
            background-color: #717171;
        }

        /* Fading animation */
        .fade {
            animation-name: fade;
            animation-duration: 1.5s;
        }

        @keyframes fade {
            from {opacity: .4}
            to {opacity: 1}
        }

        /* On smaller screens, decrease text size */
        @media only screen and (max-width: 300px) {
            .prev, .next,.text {font-size: 11px}
        }

        i{
            font-size: 25px;
        }


    </style>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Homy Hon</title>
</head>
<body>
<div id="app-container">
    <div id="root">
        <div class="Sidenav" style="width: 312px;">
            <div class="options"><a href="{{route('charts.index')}}">
                    <button class="btnOption flex jcl aic charts">
                        <div class="icon-wrapper"><img class="btnIcons" src=""><i class="fa-solid fa-house"></i></div>
                        <span style="display: inline;">Home</span></button>
                </a><a href="{{route('accounts.index')}}">
                    <button class="btnOption flex jcl aic accounts">
                        <div class="icon-wrapper" style="font-size: 25px"><img class="btnIcons" src=""><i class="fa-solid fa-user"></i></div>
                        <span style="display: inline;">Accounts </span></button>
                </a><a href="{{route('ads.index')}}">
                    <button class="btnOption flex jcl aic ads">
                        <div class="icon-wrapper"><img class="btnIcons" src=""><i class="fa fa-bullhorn"></i></div>
                        <span style="display: inline;">Ads </span></button>
                </a><a href="{{route('orders.index')}}">
                    <button class="btnOption flex jcl aic property">
                        <div class="icon-wrapper"><img class="btnIcons" src=""><i class="fa fa-building"></i></div>
                        <span style="display: inline;">Property orders</span></button>
                </a><a href="{{route('ordersContract.index')}}">
                    <button class="btnOption flex jcl aic Contracts">
                        <div class="icon-wrapper"><img class="btnIcons" src=""><i class="fa-solid fa-pen-to-square"></i></div>
                        <span style="display: inline;">Contracts </span></button>
                </a><a href="{{route('subscriptions.index')}}">
                    <button class="btnOption flex jcl aic subscriptions">
                        <div class="icon-wrapper"><img class="btnIcons" src=""><i class="fa-solid fa-credit-card"></i></div>
                        <span style="display: inline;">Subscriptions </span></button>
                </a><a href="{{route('wallets.index')}}">
                    <button class="btnOption flex jcl aic wallets">
                        <div class="icon-wrapper"><img class="btnIcons" src=""><i class="fa fa-wallet"></i></div>
                        <span style="display: inline;">Wallets </span></button>
                </a></div>


            <div style="text-align: right; padding-right: 30px;">
                <button class="btnToggleOff" style="transform: none;"><svg width="45" height="45" viewBox="0 0 45 45" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle r="22.5" transform="matrix(-1 0 0 1 22.5 22.5)" fill="#05153F"/>
<path d="M25.1255 29.625L18.3755 22.875" stroke="white" stroke-width="2" stroke-linecap="round"/>
<path d="M18.3755 22.875L25.1255 16.125" stroke="white" stroke-width="2" stroke-linecap="round"/>
</svg>
</button>
            </div>
            <div style="text-align: right; padding-right: 30px;">
                <button class="btnToggle" style="transform: none;"><svg width="45" height="45" viewBox="0 0 45 45" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle r="22.5" transform="matrix(-1 0 0 1 22.5 22.5)" fill="#05153F"/>
<path d="M25.1255 29.625L18.3755 22.875" stroke="white" stroke-width="2" stroke-linecap="round"/>
<path d="M18.3755 22.875L25.1255 16.125" stroke="white" stroke-width="2" stroke-linecap="round"/>
</svg>
</button>
            </div>
            <div class="flex aic jcc">
                <button class="btnScanNow flex aic jcc"><img src=""><i class="fa fa-arrow-down"></i><span
                        style="display: inline;">Download App</span></button>
            </div>
        </div>
        <div class="btnProfile"><svg width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M6.66667 7.66676C6.66667 6.31123 7.22857 5.0112 8.22876 4.05268C9.22896 3.09417 10.5855 2.55568 12 2.55568C13.4145 2.55568 14.7711 3.09417 15.7712 4.05268C16.7715 5.0112 17.3333 6.31123 17.3333 7.66676C17.3333 9.02234 16.7715 10.3223 15.7712 11.2808C14.7711 12.2394 13.4145 12.7778 12 12.7778C10.5855 12.7778 9.22896 12.2394 8.22876 11.2808C7.22857 10.3223 6.66667 9.02234 6.66667 7.66676ZM17.0987 13.5739C18.3673 12.5682 19.2813 11.2117 19.7157 9.68972C20.1503 8.16764 20.0841 6.55431 19.5264 5.06967C18.9687 3.58504 17.9465 2.3015 16.5993 1.39418C15.2523 0.486869 13.6459 0 11.9993 0C10.3528 0 8.74637 0.486869 7.39928 1.39418C6.05217 2.3015 5.03001 3.58504 4.47228 5.06967C3.91453 6.55431 3.8484 8.16764 4.28289 9.68972C4.71737 11.2117 5.63132 12.5682 6.9 13.5739C2.648 15.0088 0 18.2045 0 21.7222C0 22.0611 0.14048 22.3862 0.39052 22.6257C0.640573 22.8655 0.979707 23 1.33333 23C1.68696 23 2.02609 22.8655 2.27615 22.6257C2.52619 22.3862 2.66667 22.0611 2.66667 21.7222C2.66667 18.8217 5.80667 15.3334 12 15.3334C18.1933 15.3334 21.3333 18.8217 21.3333 21.7222C21.3333 22.0611 21.4739 22.3862 21.7239 22.6257C21.9739 22.8655 22.3131 23 22.6667 23C23.0203 23 23.3595 22.8655 23.6095 22.6257C23.8595 22.3862 24 22.0611 24 21.7222C24 18.2045 21.3547 15.0088 17.0987 13.5739Z" fill="url(#paint0_radial_4613_17013)"/>
<defs>
<radialGradient id="paint0_radial_4613_17013" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(11.8051 0.0913616) rotate(90) scale(23 23.2412)">
<stop stop-color="#4FBFFE"/>
<stop offset="1" stop-color="#7644FF"/>
</radialGradient>
</defs>
</svg>
</div>
        <div class="popupContent">
            <div class="popupMenu">
                    <button class="flex aic"><i class="fa-solid fa-address-card"></i><img src="">My Profile</button>
                <form action="{{route('auth')}}">
                    <button class="flex aic"><i class="fa-solid fa-right-from-bracket"></i><img src="">Log Out</button>
                </form>

            </div>
        </div>
        <div class="App" style="margin-left: 20px;">
            <div class="content">
                <div class="sectionContent ">
                    <span>
                        <h1 class="">Homy Hon Dashboard</h1>
                    </span>
                    <div class="search flex aic mb-5"><svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M7.15239 4.11849e-05C3.2132 4.11849e-05 0 3.21311 0 7.15243C0 11.0917 3.21307 14.3048 7.15239 14.3048C8.79134 14.3048 10.3059 13.753 11.5146 12.8196L16.4175 17.7294C16.7779 18.0899 17.3693 18.0899 17.7297 17.7294C18.0901 17.369 18.0901 16.7849 17.7297 16.4245L12.8124 11.5072C13.7436 10.2994 14.3049 8.78927 14.3049 7.15239C14.3049 3.2132 11.0918 0 7.15251 0L7.15239 4.11849e-05ZM7.15239 1.84585C10.0939 1.84585 12.4587 4.21079 12.4587 7.15214C12.4587 10.0937 10.0937 12.4584 7.15239 12.4584C4.21071 12.4584 1.8461 10.0935 1.8461 7.15214C1.8461 4.21046 4.21104 1.84585 7.15239 1.84585Z" fill="#93A0B5"/>
</svg>

                        <form action="--}}
            {{ route('accounts.search') }}
                            " method="get"
                              class="col-12 col-lg-auto mb-3 mb-lg-0 me-lg-3">
                            <input type="text" placeholder="Search" id="myInputUsers">
                        </form>
                    </div>
                </div>


                <div class="boxContent undefined" style="width: 1800px;">

                    @yield('content')

                </div>
            </div>
        </div>
        <span class="helpContent"><button class="btnHelp"><svg width="41" height="36" viewBox="0 0 41 36" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M21.2061 0.251953C11.9158 0.251953 4.41181 7.80096 4.31059 17.2537C4.28508 19.6358 4.73689 21.9069 5.57285 23.9764C5.57767 23.978 5.58248 23.9796 5.58738 23.9812C6.19828 24.1844 6.76431 23.5702 6.54145 22.9567C5.90233 21.197 5.56018 19.291 5.57741 17.3013C5.65345 8.54757 12.603 1.54143 21.2061 1.54143C29.838 1.54143 36.8354 8.66173 36.8354 17.4449C36.8354 26.2282 29.838 33.3484 21.2061 33.3484V34.6379C30.5378 34.6379 38.1027 26.9404 38.1027 17.4449C38.1027 7.94951 30.5378 0.251953 21.2061 0.251953Z" fill="white"/>
<path d="M21.2061 0.251953C11.9158 0.251953 4.41181 7.80096 4.31059 17.2537C4.28508 19.6358 4.73689 21.9069 5.57285 23.9764C5.57767 23.978 5.58248 23.9796 5.58738 23.9812C6.19828 24.1844 6.76431 23.5702 6.54145 22.9567C5.90233 21.197 5.56018 19.291 5.57741 17.3013C5.65345 8.54757 12.603 1.54143 21.2061 1.54143C29.838 1.54143 36.8354 8.66173 36.8354 17.4449C36.8354 26.2282 29.838 33.3484 21.2061 33.3484V34.6379C30.5378 34.6379 38.1027 26.9404 38.1027 17.4449C38.1027 7.94951 30.5378 0.251953 21.2061 0.251953" stroke="white" stroke-width="0.3"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M23.6196 33.4098C23.6196 34.7662 22.5389 35.8659 21.2058 35.8659C19.8727 35.8659 18.792 34.7662 18.792 33.4098C18.792 32.0533 19.8727 30.9536 21.2058 30.9536C22.5389 30.9536 23.6196 32.0533 23.6196 33.4098Z" fill="white"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M35.6885 11.3052C36.4936 13.3683 36.9418 15.6539 36.9418 18.0596C36.9418 20.4652 36.4936 22.7508 35.6885 24.8139C38.442 24.1807 40.5161 21.3976 40.5161 18.0596C40.5161 14.7215 38.442 11.9385 35.6885 11.3052Z" fill="white"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M0.688477 17.5315C0.688477 20.3472 2.85934 22.7345 5.86534 23.567C6.20856 23.6621 6.52694 23.3813 6.39526 23.0973C5.59567 21.3726 5.15623 19.4954 5.15623 17.5315C5.15623 15.3139 5.71654 13.207 6.72296 11.3052C3.281 11.8889 0.688477 14.4544 0.688477 17.5315Z" fill="white"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M21.1977 7.6211C15.916 7.62553 11.585 12.1442 11.5505 17.6882C11.5367 19.9053 12.2015 21.9584 13.3393 23.633C13.4733 23.8303 13.5101 24.0824 13.436 24.3117L11.8459 29.229C11.741 29.5532 12.0705 29.8435 12.3565 29.6788L16.8046 27.1172C16.9972 27.0063 17.2273 26.9953 17.4295 27.0857C18.6471 27.6296 19.9928 27.9188 21.4061 27.8887C26.661 27.7766 30.8617 23.2709 30.8606 17.7539C30.8596 12.1548 26.533 7.61667 21.1977 7.6211Z" fill="white"/>
</svg>
</button><div
                class="btnHelpSwipe"><div>Need help?</div></div></span>
    </div>
</div>
<div class="ReactModalPortal"></div>
<script src="https://kit.fontawesome.com/62b8175b89.js" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        }
    });
</script>
<script>

    $(document).ready(function () {

        $(".btnToggle").click(function () {
            console.log('toggle')
            $(".App").css("margin-left", "0px");
            $(".Sidenav").css("width", "101px");
            $("button span").css("display", "none");
            $(".btnToggle").css("display", "none");
        });

        $(".btnToggleOff").click(function () {
            console.log('id')
            $(".App").css("margin-left", "20px");
            $(".Sidenav").css("width", "312px");
            $("button span").css("display", "inline");
            $(".btnToggle").css("display", "inline");
        });
    });

</script>

<script>
    $(document).ready(function () {
        $("#myInputUsers").on("keyup", function () {
            var value = $(this).val().toLowerCase();
            $("#myTable tr").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                $("tr:first").fadeIn();
            });
        });
    });
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous">

</script>
    @yield('scripts')
<script>
    let slideIndex = 1;
    showSlides(slideIndex);

    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

    function currentSlide(n) {
        showSlides(slideIndex = n);
    }

    function showSlides(n) {
        let i;
        let slides = document.getElementsByClassName("mySlides");
        let dots = document.getElementsByClassName("dot");
        if (n > slides.length) {slideIndex = 1}
        if (n < 1) {slideIndex = slides.length}
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " active";
    }
</script>

</body>
</html>

