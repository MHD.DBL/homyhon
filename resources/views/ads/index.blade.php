@extends('home.home')

@section('content')

    <h1 class="mb- text-center">Ads Management</h1>

    <div class="bg-light p-4 rounded" style="height: 700px !important;">

        <div class="row justify-content-end">
            <div class="col-11">
                <h1>Ads</h1>
            </div>
            <div class="col-1">
                <a href="{{ route('ads.create') }}" class="btn btn-primary btn-sm float-right"
                   style="margin: 0px;">Add new ads</a>
            </div>
        </div>


        <div style=" height: 100% !important;
            width: auto !important;
            overflow: auto !important;">
            <table class="table table-striped" id="myTable" style="">
                <thead>
                <tr>
                    <th scope="col"
                        width="1%"
                    >ID
                    </th>
                    <th scope="col"
                        width="15%"
                    >Image
                    </th>
                    <th scope="col"
                        width="15%"
                    >Company Name
                    </th>

                    <th scope="col"
                        width="15%"
                    >Link
                    </th>
                    <th scope="col"
                        width="15%"
                    >Description
                    </th>
                    <th scope="col"
                        width="15%"
                    >Actions
                    </th>

                </tr>
                </thead>
                <tbody>
                @foreach($ads as $ad)
                    <tr>
                        <th scope="row">{{ $ad->id }}</th>
                        <td>
                            <div class="d-flex justify-content-center">
                                 @if(isset($ad->img->path))
                                <a data-fslightbox="" href="{{$ad->img->path}}" target="_blank"><img src="{{$ad->img->path}}" class="img-thumbnail" style="width:75px; height: 75px; border-radius: 70px;    border: solid;"></a>
                                 @else
                                 <img src="" class="img-thumbnail" style="width:75px; height: 75px; border-radius: 70px;    border: solid;">
                                 @endif
                            </div>
                        </td>
                        <td>{{ $ad->companyName}}</td>
                        <td>
                            @if(isset($ad->link))
                            <a href="{{ $ad->link }}" target="_blank">Go To  <b>{{ $ad->companyName}}</b> Website</a>
                            @else
                                ---
                            @endif
                        </td>
                        <td>{{ $ad->description }}</td>
                        <td>
                            <a href="{{ route('ads.show', $ad->id) }}" class="btn btn-warning btn-sm">Show</a>
                            <a href="{{ route('ads.edit', $ad->id) }}" class="btn btn-info btn-sm">Edit</a>
                            {!! Form::open(['method' => 'DELETE','route' => ['ads.delete', $ad->id],'style'=>'display:inline']) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            @if(!isset($paginate))
                <div class="d-flex justify-content-center">
                    {!! $ads->links() !!}
                </div>
            @endif

        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            }
        });
    </script>
    <script>
        $('.ads').addClass("btnFocused");

        $(document).ready(function () {
            $('.blockedSwitch').click(function () {

                let blocked;
                var currentRow = $(this).closest("tr");
                var account_id = currentRow.find("th:eq(0)").text();

                console.log(account_id)

                if ($(this).prop("checked") == true) {
                    blocked = 1;

                } else if ($(this).prop("checked") == false) {
                    blocked = 0;

                }
                $.ajax({
                    url: "{{route('accounts.blocked')}}",
                    data: {
                        blocked: blocked,
                        account_id: account_id

                    },
                    type: "POST",
                    success: function (response) {
                        JSON.stringify(response);
                        console.log(response)
                        setTimeout(function () {
                            location.reload();
                        }, 200);
                    }
                });
            });
        });
    </script>
@endsection
