@extends('home.home')

@section('content')
    <div class="bg-light p-4 rounded" style="height: 700px !important;">

    <h1>Add New Ads</h1>
        <div class="container mt-4">


            <div class="row">
                <div class="col-4 d-flex justify-content-center">
                </div>


                <div class="col-4 d-flex justify-content-center">
                    <form method="POST" enctype="multipart/form-data" action="{{ route('ads.store') }}">
                        <h3 class="mb-4">Ad Information</h3>
                        @csrf
                        <div class="mb-3">
                            <label for="companyName" class="form-label">Company Name</label>
                            <input type="text" class="form-control" name="companyName" placeholder="Company Name" required style="width: 320px !important;">
                        </div>
                        <div class="mb-3">
                            <label for="link" class="form-label">Website Link</label>
                            <input type="text" class="form-control" name="link" placeholder="Website Link" style="width: 320px !important;">
                        </div>
                        <div class="mb-3">
                            <label for="description" class="form-label">Description</label>
                            <textarea type="text" class="form-control" name="description" placeholder="Description" required></textarea>
                        </div>
                        <div class="mb-3">
                            <label for="image" class="form-label">Ad Image</label>
                            <input type="file" class="form-control" name="image" placeholder="Select Image" required>
                        </div>

                        <input type="text" class="form-control" name="accountType" value="office" hidden>


                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-primary mb-3">Add New Ad</button>
                        </div>
                    </form>
                </div>


                <div class="col-4 d-flex justify-content-center">
                </div>
                <div class="d-flex justify-content-center mt-3">
                    <a href="{{ route('ads.index') }}" class="btn btn-primary">Back</a>
                </div>

            </div>

        </div>
@endsection

@section('scripts')
            <script>
                $('.ads').addClass("btnFocused");

            </script>
@endsection
