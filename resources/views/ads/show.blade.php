@extends('home.home')

@section('content')
    <div class="bg-light p-5 rounded" style="height: 900px !important;">
        <h1>Show Ad</h1>

        <div class="container mt-2">

            <h3 class="mb-4">Ad information</h3>
            <div class="mb-3">
                <span style="font-weight: bold;">Ad Image  :  </span> <br>
                <img src="{{$ad->img->path}}" alt="" width="415px" height="275px">
            </div>
            <div style="font-size: large;">
                <span style="font-weight: bold;">Company Name  :  </span>{{ $ad->companyName}}
            </div>
            <div style="font-size: large;">
                <span style="font-weight: bold;">Website Link :  </span><a href="{{ $ad->link }}">{{ $ad->link }}</a>
            </div>
            <div style="font-size: large;">
                <span style="font-weight: bold;">Description About Company  :  </span>{{ $ad->description }}
            </div>

            <div class="d-flex justify-content-center mt-5">
                <a href="{{ route('ads.index') }}" class="btn btn-primary">Back</a>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script>
        $('.ads').addClass("btnFocused");

    </script>
@endsection
