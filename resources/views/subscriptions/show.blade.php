@extends('home.home')

@section('content')
    <div class="bg-light p-5 rounded">
        <h1>Show Subscription</h1>

        <div class="container mt-4">
            <div style="font-size: large;">
                <span style="font-weight: bold;">Name  :  </span><span class="badge rounded-pill bg-primary">{{ $subscription->name}}</span>

            </div>
            <div style="font-size: large;">
                <span style="font-weight: bold;">Price :  </span><span class="badge rounded-pill bg-primary">{{ $subscription->price }}</span> SP
            </div>
            <div style="font-size: large;">
                <span style="font-weight: bold;">Period :  </span><span class="badge rounded-pill bg-primary">{{ $subscription->period }}</span> Days
            </div>
            <div style="font-size: large;">
                <span style="font-weight: bold;">Property Number  :  </span><span class="badge rounded-pill bg-primary">{{ $subscription->propertyNumber }}</span> Properties
            </div>
            <div class="d-flex justify-content-center mt-5">
                <a href="{{ route('subscriptions.index') }}" class="btn btn-primary">Back</a>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script>
        $('.subscriptions').addClass("btnFocused");
    </script>
@endsection
