@extends('home.home')

@section('content')
    <div class="bg-light p-4 rounded" style="height: 700px">
        <h1>Add New Subscription</h1>
        <div class="container mt-4">


            <div class="row">
                <div class="col-4 d-flex justify-content-center">
                </div>


                <div class="col-4 d-flex justify-content-center">
                    <form method="POST" action="{{ route('subscriptions.store') }}">
                        @csrf
                        <div class="mb-3">
                            <label for="name" class="form-label">Name</label>
                            <input type="text" class="form-control" name="name" placeholder="Name" required>
                        </div>
                        <div class="mb-3">
                            <label for="price" class="form-label">Price</label>
                            <input type="number" class="form-control" name="price" placeholder="Price" required>
                        </div>
                        <div class="mb-3">
                            <label for="period" class="form-label">Period Of Subscription</label>
                            <input type="number" class="form-control" name="period" placeholder="Period ( by days )" required>
                        </div>

                        <div class="mb-3">
                            <label for="propertyNumber" class="form-label">Property Number</label>
                            <input type="number" class="form-control" name="propertyNumber" placeholder="Property Number"
                                   required>
                        </div>

                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-primary mb-3">Create subscription</button>
                        </div>
                    </form>
                </div>


                <div class="col-4 d-flex justify-content-center">
                </div>
                <div class="d-flex justify-content-center mt-5">
                    <a href="{{ route('subscriptions.index') }}" class="btn btn-primary">Back</a>
                </div>

            </div>

        </div>
@endsection
        @section('scripts')
            <script>
                $('.subscriptions').addClass("btnFocused");

            </script>
@endsection
