@extends('home.home')

@section('content')

    <h1 class="mb- text-center">Subscription Management</h1>

    <div class="bg-light p-4 rounded">

        <div class="row justify-content-end">
            <div class="col-11">
                <h1>Subscriptions</h1>
            </div>
            <div class="col-1">
                <a href="{{ route('subscriptions.create') }}" class="btn btn-primary btn-sm float-right"
                   style="margin: 0px;">Add New Subscription</a>
            </div>
        </div>

        <div style=" height: 100% !important;
            width: auto !important;
            overflow: auto !important;">
            <table class="table table-striped" id="myTable" style="">
                <thead>
                <tr>
                    <th scope="col" width="1%">ID</th>
                    <th scope="col" width="10%">Name</th>
                    <th scope="col" width="10%">Price</th>
                    <th scope="col" width="10%">Property Number</th>
                    <th scope="col" width="10%">Period</th>
                    <th scope="col" width="10%" colspan="3">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($subscriptions as $subscription)
                    <tr>
                        <th scope="row">{{ $subscription->id }}</th>
                        <td> {{ $subscription->name}}</td>
                        <td><span class="badge rounded-pill bg-primary">{{ $subscription->price }}  SP</span></td>
                        <td><span class="badge rounded-pill bg-primary">{{ $subscription->propertyNumber}}  Properties</span></td>
                        <td><span class="badge rounded-pill bg-primary">{{ $subscription->period }}  Days</span></td>
                        <td>
                            <a href="{{ route('subscriptions.show', $subscription->id) }}" class="btn btn-warning btn-sm">Show</a>
                            <a href="{{ route('subscriptions.edit', $subscription->id) }}" class="btn btn-info btn-sm">Edit</a>
                            {!! Form::open(['method' => 'DELETE','route' => ['subscriptions.destroy', $subscription->id],'style'=>'display:inline']) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            @if(!isset($paginate))
                <div class="d-flex justify-content-center">
                    {!! $subscriptions->links() !!}
                </div>
            @endif

        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            }
        });
    </script>
    <script>
        $('.subscriptions').addClass("btnFocused");
    </script>
@endsection
