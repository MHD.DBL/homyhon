@extends('home.home')

@section('content')
    <div class="bg-light p-4 rounded" style=" height: 700px;">
        <h1>Update Subscription</h1>
        <div class="lead">

        </div>

        <div class="container mt-4">

            <div class="row">
                    <div class="col-4 d-flex justify-content-center">
                    </div>

                    <div class="col-4 d-flex justify-content-center">
                        <form method="POST" action="{{ route('subscriptions.update', $subscription->id) }}">
                            @csrf
                            <div class="mb-3">
                                <label for="name" class="form-label">Name</label>
                                <input type="text" class="form-control" name="name" placeholder="Name"
                                       value="{{ $subscription->name}}" required>
                            </div>
                            <div class="mb-3">
                                <label for="price" class="form-label">price</label>
                                <input type="number" class="form-control" name="price" placeholder="price"
                                       value="{{ $subscription->price}}" required>
                            </div>
                            <div class="mb-3">
                                <label for="period" class="form-label">Period ( By Days )</label>
                                <input type="number" class="form-control" name="period" placeholder="Period"
                                       value="{{ $subscription->period}}" required>
                            </div>

                            <div class="mb-3">
                                <label for="propertyNumber" class="form-label">Property Number</label>
                                <input type="number" class="form-control" name="propertyNumber" placeholder="Property Number"
                                       value="{{ $subscription->propertyNumber}}" required>
                            </div>

                            <div class="d-flex justify-content-center">
                                <button type="submit" class="btn btn-primary mb-3">Update subscription</button>
                            </div>
                        </form>
                    </div>

                    <div class="col-4 d-flex justify-content-center">
                    </div>
                <div class="d-flex justify-content-center mt-5">
                    <a href="{{ route('subscriptions.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('.subscriptions').addClass("btnFocused");
    </script>
@endsection
