@extends('home.home')

@section('content')

    <h1 class="mb- text-center">Contracts Management</h1>

    <div class="bg-light p-4 rounded">

        <div class="row justify-content-end">
            <div class="col-11">
                <h1>Contracts</h1>
            </div>
            <div class="col-1">

            </div>
        </div>

        <div style=" height: 100% !important;
            width: auto !important;
            overflow: auto !important;">
            <table class="table table-striped" id="myTable" style="">
                <thead>
                <tr>
                    <th scope="col" width="1%">ID</th>
                    <th scope="col" width="10%">Office Name</th>
                    <th scope="col" width="10%">Office Telephone</th>
                    <th scope="col" width="10%">Name First Party</th>
                    <th scope="col" width="10%">Phone Number FP</th>
                    <th scope="col" width="10%">Ratio</th>
                    <th scope="col" width="10%">Property Type</th>
                    <th scope="col" width="10%">Status</th>
                    <th scope="col" width="10%" colspan="3">Action</th>

                </tr>
                </thead>
                <tbody>
                @foreach($contracts as $contract)
                    <tr>
                        <th scope="row">{{ $contract->id }}</th>
                        <td>{{ $contract->office->officeName}}</td>
                        <td>0{{ $contract->office->telephone}}</td>
                        <td>{{ $contract->name_first_party }}</td>
                        <td>0{{ $contract->phone_number_FP }}</td>
                        <td> <span class="badge rounded-pill bg-success">{{ $contract->ratio }} %</span></td>
                        <td>{{ $contract->property->propertyable_type }}</td>
                        <td>
                            @if(!isset($contract->accept_refuse))
                                <span class="badge rounded-pill bg-warning">Pending</span>
                            @elseif($contract->accept_refuse == 0)
                                <span class="badge rounded-pill bg-danger">Refused</span>
                            @endif
                        </td>
                        <td>
                            <button class="btn btn-success btn-sm Accept">Accept</button>
                            <button class="btn btn-danger btn-sm Refuse">Refuse</button>
                            <a href="{{ route('ordersContract.showContract', $contract->id) }}" class="btn btn-warning btn-sm">Show</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            @if(!isset($paginate))
                <div class="d-flex justify-content-center">
                    {!! $contracts->links() !!}
                </div>
            @endif

        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            }
        });
    </script>
    <script>

        $('.Contracts').addClass("btnFocused");


        $(document).ready(function () {
            $('.Accept').click(function () {
                var currentRow = $(this).closest("tr");
                var contract_id = currentRow.find("th:eq(0)").text();
                $.ajax({
                    url: "{{route('ordersContract.acceptRefuse')}}",
                    data: {
                        accept_refuse: 1,
                        contract_id: contract_id
                    },
                    type: "POST",
                    success: function (response) {
                        JSON.stringify(response);
                        console.log(response)
                        setTimeout(function () {
                            location.reload();
                        }, 200);
                    }
                });
            });
            $('.Refuse').click(function () {
                var currentRow = $(this).closest("tr");
                var contract_id = currentRow.find("th:eq(0)").text();
                $.ajax({
                    url: "{{route('ordersContract.acceptRefuse')}}",
                    data: {
                        accept_refuse: 0,
                        contract_id: contract_id
                    },
                    type: "POST",
                    success: function (response) {
                        JSON.stringify(response);
                        console.log(response)
                        setTimeout(function () {
                            location.reload();
                        }, 200);
                    }
                });
            });
        });
    </script>
@endsection
