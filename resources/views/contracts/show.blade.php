@extends('home.home')

@section('content')
    <div class="bg-light p-4 rounded" style="  height: 100%;">
        <h1>Show Contract Details</h1>
        <div class="lead">
        </div>
        <div class="container mt-4">
            <div class="row">
                <div class="col-6">
                    <div class="d-flex justify-content-center">
                        <h3 class="mb-4">Office</h3>
                    </div>

                    <div style="font-size: large;">
                        <span
                            style="font-weight: bold;">Full Name  :  </span>{{ $contract->office->account->firstName}} {{$contract->office->account->lastName}}
                    </div>
                    <div style="font-size: large;">
                        <span style="font-weight: bold;">Email :  </span>{{ $contract->office->account->email }}
                    </div>
                    <div style="font-size: large;">
                        <span
                            style="font-weight: bold;">Phone Number  :  </span>0{{ $contract->office->account->phoneNumber }}
                    </div>
                    <div style="font-size: large;">
                        <span style="font-weight: bold;">Status  :  </span>@if($contract->office->account->blocked == 0)
                            <span class="badge rounded-pill bg-success">Active</span>
                        @else
                            <span class="badge rounded-pill bg-danger">InActive</span>
                        @endif
                    </div>
                    <div style="font-size: large;">
                        <span style="font-weight: bold;">Office Name  :  </span>{{ $contract->office->officeName}}
                    </div>
                    <div style="font-size: large;">
                        <span style="font-weight: bold;">Address :  </span>{{ $contract->office->address}}
                    </div>
                    
                    <div style="font-size: large;">
                    <span style="font-weight: bold;">Description  :  </span>{{ $contract->office->description }}
                    </div>
                    
                    <div style="font-size: large;">
                        <span style="font-weight: bold;">Telephone Number :  </span>0{{ $contract->office->telephone}}
                    </div>
                </div>


                <div class="col-6">
                    <div class="d-flex justify-content-center">
                        <h3 class="mb-4">Contract</h3>
                    </div>
                    <div style="font-size: large;">
                        <span style="font-weight: bold;">Name First Party  :  </span>{{ $contract->name_first_party}}
                    </div>
                    <div style="font-size: large;">
                        <span
                            style="font-weight: bold;">Phone Number First Party  :  </span>0{{ $contract->phone_number_FP }}
                    </div>
                    <div style="font-size: large;">
                        <span style="font-weight: bold;">Ratio  :  </span> <span class="badge rounded-pill bg-success">{{ $contract->ratio }} %</span>
                    </div>
                    <div style="font-size: large;">
                        <span style="font-weight: bold;">Status  :  </span> @if(!isset($contract->accept_refuse))
                            <span class="badge rounded-pill bg-warning">Pending</span>
                        @elseif($contract->accept_refuse == 0)
                            <span class="badge rounded-pill bg-danger">Refused</span>
                        @endif
                    </div>

                    <div class="d-flex justify-content-center mt-5">
                        <button class="btn btn-success btn-sm Accept mx-3">Accept</button>
                        <button class="btn btn-danger btn-sm Refuse">Refuse</button>
                    </div>

                </div>
            </div>
        </div>
        <div class="bg-light p-4 rounded" style="    height: 600px;">
            <div class="container mt-4">
                <div class="row">
                    <div class="d-flex justify-content-center">
                        <h3 class="mb-4">Property</h3>
                    </div>
                    <div class="col-12 d-flex justify-content-center">
                        <div class="container mt-2">
                            <div class="slideshow-container ">
                                <span style="font-weight: bold;">Property Images  :  </span> <br> <br>
                                @foreach($contract->property->imgs as $img)
                                    <div class="mySlides ">
                                        <div class="numbertext"></div>
                                        <img src="{{$img->path}}" style="width:415px" height="275px">
                                        <div class="text"></div>
                                    </div>
                                @endforeach
                                <a class="prev" onclick="plusSlides(-1)">❮</a>
                                <a class="next" onclick="plusSlides(1)">❯</a>
                                <br>
                            </div>
                            <br>

                            <div class="d-flex justify-content-center ">
                                <div class="col-3" style="font-size: large;">
                                    <span style="font-weight: bold;">Property Type  :  </span> <span
                                        class="badge rounded-pill bg-primary">{{ $contract->property->propertyable_type}}</span>

                                </div>
                                <div class="col-3" style="font-size: large;">
                                    <span style="font-weight: bold;">Owner Type :  </span><span
                                        class="badge rounded-pill bg-primary">{{ $contract->property->owner_type}}</span>
                                </div>
                                <div class="col-3" style="font-size: large;">
                                    <span style="font-weight: bold;">Property Description :  </span><span
                                        class="badge rounded-pill bg-primary">{{ $contract->property->description}}</span>
                                </div>
                                <div class="col-3" style="font-size: large;">
                                    <span style="font-weight: bold;">Property price :  </span><span
                                        class="badge rounded-pill bg-primary">{{ $contract->property->price}} SP</span>
                                </div>

                            </div>

                            <div class="d-flex justify-content-center mt-5 ">
                                <div class="col-3" style="font-size: large;">
                                    <span style="font-weight: bold;">Property space :  </span> <span
                                        class="badge rounded-pill bg-primary">{{ $contract->property->space}} M</span>

                                </div>
                                <div class="col-3" style="font-size: large;">
                                    <span style="font-weight: bold;">Property region :  </span> <span
                                        class="badge rounded-pill bg-primary">{{ $contract->property->region->name}}</span>

                                </div>
                                <div class="col-3" style="font-size: large;">
                    <span
                        style="font-weight: bold;">Property governorate :  </span> <span
                                        class="badge rounded-pill bg-primary">{{ $contract->property->region->governorate->name}}</span>

                                </div>
                                <div class="col-3" style="font-size: large;">
                    <span style="font-weight: bold;">Property Status :
                         @if($contract->property->rent_sale == 1)
                            <span class="badge rounded-pill bg-primary">For Rent</span>
                        @else
                            <span class="badge rounded-pill bg-dark">For Sell</span>
                                    @endif
                                </div>
                                <div class="col-3" style="font-size: large;"><span
                                        style="font-weight: bold;">Property has cladding  :  </span>
                                    @if($contract->property->has_cladding == 1)
                                        <span class="badge rounded-pill bg-primary">Yes</span>
                                    @else
                                        <span class="badge rounded-pill bg-danger">No</span>

                                    @endif
                                </div>

                            </div>
                        </div>
                    </div>


                </div>


            </div>
        </div>
        <div class="d-flex justify-content-center mt-5">
            <a href="{{ route('ordersContract.index') }}" class="btn btn-primary">Back</a>
            <input type="text" id="contract_id" value="{{$contract->id}}" hidden>
        </div>
        @endsection
        @section('scripts')
            <script>
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    }
                });
            </script>
            <script>
                $('.Contracts').addClass("btnFocused");


                $(document).ready(function () {
                    $('.Accept').click(function () {
                        var contract_id = $('#contract_id').val();
                        $.ajax({
                            url: "{{route('ordersContract.acceptRefuse')}}",
                            data: {
                                accept_refuse: 1,
                                contract_id: contract_id
                            },
                            type: "POST",
                            success: function (response) {
                                JSON.stringify(response);
                                console.log(response)
                                setTimeout(function () {
                                    window.location.assign('{{route("ordersContract.index")}}');
                                }, 100);
                            }
                        });
                    });
                    $('.Refuse').click(function () {
                        var contract_id = $('#contract_id').val();
                        $.ajax({
                            url: "{{route('ordersContract.acceptRefuse')}}",
                            data: {
                                accept_refuse: 0,
                                contract_id: contract_id
                            },
                            type: "POST",
                            success: function (response) {
                                JSON.stringify(response);
                                console.log(response)
                                setTimeout(function () {
                                    window.location.assign('{{route("ordersContract.index")}}');
                                }, 100);
                            }
                        });
                    });
                });

            </script>
@endsection
