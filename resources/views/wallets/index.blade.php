@extends('home.home')

@section('content')

    <h1 class="mb- text-center">Wallets Management</h1>

    <div class="bg-light p-4 rounded">

        <div class="row justify-content-end">
            <div class="col-11">
                <h1>Wallets</h1>
            </div>
            <div class="col-1">

            </div>
        </div>

        <div style=" height: 100% !important;
            width: auto !important;
            overflow: auto !important;">
            <table class="table table-striped" id="myTable" style="">
                <thead>
                <tr>
                    <th scope="col"
                        width="1%"
                    >ID
                    </th>
                    <th scope="col"
                        width="10%"
                    >Full Name
                    </th>
                    <th scope="col" width="10%">Email</th>
                    <th scope="col"
                        width="10%"
                    >Phone Number
                    </th>
                    <th scope="col"
                        width="10%"
                    >Value
                    </th>
                    <th scope="col"
                        width="10%">Cash Transfer
                    </th>
                    <th scope="col"
                        width="10%"
                        colspan="3">Action
                    </th>

                </tr>
                </thead>
                <tbody>
                @foreach($wallets as $wallet)
                    <tr>
                        <th scope="row">{{ $wallet->id }}</th>
                        <td>{{ $wallet->user->account->firstName}} {{$wallet->user->account->lastName}}</td>
                        <td>{{ $wallet->user->account->email }}</td>
                        <td>0{{ $wallet->user->account->phoneNumber }}</td>
                        <td><span class="badge rounded-pill bg-dark"
                                  style="font-size: 16px">{{ $wallet->amount }}</span></td>
                        <td>
                            <div class="d-flex justify-content-center">
                                <input type="number" class="form-control amount">
                            </div>
                        </td>
                        <td>
                            <div class="d-flex justify-content-center">
                                <button class="btn btn-primary btn-sm charge">Charge</button>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            @if(!isset($paginate))
                <div class="d-flex justify-content-center">
                    {!! $wallets->links() !!}
                </div>
            @endif

        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            }
        });
    </script>
    <script>

        $('.wallets').addClass("btnFocused");


        $(document).ready(function () {
            $('.charge').click(function () {
                var currentRow = $(this).closest("tr");
                var wallet_id = currentRow.find("th:eq(0)").text();
                var amount = currentRow.find(".amount").val();
                console.log(amount)

                $.ajax({
                    url: "{{route('wallets.chargeWallet')}}",
                    data: {
                        amount: amount,
                        wallet_id: wallet_id
                    },
                    type: "POST",
                    success: function (response) {
                        JSON.stringify(response);
                        console.log(response)
                        setTimeout(function () {
                            location.reload();
                        }, 200);
                    }
                });
            });
        });
    </script>
@endsection
