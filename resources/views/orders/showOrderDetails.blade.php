@extends('home.home')

@section('content')
    <style>
        .bg-primary{
            font-weight: bold;
            font-size: 15px;
        }
    </style>

    <div class="bg-light p-5 rounded" style="height: 1500px !important;">
        <h1>Show Property</h1>

        <div class="container mt-2">

            <h3 class="mb-3">Property Details</h3>

            <div class="slideshow-container ">
                <span style="font-weight: bold;">Property Images  :  </span> <br> <br>
                @foreach($property->imgs as $img)
                    <div class="mySlides ">
                        <div class="numbertext"></div>
                        <img src="{{$img->path}}" style="width:415px" height="275px">
                        <div class="text"></div>
                    </div>
                @endforeach
                <a class="prev" onclick="plusSlides(-1)">❮</a>
                <a class="next" onclick="plusSlides(1)">❯</a>
                <br>
            </div>
            <br>

            <div class="d-flex justify-content-center ">
                <div class="col-3" style="font-size: large;">
                    <span style="font-weight: bold;">Property Type  :  </span> <span
                        class="badge rounded-pill bg-primary">{{ $property->propertyable_type}}</span>

                </div>
                <div class="col-3" style="font-size: large;">
                    <span style="font-weight: bold;">Owner Type :  </span><span
                        class="badge rounded-pill bg-primary">{{ $property->owner_type}}</span>
                </div>
                <div class="col-3" style="font-size: large;">
                    <span style="font-weight: bold;">Property Description :  </span><span
                        class="badge rounded-pill bg-primary">{{ $property->description}}</span>
                </div>
                <div class="col-3" style="font-size: large;">
                    <span style="font-weight: bold;">Property price :  </span><span
                        class="badge rounded-pill bg-primary">{{ $property->price}} SP</span>
                </div>

            </div>

            <div class="d-flex justify-content-center mt-5 ">
                <div class="col-3" style="font-size: large;">
                    <span style="font-weight: bold;">Property space :  </span> <span
                        class="badge rounded-pill bg-primary">{{ $property->space}} M</span>

                </div>
                <div class="col-3" style="font-size: large;">
                    <span style="font-weight: bold;">Property region :  </span> <span
                        class="badge rounded-pill bg-primary">{{ $property->region->name}}</span>

                </div>
                <div class="col-3" style="font-size: large;">
                    <span
                        style="font-weight: bold;">Property governorate :  </span> <span
                        class="badge rounded-pill bg-primary">{{ $property->region->governorate->name}}</span>

                </div>
                <div class="col-3" style="font-size: large;">
                    <span style="font-weight: bold;">Property Status :
                         @if($property->rent_sale == 1)
                            <span class="badge rounded-pill bg-primary">For Rent</span>
                        @else
                            <span class="badge rounded-pill bg-dark">For Sell</span>
                    @endif
                </div>
                <div class="col-3" style="font-size: large;"><span
                        style="font-weight: bold;">Property has cladding  :  </span>
                    @if($property->has_cladding == 1)
                        <span class="badge rounded-pill bg-primary">Yes</span>
                    @else
                        <span class="badge rounded-pill bg-danger">No</span>

                    @endif
                </div>

            </div>

            <div class="d-flex justify-content-center mt-5 ">
                <button class="btn btn-success btn-sm Accept mx-2">Accept</button>
                <button class="btn btn-danger btn-sm Refuse">Refuse</button>
            </div>


            <div class="d-flex justify-content-center mt-5">
                <a href="{{ route('orders.index') }}" class="btn btn-primary">Back</a>
            </div>

            <input type="text" class="property_id" value="{{$property->id}}" hidden>
        </div>
    </div>

@endsection
@section('scripts')
    <script>
        $('.property').addClass("btnFocused");
        $(document).ready(function () {
            $('.Accept').click(function () {
                var property_id = $('.property_id').val();
                $.ajax({
                    url: "{{route('orders.acceptRefuse')}}",
                    data: {
                        accept_refuse: 1,
                        property_id: property_id
                    },
                    type: "POST",
                    success: function (response) {
                        JSON.stringify(response);
                        console.log(response)
                        setTimeout(function() {
                            window.location.assign('{{route("orders.index")}}');
                        }, 200);
                    }
                });
            });
            $('.Refuse').click(function () {
                var property_id =$('.property_id').val();
                $.ajax({
                    url: "{{route('orders.acceptRefuse')}}",
                    data: {
                        accept_refuse: 0,
                        property_id: property_id
                    },
                    type: "POST",
                    success: function (response) {
                        JSON.stringify(response);
                        console.log(response)
                        setTimeout(function() {
                            window.location.assign('{{route("orders.index")}}');
                        }, 200);
                    }
                });
            });
        });
    </script>
@endsection
