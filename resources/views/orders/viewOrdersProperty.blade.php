@extends('home.home')

@section('content')

    <h1 class="mb- text-center">Orders Management</h1>

    <div class="bg-light p-4 rounded" style="height: 700px !important;">

        <div class="row justify-content-end mb-4">
            <div class="col-11">
                <h1>Property Orders</h1>
            </div>
            <div class="col-1">

            </div>
        </div>



        <div style=" height: 100% !important;
            width: auto !important;
            overflow: auto !important;">
            <table class="table table-striped" id="myTable" style="">
                <thead>
                <tr>
                    <th scope="col" width="1%">ID</th>
                    <th scope="col" width="10%">Property Type</th>
                    <th scope="col" width="10%">Property Owner</th>
                    <th scope="col" width="10%">Property Status</th>
                    <th scope="col" width="10%">Rent Or Sell</th>
                    <th scope="col" width="10%">Price</th>
                    <th scope="col" width="10%">Region</th>
                    <th scope="col" width="10%">Governorate</th>
                    <th scope="col" width="10%">address</th>
                    <th scope="col" width="15%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($properties as $property)
                    <tr>
                        <th scope="row">{{ $property->id }}</th>
                        <td>{{ $property->propertyable_type}}</td>
                        <td>{{ $property->owner_type}}</td>
                        <td>
                            @if(!isset($property->accept_refuse))
                                <span class="badge rounded-pill bg-warning">Pending</span>
                            @elseif($property->accept_refuse == 0)
                                <span class="badge rounded-pill bg-danger">Refused</span>
                            @endif
                        </td>
                        <td>
                            @if($property->rent_sale == 1)
                                <span class="badge rounded-pill bg-primary">For Rent</span>
                            @else
                                <span class="badge rounded-pill bg-dark">For Sell</span>
                            @endif
                        </td>
                        <td>{{ $property->price }} SP</td>
                        <td>{{ $property->region->name }}</td>
                        <td>{{ $property->region->governorate->name }}</td>
                        <td>{{ $property->address }}</td>

                        <td>
                            <button class="btn btn-success btn-sm Accept">Accept</button>
                            <button class="btn btn-danger btn-sm Refuse">Refuse</button>
                            <a href="{{ route('orders.showProperty', $property->id) }}" class="btn btn-warning btn-sm">Show</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            @if(!isset($paginate))
                <div class="d-flex justify-content-center">
                    {!! $properties->links() !!}
                </div>
            @endif

        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            }
        });
    </script>
    <script>
        $('.property').addClass("btnFocused");

        $(document).ready(function () {
            $('.Accept').click(function () {
                var currentRow = $(this).closest("tr");
                var property_id = currentRow.find("th:eq(0)").text();
                $.ajax({
                    url: "{{route('orders.acceptRefuse')}}",
                    data: {
                        accept_refuse: 1,
                        property_id: property_id
                    },
                    type: "POST",
                    success: function (response) {
                        JSON.stringify(response);
                        console.log(response)
                        setTimeout(function () {
                            location.reload();
                        }, 200);
                    }
                });
            });
            $('.Refuse').click(function () {
                var currentRow = $(this).closest("tr");
                var property_id = currentRow.find("th:eq(0)").text();
                $.ajax({
                    url: "{{route('orders.acceptRefuse')}}",
                    data: {
                        accept_refuse: 0,
                        property_id: property_id
                    },
                    type: "POST",
                    success: function (response) {
                        JSON.stringify(response);
                        console.log(response)
                        setTimeout(function () {
                            location.reload();
                        }, 200);
                    }
                });
            });
        });
    </script>
@endsection
