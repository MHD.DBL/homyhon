@extends('home.home')

@section('content')
    <div class="bg-light p-4 rounded" style="height: 120%">
        <h1>Add New Account</h1>
        <div class="container mt-4">


            <div class="row">
                <div class="col-4 d-flex justify-content-center">
                    <form method="POST" enctype="multipart/form-data" action="{{ route('accounts.store') }}">
                        <h3 class="mb-4">User Account</h3>
                        @csrf
                        <div class="mb-3">
                            <label for="firstName" class="form-label">First Name</label>
                            <input type="text" class="form-control" name="firstName" placeholder="First Name" required>
                        </div>
                        <div class="mb-3">
                            <label for="lastName" class="form-label">Last Name</label>
                            <input type="text" class="form-control" name="lastName" placeholder="Last Name" required>
                        </div>
                        <div class="mb-3">
                            <label for="password" class="form-label">Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Email</label>
                            <input type="text" class="form-control" name="email" placeholder="Email" required>
                            @if ($errors->has('email'))
                                <span class="text-danger text-left">{{ $errors->first('email') }}</span>
                            @endif
                        </div>

                        <div class="mb-3">
                            <label for="phoneNumber" class="form-label">Phone Number</label>
                            <input type="text" class="form-control" name="phoneNumber" placeholder="Phone Number"
                                   required>
                        </div>

                        <div class="mb-3">
                            <label for="gender" class="form-label">Gender</label>
                            <select class="form-control" name="gender" style="width: 209px !important;">
                                <option>Choose</option>
                                <option value="male">male</option>
                                <option value="female">female</option>
                            </select>
                        </div>

                        <div class="mb-3">
                            <label for="birthdate" class="form-label">Birthdate</label>
                            <input type="date" class="form-control" name="birthdate" placeholder="Birthdate"
                                   style="width: 209px !important;">
                        </div>

                        <div class="mb-3">
                            <label for="image" class="form-label">Profile Image</label>
                            <input type="file" class="form-control" name="image" placeholder="Select Image" style="width: 209px !important;">
                        </div>

                        <input type="text" class="form-control" name="accountType" value="user" hidden>

                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-primary ">Create User</button>
                        </div>
                    </form>
                </div>


                <div class="col-4 d-flex justify-content-center">
                    <form method="POST" enctype="multipart/form-data" action="{{ route('accounts.store') }}">
                        <h3 class="mb-4">Office Account</h3>
                        @csrf
                        <div class="mb-3">
                            <label for="firstName" class="form-label">First Name</label>
                            <input type="text" class="form-control" name="firstName" placeholder="First Name" required>
                        </div>
                        <div class="mb-3">
                            <label for="lastName" class="form-label">Last Name</label>
                            <input type="text" class="form-control" name="lastName" placeholder="Last Name" required>
                        </div>
                        <div class="mb-3">
                            <label for="password" class="form-label">Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Email</label>
                            <input type="text" class="form-control" name="email" placeholder="Email" required>
                        </div>

                        <div class="mb-3">
                            <label for="phoneNumber" class="form-label">Phone Number</label>
                            <input type="text" class="form-control" name="phoneNumber" placeholder="Phone Number"
                                   required>
                        </div>

                        <div class="mb-3">
                            <label for="officeName" class="form-label">Office Name</label>
                            <input type="text" class="form-control" name="officeName" placeholder="Office Name"
                                   required>
                        </div>

                        <div class="mb-3">
                            <label for="address" class="form-label">Address</label>
                            <textarea class="form-control" name="address" placeholder="Your Address" required style="width: 209px !important;"> </textarea>
                        </div>
                        
                        <div class="mb-3">
                            <label for="description" class="form-label">Description</label>
                            <textarea class="form-control" name="description" style="width: 209px !important;"> </textarea>
                        </div>

                        <div class="mb-3">
                            <label for="telephone" class="form-label">Telephone Number</label>
                            <input type="text" class="form-control" name="telephone" placeholder="Telephone Number"
                                   required>
                        </div>

                        <div class="mb-3">
                            <label for="phoneNumber" class="form-label">Region</label>
                            <select type="text" class="form-control" name="region_id" required style="width: 209px !important;">
                                <option selected>Choose</option>
                            @foreach($regions as $region)
                                    <option value="{{$region->id}}">{{$region->name}}</option>
                            @endforeach
                            </select>

                        </div>

                        <div class="mb-3">
                            <label for="image" class="form-label">Profile Image</label>
                            <input type="file" class="form-control" name="image"
                                   style="width: 209px !important;">
                        </div>

                        <input type="text" class="form-control" name="accountType" value="office" hidden>


                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-primary mb-3">Create Office</button>
                        </div>
                    </form>
                </div>


                <div class="col-4 d-flex justify-content-center">
                    <form method="POST" enctype="multipart/form-data" action="{{ route('accounts.store') }}">
                        <h3 class="mb-4">Admin Account</h3>
                        @csrf
                        <div class="mb-3">
                            <label for="firstName" class="form-label">First Name</label>
                            <input type="text" class="form-control" name="firstName" placeholder="First Name" required>
                        </div>
                        <div class="mb-3">
                            <label for="lastName" class="form-label">Last Name</label>
                            <input type="text" class="form-control" name="lastName" placeholder="Last Name" required>
                        </div>
                        <div class="mb-3">
                            <label for="password" class="form-label">Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">Email</label>
                            <input type="text" class="form-control" name="email" placeholder="Email" required>
                        </div>

                        <div class="mb-3">
                            <label for="phoneNumber" class="form-label">Phone Number</label>
                            <input type="text" class="form-control" name="phoneNumber" placeholder="Phone Number"
                                   required>
                        </div>

                        <div class="mb-3">
                            <label for="role" class="form-label">Role</label>
                            <input type="text" class="form-control" name="role" placeholder="Role" required>
                        </div>

                        <div class="mb-3">
                            <label for="image" class="form-label">Profile Image</label>
                            <input type="file" class="form-control" name="image"
                                   style="width: 209px !important;">
                        </div>

                        <input type="text" class="form-control" name="accountType" value="admin" hidden>

                        <div class="d-flex justify-content-center">
                            <button type="submit" class="btn btn-primary ">Create Admin</button>
                        </div>
                    </form>
                </div>
                <div class="d-flex justify-content-center mt-5">
                    <a href="{{ route('accounts.index') }}" class="btn btn-primary">Back</a>
                </div>

            </div>

        </div>
@endsection
        @section('scripts')
            <script>
                $('.accounts').addClass("btnFocused");

            </script>
@endsection
