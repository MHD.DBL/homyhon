@extends('home.home')

@section('content')
    <div class="bg-light p-4 rounded" style="    height: 140%;">
        <h1>Update Account</h1>
        <div class="lead">

        </div>

        <div class="container mt-4">

            <div class="row">
                @if(isset($account->user))
                    <div class="col-4 d-flex justify-content-center">
                        <form method="POST" enctype="multipart/form-data" action="{{ route('accounts.update')}}">
                            <h3 class="mb-4">User Account</h3>
                          
                            @csrf

                            <div class="mb-3">
                                <label for="image" class="form-label">Profile Image</label>
                                @if(isset($account->img->path))
                                <img src="{{$account->img->path}}" alt="" width="415px" height="275">
                                @endif
                                <input type="file" class="form-control mt-2" name="image"
                                       style="width: 209px !important;">
                            </div>

                            <div class="mb-3">
                                <label for="firstName" class="form-label">First Name</label>
                                <input type="text" class="form-control" name="firstName" placeholder="First Name"
                                       value="{{ $account->firstName}}" required>
                            </div>
                            <div class="mb-3">
                                <label for="lastName" class="form-label">Last Name</label>
                                <input type="text" class="form-control" name="lastName" placeholder="Last Name"
                                       value="{{ $account->lastName}}" required>
                            </div>
                            <div class="mb-3">
                                <label for="password" class="form-label">Change Password</label>
                                <br>
                                <label class="switch mb-2">
                                    <input type="checkbox" class="blockedSwitch">
                                    <div class="slider">
                                        <div class="circle">
                                            <svg class="cross" xml:space="preserve"
                                                 style="enable-background:new 0 0 512 512" viewBox="0 0 365.696 365.696"
                                                 y="0" x="0" height="6" width="6"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 version="1.1" xmlns="http://www.w3.org/2000/svg">
                <g>
                    <path data-original="#000000" fill="currentColor"
                          d="M243.188 182.86 356.32 69.726c12.5-12.5 12.5-32.766 0-45.247L341.238 9.398c-12.504-12.503-32.77-12.503-45.25 0L182.86 122.528 69.727 9.374c-12.5-12.5-32.766-12.5-45.247 0L9.375 24.457c-12.5 12.504-12.5 32.77 0 45.25l113.152 113.152L9.398 295.99c-12.503 12.503-12.503 32.769 0 45.25L24.48 356.32c12.5 12.5 32.766 12.5 45.247 0l113.132-113.132L295.99 356.32c12.503 12.5 32.769 12.5 45.25 0l15.081-15.082c12.5-12.504 12.5-32.77 0-45.25zm0 0"></path>
                </g>
            </svg>
                                            <svg class="checkmark" xml:space="preserve"
                                                 style="enable-background:new 0 0 512 512" viewBox="0 0 24 24" y="0"
                                                 x="0"
                                                 height="10" width="10" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 version="1.1" xmlns="http://www.w3.org/2000/svg">
                <g>
                    <path class="" data-original="#000000" fill="currentColor"
                          d="M9.707 19.121a.997.997 0 0 1-1.414 0l-5.646-5.647a1.5 1.5 0 0 1 0-2.121l.707-.707a1.5 1.5 0 0 1 2.121 0L9 14.171l9.525-9.525a1.5 1.5 0 0 1 2.121 0l.707.707a1.5 1.5 0 0 1 0 2.121z"></path>
                </g>
            </svg>
                                        </div>
                                    </div>
                                </label>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Enter New Password" hidden disabled>
                            </div>
                            <div class="mb-3">
                                <label for="email" class="form-label">Email</label>
                                <input type="text" class="form-control" name="email" placeholder="Email"
                                       value="{{ $account->email}}" required>
                            </div>

                            <div class="mb-3">
                                <label for="phoneNumber" class="form-label">Phone Number</label>
                                <input type="text" class="form-control" name="phoneNumber" placeholder="Phone Number"
                                       value="0{{ $account->phoneNumber}}"
                                       required>
                            </div>

                            <div class="mb-3">
                                <label for="gender" class="form-label">Gender</label>
                                <select class="form-control" name="gender"  style="width: 209px !important;">
                                    <option>{{ $account->user->gender }}</option>
                                    <option value="male">male</option>
                                    <option value="female">female</option>
                                </select>
                            </div>

                            <div class="mb-3">
                                <label for="birthdate" class="form-label">Birthdate</label>
                                <input type="date" class="form-control" name="birthdate" placeholder="Birthdate"
                                       style="width: 209px !important;" value="{{ $account->user->birthdate}}">
                            </div>

                            <input type="text" class="form-control" name="accountType" value="user" hidden>
                            <input type="text" class="form-control" name="account_id" value="{{$account->id}}" hidden>


                            <div class="d-flex justify-content-center">
                                <button type="submit" class="btn btn-primary ">Update User</button>
                            </div>
                        </form>
                    </div>
                @endif

                @if(isset($account->office))
                    <div class="col-4 d-flex justify-content-center">
                        <form method="POST" enctype="multipart/form-data" action="{{ route('accounts.update') }}">
                            <h3 class="mb-4">Office Account</h3>
                         
                            @csrf

                            <div class="mb-3">
                                <label for="image" class="form-label">Profile Image</label>
                                @if(isset($account->img->path))

                                <img src="{{$account->img->path}}" alt="" width="415px" height="275">
                                @endif
                                <input type="file" class="form-control mt-2" name="image"
                                       style="width: 209px !important;">
                            </div>

                            <div class="mb-3">
                                <label for="firstName" class="form-label">First Name</label>
                                <input type="text" class="form-control" name="firstName" placeholder="First Name"
                                       value="{{ $account->firstName}}" required>
                            </div>
                            <div class="mb-3">
                                <label for="lastName" class="form-label">Last Name</label>
                                <input type="text" class="form-control" name="lastName" placeholder="Last Name"
                                       value="{{ $account->lastName}}" required>
                            </div>
                            <div class="mb-3">
                                <label for="password" class="form-label">Change Password</label>
                                <br>
                                <label class="switch mb-2">
                                    <input type="checkbox" class="blockedSwitch">
                                    <div class="slider">
                                        <div class="circle">
                                            <svg class="cross" xml:space="preserve"
                                                 style="enable-background:new 0 0 512 512" viewBox="0 0 365.696 365.696"
                                                 y="0" x="0" height="6" width="6"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 version="1.1" xmlns="http://www.w3.org/2000/svg">
                <g>
                    <path data-original="#000000" fill="currentColor"
                          d="M243.188 182.86 356.32 69.726c12.5-12.5 12.5-32.766 0-45.247L341.238 9.398c-12.504-12.503-32.77-12.503-45.25 0L182.86 122.528 69.727 9.374c-12.5-12.5-32.766-12.5-45.247 0L9.375 24.457c-12.5 12.504-12.5 32.77 0 45.25l113.152 113.152L9.398 295.99c-12.503 12.503-12.503 32.769 0 45.25L24.48 356.32c12.5 12.5 32.766 12.5 45.247 0l113.132-113.132L295.99 356.32c12.503 12.5 32.769 12.5 45.25 0l15.081-15.082c12.5-12.504 12.5-32.77 0-45.25zm0 0"></path>
                </g>
            </svg>
                                            <svg class="checkmark" xml:space="preserve"
                                                 style="enable-background:new 0 0 512 512" viewBox="0 0 24 24" y="0"
                                                 x="0"
                                                 height="10" width="10" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 version="1.1" xmlns="http://www.w3.org/2000/svg">
                <g>
                    <path class="" data-original="#000000" fill="currentColor"
                          d="M9.707 19.121a.997.997 0 0 1-1.414 0l-5.646-5.647a1.5 1.5 0 0 1 0-2.121l.707-.707a1.5 1.5 0 0 1 2.121 0L9 14.171l9.525-9.525a1.5 1.5 0 0 1 2.121 0l.707.707a1.5 1.5 0 0 1 0 2.121z"></path>
                </g>
            </svg>
                                        </div>
                                    </div>
                                </label>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Enter New Password" hidden disabled>
                            </div>
                            <div class="mb-3">
                                <label for="email" class="form-label">Email</label>
                                <input type="text" class="form-control" name="email" placeholder="Email"
                                       value="{{ $account->email}}" required>
                            </div>

                            <div class="mb-3">
                                <label for="phoneNumber" class="form-label">Phone Number</label>
                                <input type="text" class="form-control" name="phoneNumber" placeholder="Phone Number"
                                       value="0{{ $account->phoneNumber}}" required>
                            </div>

                            <div class="mb-3">
                                <label for="officeName" class="form-label">Office Name</label>
                                <input type="text" class="form-control" name="officeName" placeholder="Office Name"
                                       value="{{ $account->office->officeName}}" required>
                            </div>

                            <div class="mb-3">
                                <label for="address" class="form-label">Address</label>
                                <textarea class="form-control" name="address" placeholder="Your Address"
                                          required  style="width: 209px !important;">{{ $account->office->address}}</textarea>
                            </div>
                            
                            <div class="mb-3">
                                <label for="description" class="form-label">Description</label>
                                <textarea class="form-control" name="description" 
                                          required  style="width: 209px !important;">{{ $account->office->description}}</textarea>
                            </div>

                            <div class="mb-3">
                                <label for="telephone" class="form-label">Telephone Number</label>
                                <input type="text" class="form-control" name="telephone" placeholder="Telephone Number"
                                       value="0{{ $account->office->telephone}}" required>
                            </div>

                            <div class="mb-3">
                                <label for="phoneNumber" class="form-label">Region</label>
                                    <select type="text" class="form-control" name="region_id" required  style="width: 209px !important;">
                                        <option selected value="{{$account->office->region->id}}">{{$account->office->region->name}}</option>
                                        @foreach($regions as $region)
                                        <option value="{{$region->id}}">{{$region->name}}</option>
                                        @endforeach

                                    </select>
                            </div>

                            <input type="text" class="form-control" name="accountType" value="office" hidden >
                             <input type="text" class="form-control" name="account_id" value="{{$account->id}}" hidden>


                            <div class="d-flex justify-content-center">
                                <button type="submit" class="btn btn-primary mb-3">Update Office</button>
                            </div>
                        </form>
                    </div>
                @endif

                @if(isset($account->admin))

                    <div class="col-4 d-flex justify-content-center">
                        <form method="POST" enctype="multipart/form-data" action="{{ route('accounts.update')
                       
                        }}">
                            <h3 class="mb-4">Admin Account</h3>
                        
                            @csrf

                            <div class="mb-3">
                                <label for="image" class="form-label">Profile Image</label>
                                @if(isset($account->img->path))

                                <img src="{{$account->img->path}}" alt="" width="415px" height="275">
                                @endif
                                <input type="file" class="form-control mt-2" name="image"
                                       style="width: 209px !important;">
                            </div>

                            <div class="mb-3">
                                <label for="firstName" class="form-label">First Name</label>
                                <input type="text" class="form-control" name="firstName" placeholder="First Name"
                                       value="{{ $account->firstName}}" required>
                            </div>
                            <div class="mb-3">
                                <label for="lastName" class="form-label">Last Name</label>
                                <input type="text" class="form-control" name="lastName" placeholder="Last Name"
                                       value="{{ $account->lastName}}" required>
                            </div>
                            <div class="mb-3">
                                <label for="password" class="form-label">Change Password</label>
                                <br>
                                <label class="switch mb-2">
                                    <input type="checkbox" class="blockedSwitch">
                                    <div class="slider">
                                        <div class="circle">
                                            <svg class="cross" xml:space="preserve"
                                                 style="enable-background:new 0 0 512 512" viewBox="0 0 365.696 365.696"
                                                 y="0" x="0" height="6" width="6"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 version="1.1" xmlns="http://www.w3.org/2000/svg">
                <g>
                    <path data-original="#000000" fill="currentColor"
                          d="M243.188 182.86 356.32 69.726c12.5-12.5 12.5-32.766 0-45.247L341.238 9.398c-12.504-12.503-32.77-12.503-45.25 0L182.86 122.528 69.727 9.374c-12.5-12.5-32.766-12.5-45.247 0L9.375 24.457c-12.5 12.504-12.5 32.77 0 45.25l113.152 113.152L9.398 295.99c-12.503 12.503-12.503 32.769 0 45.25L24.48 356.32c12.5 12.5 32.766 12.5 45.247 0l113.132-113.132L295.99 356.32c12.503 12.5 32.769 12.5 45.25 0l15.081-15.082c12.5-12.504 12.5-32.77 0-45.25zm0 0"></path>
                </g>
            </svg>
                                            <svg class="checkmark" xml:space="preserve"
                                                 style="enable-background:new 0 0 512 512" viewBox="0 0 24 24" y="0"
                                                 x="0"
                                                 height="10" width="10" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 version="1.1" xmlns="http://www.w3.org/2000/svg">
                <g>
                    <path class="" data-original="#000000" fill="currentColor"
                          d="M9.707 19.121a.997.997 0 0 1-1.414 0l-5.646-5.647a1.5 1.5 0 0 1 0-2.121l.707-.707a1.5 1.5 0 0 1 2.121 0L9 14.171l9.525-9.525a1.5 1.5 0 0 1 2.121 0l.707.707a1.5 1.5 0 0 1 0 2.121z"></path>
                </g>
            </svg>
                                        </div>
                                    </div>
                                </label>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Enter New Password" hidden disabled>
                            </div>
                            <div class="mb-3">
                                <label for="email" class="form-label">Email</label>
                                <input type="text" class="form-control" name="email" placeholder="Email"
                                       value="{{ $account->email}}" required>
                            </div>

                            <div class="mb-3">
                                <label for="phoneNumber" class="form-label">Phone Number</label>
                                <input type="text" class="form-control" name="phoneNumber" placeholder="Phone Number"
                                       value="0{{ $account->phoneNumber}}"
                                       required>
                            </div>

                            <div class="mb-3">
                                <label for="role" class="form-label">Role</label>
                                <input type="text" class="form-control" name="role" placeholder="Role"
                                       value="{{ $account->admin->role}}" required>
                            </div>

                            <input type="text" class="form-control" name="accountType" value="admin" hidden >
                             <input type="text" class="form-control" name="account_id" value="{{$account->id}}" hidden>

                            <div class="d-flex justify-content-center">
                                <button type="submit" class="btn btn-primary ">Update Admin</button>
                            </div>
                        </form>
                    </div>
                @endif

                <div class="d-flex justify-content-center mt-5">
                    <a href="{{ route('accounts.index') }}" class="btn btn-primary">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $('.accounts').addClass("btnFocused");
        $('.blockedSwitch').click(function () {

            let blocked;
            var currentRow = $(this).closest("tr");
            var account_id = currentRow.find("th:eq(0)").text();

            console.log(account_id)

            if ($(this).prop("checked") == true) {
                $('#password').attr('hidden',false);
                $('#password').attr('disabled',false);
            } else if ($(this).prop("checked") == false) {
                $('#password').attr('hidden',true);
                $('#password').attr('disabled',true);

            }
        });

    </script>
@endsection
