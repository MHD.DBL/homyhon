@extends('home.home')

@section('content')

    <h1 class="mb- text-center">Users Management</h1>

    <div class="bg-light p-4 rounded" style="height: 700px !important;">

        <div class="row justify-content-end">
            <div class="col-11">
                <h1>Users</h1>
            </div>
            <div class="col-1">
                <a href="{{ route('accounts.create') }}" class="btn btn-primary btn-sm float-right"
                   style="margin: 0px;">Add new user</a>
            </div>
        </div>

        <div style=" height: 100% !important;
            width: auto !important;
            overflow: auto !important;">
            <table class="table table-striped" id="myTable" style="">
                <thead>
                <tr>
                    <th scope="col"
                        width="1%"
                    >ID
                    </th>
                    <th scope="col"
                        width="10%"
                    >Profile Image
                    <th scope="col"
                        width="10%"
                    >Full Name

                    </th>
                    <th scope="col" width="10%">Email</th>
                    <th scope="col"
                        width="10%"
                    >Phone Number
                    </th>
                    <th scope="col"
                        width="10%"
                    >Type
                    </th>
                    <th scope="col"
                        width="10%">Status
                    </th>
                    <th scope="col"
                        width="10%">Blocked
                    </th>
                    <th scope="col"
                        width="10%"
                        colspan="3">Action
                    </th>

                </tr>
                </thead>
                <tbody>
                @foreach($accounts as $account)
                    <tr>
                        <th scope="row">{{ $account->id }}</th>
                        <td>
                            @if(isset($account->img->path))
                                <div class="d-flex justify-content-center">
                                    <a data-fslightbox="" href="{{$account->img->path}}" target="_blank"><img src="{{$account->img->path}}" class="img-thumbnail" style="width:75px; height: 75px; border-radius: 70px;    border: solid;"></a>
                                </div>
                            @else
                                <div class="d-flex justify-content-center">
                                    <svg width="24" height="23" viewBox="0 0 24 23" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M6.66667 7.66676C6.66667 6.31123 7.22857 5.0112 8.22876 4.05268C9.22896 3.09417 10.5855 2.55568 12 2.55568C13.4145 2.55568 14.7711 3.09417 15.7712 4.05268C16.7715 5.0112 17.3333 6.31123 17.3333 7.66676C17.3333 9.02234 16.7715 10.3223 15.7712 11.2808C14.7711 12.2394 13.4145 12.7778 12 12.7778C10.5855 12.7778 9.22896 12.2394 8.22876 11.2808C7.22857 10.3223 6.66667 9.02234 6.66667 7.66676ZM17.0987 13.5739C18.3673 12.5682 19.2813 11.2117 19.7157 9.68972C20.1503 8.16764 20.0841 6.55431 19.5264 5.06967C18.9687 3.58504 17.9465 2.3015 16.5993 1.39418C15.2523 0.486869 13.6459 0 11.9993 0C10.3528 0 8.74637 0.486869 7.39928 1.39418C6.05217 2.3015 5.03001 3.58504 4.47228 5.06967C3.91453 6.55431 3.8484 8.16764 4.28289 9.68972C4.71737 11.2117 5.63132 12.5682 6.9 13.5739C2.648 15.0088 0 18.2045 0 21.7222C0 22.0611 0.14048 22.3862 0.39052 22.6257C0.640573 22.8655 0.979707 23 1.33333 23C1.68696 23 2.02609 22.8655 2.27615 22.6257C2.52619 22.3862 2.66667 22.0611 2.66667 21.7222C2.66667 18.8217 5.80667 15.3334 12 15.3334C18.1933 15.3334 21.3333 18.8217 21.3333 21.7222C21.3333 22.0611 21.4739 22.3862 21.7239 22.6257C21.9739 22.8655 22.3131 23 22.6667 23C23.0203 23 23.3595 22.8655 23.6095 22.6257C23.8595 22.3862 24 22.0611 24 21.7222C24 18.2045 21.3547 15.0088 17.0987 13.5739Z" fill="url(#paint0_radial_4613_17013)"/>
<defs>
<radialGradient id="paint0_radial_4613_17013" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(11.8051 0.0913616) rotate(90) scale(23 23.2412)">
<stop stop-color="#4FBFFE"/>
<stop offset="1" stop-color="#7644FF"/>
</radialGradient>
</defs>
</svg>
                                </div>
                            @endif
                        </td>
                        <td>{{ $account->firstName}} {{$account->lastName}}</td>
                        <td>{{ $account->email }}</td>
                        <td>0{{ $account->phoneNumber }}</td>
                        <td>
                            @if(isset($account->user))
                                <span class="badge rounded-pill bg-primary">User</span>
                            @elseif(isset($account->office))
                                <span class="badge rounded-pill bg-warning text-dark">Office</span>
                            @elseif(isset($account->admin))
                                <span class="badge rounded-pill bg-dark">Admin</span>
                            @endif
                        </td>
                        <td>
                            @if($account->blocked == 0)
                                <span class="badge rounded-pill bg-success">Active</span>
                            @else
                                <span class="badge rounded-pill bg-danger">InActive</span>
                            @endif
                        </td>
                        <td>
                            @if($account->blocked == 0)
                                <label class="switch">
                                    <input type="checkbox" class="blockedSwitch">
                                    <div class="slider">
                                        <div class="circle">
                                            <svg class="cross" xml:space="preserve"
                                                 style="enable-background:new 0 0 512 512" viewBox="0 0 365.696 365.696"
                                                 y="0" x="0" height="6" width="6"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 version="1.1" xmlns="http://www.w3.org/2000/svg">
                <g>
                    <path data-original="#000000" fill="currentColor"
                          d="M243.188 182.86 356.32 69.726c12.5-12.5 12.5-32.766 0-45.247L341.238 9.398c-12.504-12.503-32.77-12.503-45.25 0L182.86 122.528 69.727 9.374c-12.5-12.5-32.766-12.5-45.247 0L9.375 24.457c-12.5 12.504-12.5 32.77 0 45.25l113.152 113.152L9.398 295.99c-12.503 12.503-12.503 32.769 0 45.25L24.48 356.32c12.5 12.5 32.766 12.5 45.247 0l113.132-113.132L295.99 356.32c12.503 12.5 32.769 12.5 45.25 0l15.081-15.082c12.5-12.504 12.5-32.77 0-45.25zm0 0"></path>
                </g>
            </svg>
                                            <svg class="checkmark" xml:space="preserve"
                                                 style="enable-background:new 0 0 512 512" viewBox="0 0 24 24" y="0"
                                                 x="0"
                                                 height="10" width="10" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 version="1.1" xmlns="http://www.w3.org/2000/svg">
                <g>
                    <path class="" data-original="#000000" fill="currentColor"
                          d="M9.707 19.121a.997.997 0 0 1-1.414 0l-5.646-5.647a1.5 1.5 0 0 1 0-2.121l.707-.707a1.5 1.5 0 0 1 2.121 0L9 14.171l9.525-9.525a1.5 1.5 0 0 1 2.121 0l.707.707a1.5 1.5 0 0 1 0 2.121z"></path>
                </g>
            </svg>
                                        </div>
                                    </div>
                                </label>
                            @else
                                <label class="switch">
                                    <input type="checkbox"  checked="" class="blockedSwitch">
                                    <div class="slider">
                                        <div class="circle">
                                            <svg class="cross" xml:space="preserve"
                                                 style="enable-background:new 0 0 512 512" viewBox="0 0 365.696 365.696"
                                                 y="0" x="0" height="6" width="6"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 version="1.1" xmlns="http://www.w3.org/2000/svg">
                <g>
                    <path data-original="#000000" fill="currentColor"
                          d="M243.188 182.86 356.32 69.726c12.5-12.5 12.5-32.766 0-45.247L341.238 9.398c-12.504-12.503-32.77-12.503-45.25 0L182.86 122.528 69.727 9.374c-12.5-12.5-32.766-12.5-45.247 0L9.375 24.457c-12.5 12.504-12.5 32.77 0 45.25l113.152 113.152L9.398 295.99c-12.503 12.503-12.503 32.769 0 45.25L24.48 356.32c12.5 12.5 32.766 12.5 45.247 0l113.132-113.132L295.99 356.32c12.503 12.5 32.769 12.5 45.25 0l15.081-15.082c12.5-12.504 12.5-32.77 0-45.25zm0 0"></path>
                </g>
            </svg>
                                            <svg class="checkmark" xml:space="preserve"
                                                 style="enable-background:new 0 0 512 512" viewBox="0 0 24 24" y="0"
                                                 x="0"
                                                 height="10" width="10" xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 version="1.1" xmlns="http://www.w3.org/2000/svg">
                <g>
                    <path class="" data-original="#000000" fill="currentColor"
                          d="M9.707 19.121a.997.997 0 0 1-1.414 0l-5.646-5.647a1.5 1.5 0 0 1 0-2.121l.707-.707a1.5 1.5 0 0 1 2.121 0L9 14.171l9.525-9.525a1.5 1.5 0 0 1 2.121 0l.707.707a1.5 1.5 0 0 1 0 2.121z"></path>
                </g>
            </svg>
                                        </div>
                                    </div>
                                </label>
                            @endif

                        </td>
                        <td>
                            <a href="{{ route('accounts.show', $account->id) }}" class="btn btn-warning btn-sm">Show</a>
                            <a href="{{ route('accounts.edit', $account->id) }}" class="btn btn-info btn-sm">Edit</a>
                            {{--                            <a href="{{ route('accounts.destroy', $account->id) }}" class="btn btn-danger btn-sm">Delete</a>--}}
                            {!! Form::open(['method' => 'DELETE','route' => ['accounts.destroy', $account->id],'style'=>'display:inline']) !!}
                            {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm']) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            @if(!isset($paginate))
                <div class="d-flex justify-content-center">
                    {!! $accounts->links() !!}
                </div>
            @endif

        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
            }
        });
    </script>
    <script>

        $('.accounts').addClass("btnFocused");



$(document).ready(function () {
            $('.blockedSwitch').click(function () {

                let blocked;
                var currentRow = $(this).closest("tr");
                var account_id = currentRow.find("th:eq(0)").text();

                console.log(account_id)

                if ($(this).prop("checked") == true) {
                    blocked = 1;

                } else if ($(this).prop("checked") == false) {
                    blocked = 0;

                }
                $.ajax({
                    url: "{{route('accounts.blocked')}}",
                    data: {
                        blocked: blocked,
                        account_id: account_id

                    },
                    type: "POST",
                    success: function (response) {
                        JSON.stringify(response);
                        console.log(response)
                        setTimeout(function () {
                            location.reload();
                        }, 200);
                    }
                });
            });
        });
    </script>
@endsection
