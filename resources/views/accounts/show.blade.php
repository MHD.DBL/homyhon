@extends('home.home')

@section('content')
    <div class="bg-light p-5 rounded" style="height: 100% !important;">
        <h1>Show Account</h1>

        <div class="container mt-4">


            @if(isset($account->user))
                <h3 class="mb-4">User Account</h3>
                @if(isset($account->img->path))
                <div class="mb-3">
                    <span style="font-weight: bold;">Profile Image  :  </span> <br>
                    <img src="{{$account->img->path}}" alt="" width="415px" height="275px">
                </div>
                @endif
                <div style="font-size: large;">
                    <span
                        style="font-weight: bold;">Full Name  :  </span>{{ $account->firstName}} {{$account->lastName}}
                </div>
                <div style="font-size: large;">
                    <span style="font-weight: bold;">Email :  </span>{{ $account->email }}
                </div>
                <div style="font-size: large;">
                    <span style="font-weight: bold;">Phone Number  :  </span>0{{ $account->phoneNumber }}
                </div>
                <div style="font-size: large;">
                    <span style="font-weight: bold;">Status  :  </span>@if($account->blocked == 0)
                        <span class="badge rounded-pill bg-success">Active</span>
                    @else
                        <span class="badge rounded-pill bg-danger">InActive</span>
                    @endif
                </div>
                <div style="font-size: large;">
                    <span style="font-weight: bold;">Gender  :  </span>{{ $account->user->gender }}
                </div>
                <div style="font-size: large;">
                    <span style="font-weight: bold;">BirthDate :  </span>{{ $account->user->birthdate}}
                </div>
                <div style="font-size: large;">
                    <span style="font-weight: bold;">Day left to end Subscription :  </span> <span
                        class="badge rounded-pill bg-primary">{{ $dayleft}}</span>
                </div>
            @endif

            @if(isset($account->office))
                <h3 class="mb-4">Office Account</h3>
                    @if(isset($account->img->path))
                    <div class="mb-3">
                    <span style="font-weight: bold;">Profile Image  :  </span> <br>
                    <img src="{{$account->img->path}}" alt="" width="415px" height="275px">
                    </div>
                    @endif
                <div style="font-size: large;">
                    <span
                        style="font-weight: bold;">Full Name  :  </span>{{ $account->firstName}} {{$account->lastName}}
                </div>
                <div style="font-size: large;">
                    <span style="font-weight: bold;">Email :  </span>{{ $account->email }}
                </div>
                <div style="font-size: large;">
                    <span style="font-weight: bold;">Phone Number  :  </span>0{{ $account->phoneNumber }}
                </div>
                <div style="font-size: large;">
                    <span style="font-weight: bold;">Status  :  </span>@if($account->blocked == 0)
                        <span class="badge rounded-pill bg-success">Active</span>
                    @else
                        <span class="badge rounded-pill bg-danger">InActive</span>
                    @endif
                </div>
                <div style="font-size: large;">
                    <span style="font-weight: bold;">Office Name  :  </span>{{ $account->office->officeName}}
                </div>
                <div style="font-size: large;">
                    <div style="font-size: large;">
                        <span
                            style="font-weight: bold;">Governorate :  </span>{{ $account->office->region->governorate->name}}
                    </div>
                    <div style="font-size: large;">
                        <span style="font-weight: bold;">Region :  </span>{{ $account->office->region->name}}
                    </div>
                    <span style="font-weight: bold;">Address :  </span>{{ $account->office->address}}
                    
                    <div style="font-size: large;">
                    <span style="font-weight: bold;">Description :  </span>{{ $account->office->description }}
                </div>
                </div>


                <div style="font-size: large;">
                    <span style="font-weight: bold;">Telephone Number :  </span>0{{ $account->office->telephone}}
                </div>
                <div style="font-size: large;">
                    <span style="font-weight: bold;">Rating Of Office :  </span><span
                        class="badge rounded-pill bg-primary">{{ $account->office->rating}}</span>
                </div>
            @endif

            @if(isset($account->admin))

                <h3 class="mb-4">Office Account</h3>
                    @if(isset($account->img->path))
                    <div class="mb-3">
                    <span style="font-weight: bold;">Profile Image  :  </span> <br>
                    <img src="{{$account->img->path}}" alt="" width="415px" height="275px">
                </div>
                    @endif
                <div style="font-size: large;">
                    <span
                        style="font-weight: bold;">Full Name  :  </span>{{ $account->firstName}} {{$account->lastName}}
                </div>
                <div style="font-size: large;">
                    <span style="font-weight: bold;">Email :  </span>{{ $account->email }}
                </div>
                <div style="font-size: large;">
                    <span style="font-weight: bold;">Phone Number  :  </span>0{{ $account->phoneNumber }}
                </div>
                <div style="font-size: large;">
                    <span style="font-weight: bold;">Status  :  </span>@if($account->blocked == 0)
                        <span class="badge rounded-pill bg-success">Active</span>
                    @else
                        <span class="badge rounded-pill bg-danger">InActive</span>
                    @endif
                </div>
                <div style="font-size: large;">
                    <span style="font-weight: bold;">Role  :  </span>{{ $account->admin->role}}
                </div>
            @endif

            <div class="d-flex justify-content-center mt-5">
                <a href="{{ route('accounts.index') }}" class="btn btn-primary">Back</a>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script>
        $('.accounts').addClass("btnFocused");

    </script>
@endsection
