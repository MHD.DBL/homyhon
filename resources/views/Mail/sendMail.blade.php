<!DOCTYPE html>
<html>
<head>
    <title>NewHome</title>
</head>
<body>

<p>
    Your New Home Verification code is:
    <br> <b style="color: #3d78e5;font-size: 40px">{{$mailData['code']}}</b>
</p>

<p>
    Enter it in the New Home verify email screen to create your account.
    <br> If you are not trying to create a <b style="color: #3D78E5FF">NewHome</b> account , you may ignore this email.
</p>

<br>

<p>Thank you for joining our app</p>

</body>
</html>
